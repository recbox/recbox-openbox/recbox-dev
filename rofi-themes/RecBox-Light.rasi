/*********************************************
* ROFI RecBox Color theme
* Copyright: 
*   > Projekt:Root
*     https://gitlab.com/ProjektRoot
*   > deadguy
*     https://gitlab.com/deadguy
**********************************************/

configuration {
    display-drun:    "Activate";
    display-run:     "Execute";
    display-window:  "Window";
    display-ssh:     "SSH";
    show-icons:      false;
    sidebar-mode:    true;
}

* {
    background:                  rgba ( 247, 247, 247, 100 % );
    normal-background:           rgba ( 220, 220, 220, 100 % );
    active-background:           @normal-background;
    urgent-background:           @normal-background;
    background-color:            @background;
    border-color:                rgba ( 80, 78, 101, 100 % );
    selected-normal-background:  rgba ( 47, 155, 133, 100 % );
    selected-active-background:  @selected-normal-background;
    selected-urgent-background:  @selected-normal-background;
    alternate-normal-background: @selected-normal-background;
    alternate-active-background: @selected-normal-background;
    alternate-urgent-background: @selected-normal-background;
    foreground:                  rgba ( 134, 134, 134, 100 % );
    normal-foreground:           @foreground;
    active-foreground:           @foreground;
    urgent-foreground:           @foreground;
    selected-normal-foreground:  rgba ( 242, 242, 242, 100 % );
    selected-active-foreground:  @selected-normal-foreground;
    selected-urgent-foreground:  @selected-normal-foreground;
    alternate-normal-foreground: @normal-foreground;
    alternate-active-foreground: @active-foreground;
    alternate-urgent-foreground: @urgent-foreground;
    entry-title:                 rgba ( 114, 113, 125, 100 % );

    line-margin:                 2;
    line-padding:                2;
    separator-style:             "none";
    hide-scrollbar:              "true";
    margin:                      0;
    padding:                     0;    
    spacing:                     4;
    font:                        "Noto Sans Medium 9";
}

window {
    border:           0px 0px 0px 1px;
    padding:          6;
    location:         north east;
    orientation:      vertical;
    height:           100%;
    width:            19.6%;
    background-color: @background;
    border-color:     @border-color;
}

mainbox {
    border:  0;
    padding: 0;
}

button {
    spacing:    0;
    padding:    5px 2px;
    text-color: @normal-foreground;
}

button.selected {
    text-color:       @selected-normal-foreground;
    background-color: @selected-normal-background;
}

inputbar {
    padding: 10px;
    spacing: 0px;
    background-color: @background;
}

message {
    border:       0px 0px 0px ;
    padding:      1px ;
    border-color: @background;
}

textbox {
    text-color: @foreground;
}

listview {
    dynamic:      false;
    cycle:        true;
    border:       0px 0px 0px 0px;
    spacing:      2px;
    padding:      15px 0px 0px;
    border-color: @background;
}

prompt {
    spacing:          2px;
    padding:          4px;
    text-color:       @normal-foreground;
    background-color: @background;
}

textbox-prompt-colon {
    expand:     false;
    str:        "";
    margin:     0px 0.3em 0em 0em ;
    text-color: @selected-active-foreground;
}

case-indicator {
    spacing:    0;
    text-color: @normal-foreground;
    background-color: @background;
}

entry {
    padding:          4px 2px;
    spacing:          0;
    text-color:       @foreground;
    background-color: @background;
}

element {
    border:         0;
    padding:        4px 24px;
    text-color:     @normal-foreground;
}

element.normal.normal {
    text-color:       @normal-foreground;
    background-color: @background;
}

element.normal.urgent {
    text-color:       @urgent-foreground;
    background-color: @background;
}

element.normal.active {
    text-color:       @active-foreground;
    background-color: @background;
}

element.selected.normal {
    text-color:       @selected-normal-foreground;
    background-color: @selected-normal-background;
}

element.selected.urgent {
    text-color:       @selected-urgent-foreground;
    background-color: @selected-urgent-background;
}

element.selected.active {
    text-color:       @selected-active-foreground;
    background-color: @selected-active-background;
}

element.alternate.normal {
    text-color:       @alternate-normal-foreground;
    background-color: @background;
}

element.alternate.urgent {
    text-color:       @alternate-urgent-foreground;
    background-color: @background;
}

element.alternate.active {
    text-color:       @alternate-active-foreground;
    background-color: @background;
}
