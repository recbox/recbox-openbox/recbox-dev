#!/bin/sh

LIGHT_OPENBOX_THEME_PATH="/usr/share/themes/light-recbox/openbox-3/themerc"
DARK_OPENBOX_THEME_PATH="/usr/share/themes/dark-recbox/openbox-3/themerc"
LIGHT_ROFI_THEME_PATH="/usr/share/rofi/themes/RecBox-Light.rasi"
DARK_ROFI_THEME_PATH="/usr/share/rofi/themes/RecBox-Dark.rasi"

OPENBOX_ROFI_MATCHA_SEA_SCHEME() {

### OPENBOX ###
### GREEN

	if [ "$(grep "menu.items.active.bg.color:" "$LIGHT_OPENBOX_THEME_PATH" | cut -d ' ' -f 2)" = "#3498db" ]; then

		sed -i 's/#3498db/#2f9b85/g' "$LIGHT_OPENBOX_THEME_PATH" "$DARK_OPENBOX_THEME_PATH"

	elif [ "$(grep "menu.items.active.bg.color:" "$LIGHT_OPENBOX_THEME_PATH" | cut -d ' ' -f 2)" = "#e44138" ]; then

		sed -i 's/#e44138/#2f9b85/g' "$LIGHT_OPENBOX_THEME_PATH" "$DARK_OPENBOX_THEME_PATH"

	elif [ "$(grep "menu.items.active.bg.color:" "$LIGHT_OPENBOX_THEME_PATH" | cut -d ' ' -f 2)" = "#2f9b85" ]; then

		echo "Openbox Matcha Sea Light: already in use"

	else
		echo "Openbox Matcha Sea Light: Error"

	fi

### BACKGROUND

	if [ "$(grep "menu.items.bg.color:" "$DARK_OPENBOX_THEME_PATH" | cut -d ' ' -f 2)" = "#1b1d24" ]; then

		sed -i 's/#1b1d24/#1b2224/g' "$DARK_OPENBOX_THEME_PATH"

	elif [ "$(grep "menu.items.bg.color:" "$DARK_OPENBOX_THEME_PATH" | cut -d ' ' -f 2)" = "#222222" ]; then

		sed -i 's/#222222/#1b2224/g' "$DARK_OPENBOX_THEME_PATH"

	elif [ "$(grep "menu.items.bg.color:" "$DARK_OPENBOX_THEME_PATH" | cut -d ' ' -f 2)" = "#1b2224" ]; then

		echo "Openbox Matcha Sea Dark: already in use"

	else
		echo "Openbox Matcha Sea Dark: Error"

	fi

### ROFI ###
### LIGHT

	if [ "$(grep "selected-normal-background:" "$LIGHT_ROFI_THEME_PATH" | awk -F"[()]" '{print $2}' | sed -e 's/^.//' -e 's/.$//' -e 's/.$//' | head -n 1 | sed -e 's/ //g' -e 's/,//g')" = "52152219100" ]; then

		sed -i 's/selected-normal-background:  rgba ( 52, 152, 219, 100 % )/selected-normal-background:  rgba ( 47, 155, 133, 100 % )/g' "$LIGHT_ROFI_THEME_PATH"

	elif [ "$(grep "selected-normal-background:" "$LIGHT_ROFI_THEME_PATH" | awk -F"[()]" '{print $2}' | sed -e 's/^.//' -e 's/.$//' -e 's/.$//' | head -n 1 | sed -e 's/ //g' -e 's/,//g')" = "2286556100" ]; then

		sed -i 's/selected-normal-background:  rgba ( 228, 65, 56, 100 % )/selected-normal-background:  rgba ( 47, 155, 133, 100 % )/g' "$LIGHT_ROFI_THEME_PATH"

	elif [ "$(grep "selected-normal-background:" "$LIGHT_ROFI_THEME_PATH" | awk -F"[()]" '{print $2}' | sed -e 's/^.//' -e 's/.$//' -e 's/.$//' | head -n 1 | sed -e 's/ //g' -e 's/,//g')" = "47155133100" ]; then

		echo "Rofi Matcha Sea Light: already in use"

	else
	
		echo "Rofi Matcha Sea Light: Error"

fi

### DARK

	if [ "$(grep "background:" "$DARK_ROFI_THEME_PATH" | awk -F"[()]" '{print $2}' | sed -e 's/^.//' -e 's/.$//' -e 's/.$//' | head -n 1 | sed -e 's/ //g' -e 's/,//g')" = "272936100" ]; then

		sed -i 's/background:                  rgba ( 27, 29, 36, 100 % )/background:                  rgba ( 27, 34, 36, 100 % )/' "$DARK_ROFI_THEME_PATH"
		sed -i 's/selected-normal-background:  rgba ( 47, 137, 197, 100 % )/selected-normal-background:  rgba ( 47, 155, 133, 100 % )/' "$DARK_ROFI_THEME_PATH"

	elif [ "$(grep "background:" "$DARK_ROFI_THEME_PATH" | awk -F"[()]" '{print $2}' | sed -e 's/^.//' -e 's/.$//' -e 's/.$//' | head -n 1 | sed -e 's/ //g' -e 's/,//g')" = "343434100" ]; then

		sed -i 's/background:                  rgba ( 34, 34, 34, 100 % )/background:                  rgba ( 27, 34, 36, 100 % )/' "$DARK_ROFI_THEME_PATH"
		sed -i 's/selected-normal-background:  rgba ( 228, 65, 56, 100 % )/selected-normal-background:  rgba ( 47, 155, 133, 100 % )/' "$DARK_ROFI_THEME_PATH"

	elif [ "$(grep "background:" "$DARK_ROFI_THEME_PATH" | awk -F"[()]" '{print $2}' | sed -e 's/^.//' -e 's/.$//' -e 's/.$//' | head -n 1 | sed -e 's/ //g' -e 's/,//g')" = "273436100" ]; then

		echo "Rofi Matcha Sea Dark: already in use"

	else

		echo "Rofi Matcha Sea Dark: Error"

	fi

}

OPENBOX_ROFI_MATCHA_ALIZ_SCHEME() {

### OPENBOX ###
### RED

	if [ "$(grep "menu.items.active.bg.color:" "$LIGHT_OPENBOX_THEME_PATH" | cut -d ' ' -f 2)" = "#3498db" ]; then

		sed -i 's/#3498db/#e44138/g' "$LIGHT_OPENBOX_THEME_PATH" "$DARK_OPENBOX_THEME_PATH"

	elif [ "$(grep "menu.items.active.bg.color:" "$LIGHT_OPENBOX_THEME_PATH" | cut -d ' ' -f 2)" = "#2f9b85" ]; then

		sed -i 's/#2f9b85/#e44138/g' "$LIGHT_OPENBOX_THEME_PATH" "$DARK_OPENBOX_THEME_PATH"

	elif [ "$(grep "menu.items.active.bg.color:" "$LIGHT_OPENBOX_THEME_PATH" | cut -d ' ' -f 2)" = "#e44138" ]; then

		echo "Openbox Matcha Aliz Light: already in use"

	else

		echo "Openbox Matcha Aliz Light: Error"

	fi

### BACKGROUND

	if [ "$(grep "menu.items.bg.color:" "$DARK_OPENBOX_THEME_PATH" | cut -d ' ' -f 2)" = "#1b1d24" ]; then

		sed -i 's/#1b1d24/#222222/g' "$DARK_OPENBOX_THEME_PATH"

	elif [ "$(grep "menu.items.bg.color:" "$DARK_OPENBOX_THEME_PATH" | cut -d ' ' -f 2)" = "#1b2224" ]; then

		sed -i 's/#1b2224/#222222/g' "$DARK_OPENBOX_THEME_PATH"

	elif [ "$(grep "menu.items.bg.color:" "$DARK_OPENBOX_THEME_PATH" | cut -d ' ' -f 2)" = "#222222" ]; then

		echo "Openbox Matcha Aliz Dark: already in use"

	else

		echo "Openbox Matcha Aliz Dark: Error"

	fi

### ROFI ###
### LIGHT

	if [ "$(grep "selected-normal-background:" "$LIGHT_ROFI_THEME_PATH" | awk -F"[()]" '{print $2}' | sed -e 's/^.//' -e 's/.$//' -e 's/.$//' | head -n 1 | sed -e 's/ //g' -e 's/,//g')" = "52152219100" ]; then

		sed -i 's/selected-normal-background:  rgba ( 52, 152, 219, 100 % )/selected-normal-background:  rgba ( 228, 65, 56, 100 % )/g' "$LIGHT_ROFI_THEME_PATH"

	elif [ "$(grep "selected-normal-background:" "$LIGHT_ROFI_THEME_PATH" | awk -F"[()]" '{print $2}' | sed -e 's/^.//' -e 's/.$//' -e 's/.$//' | head -n 1 | sed -e 's/ //g' -e 's/,//g')" = "47155133100" ]; then

		sed -i 's/selected-normal-background:  rgba ( 47, 155, 133, 100 % )/selected-normal-background:  rgba ( 228, 65, 56, 100 % )/g' "$LIGHT_ROFI_THEME_PATH"

	elif [ "$(grep "selected-normal-background:" "$LIGHT_ROFI_THEME_PATH" | awk -F"[()]" '{print $2}' | sed -e 's/^.//' -e 's/.$//' -e 's/.$//' | head -n 1 | sed -e 's/ //g' -e 's/,//g')" = "2286556100" ]; then

		echo "Rofi Matcha Aliz Light: already in use"

	else

		echo "Rofi Matcha Aliz Light: Error"

	fi

### DARK
	if [ "$(grep "background:" "$DARK_ROFI_THEME_PATH" | awk -F"[()]" '{print $2}' | sed -e 's/^.//' -e 's/.$//' -e 's/.$//' | head -n 1 | sed -e 's/ //g' -e 's/,//g')" = "273436100" ]; then

		sed -i 's/background:                  rgba ( 27, 34, 36, 100 % )/background:                  rgba ( 34, 34, 34, 100 % )/' "$DARK_ROFI_THEME_PATH"
		sed -i 's/selected-normal-background:  rgba ( 47, 155, 133, 100 % )/selected-normal-background:  rgba ( 228, 65, 56, 100 % )/' "$DARK_ROFI_THEME_PATH"

	elif [ "$(grep "background:" "$DARK_ROFI_THEME_PATH" | awk -F"[()]" '{print $2}' | sed -e 's/^.//' -e 's/.$//' -e 's/.$//' | head -n 1 | sed -e 's/ //g' -e 's/,//g')" = "272936100" ]; then

		sed -i 's/background:                  rgba ( 27, 29, 36, 100 % )/background:                  rgba ( 34, 34, 34, 100 % )/' "$DARK_ROFI_THEME_PATH"
		sed -i 's/selected-normal-background:  rgba ( 47, 137, 197, 100 % )/selected-normal-background:  rgba ( 228, 65, 56, 100 % )/' "$DARK_ROFI_THEME_PATH"

	elif [ "$(grep "background:" "$DARK_ROFI_THEME_PATH" | awk -F"[()]" '{print $2}' | sed -e 's/^.//' -e 's/.$//' -e 's/.$//' | head -n 1 | sed -e 's/ //g' -e 's/,//g')" = "343434100" ]; then

		echo "Rofi Matcha Aliz Dark: already in use"

	else

		echo "Rofi Matcha Aliz Dark: Error"

	fi

}

OPENBOX_ROFI_MATCHA_AZUL_SCHEME() {

### OPENBOX ###
### BLUE

	if [ "$(grep "menu.items.active.bg.color:" "$LIGHT_OPENBOX_THEME_PATH" | cut -d ' ' -f 2)" = "#2f9b85" ]; then

		sed -i 's/#2f9b85/#3498db/g' "$LIGHT_OPENBOX_THEME_PATH" "$DARK_OPENBOX_THEME_PATH"

	elif [ "$(grep "menu.items.active.bg.color:" "$LIGHT_OPENBOX_THEME_PATH" | cut -d ' ' -f 2)" = "#e44138" ]; then

		sed -i 's/#e44138/#3498db/g' "$LIGHT_OPENBOX_THEME_PATH" "$DARK_OPENBOX_THEME_PATH"

	elif [ "$(grep "menu.items.active.bg.color:" "$LIGHT_OPENBOX_THEME_PATH" | cut -d ' ' -f 2)" = "#3498db" ]; then

		echo "Openbox Matcha Azul Light: already in use"

	else

		echo "Openbox Matcha Azul Light: Error"

	fi

### BACKGROUND

	if [ "$(grep "menu.items.bg.color:" "$DARK_OPENBOX_THEME_PATH" | cut -d ' ' -f 2)" = "#1b2224" ]; then

		sed -i 's/#1b2224/#1b1d24/g' "$DARK_OPENBOX_THEME_PATH"

	elif [ "$(grep "menu.items.bg.color:" "$DARK_OPENBOX_THEME_PATH" | cut -d ' ' -f 2)" = "#222222" ]; then

		sed -i 's/#222222/#1b1d24/g' "$DARK_OPENBOX_THEME_PATH"

	elif [ "$(grep "menu.items.bg.color:" "$DARK_OPENBOX_THEME_PATH" | cut -d ' ' -f 2)" = "#1b1d24" ]; then

		echo "Openbox Matcha Azul Dark: already in use"

	else

		echo "Openbox Matcha Azul Dark: Error"

	fi

### ROFI ###
### LIGHT

	if [ "$(grep "selected-normal-background:" "$LIGHT_ROFI_THEME_PATH" | awk -F"[()]" '{print $2}' | sed -e 's/^.//' -e 's/.$//' -e 's/.$//' | head -n 1 | sed -e 's/ //g' -e 's/,//g')" = "47155133100" ]; then

		sed -i 's/selected-normal-background:  rgba ( 47, 155, 133, 100 % )/selected-normal-background:  rgba ( 52, 152, 219, 100 % )/g' "$LIGHT_ROFI_THEME_PATH"

	elif [ "$(grep "selected-normal-background:" "$LIGHT_ROFI_THEME_PATH" | awk -F"[()]" '{print $2}' | sed -e 's/^.//' -e 's/.$//' -e 's/.$//' | head -n 1 | sed -e 's/ //g' -e 's/,//g')" = "2286556100" ]; then

		sed -i 's/selected-normal-background:  rgba ( 228, 65, 56, 100 % )/selected-normal-background:  rgba ( 52, 152, 219, 100 % )/g' "$LIGHT_ROFI_THEME_PATH"

	elif [ "$(grep "selected-normal-background:" "$LIGHT_ROFI_THEME_PATH" | awk -F"[()]" '{print $2}' | sed -e 's/^.//' -e 's/.$//' -e 's/.$//' | head -n 1 | sed -e 's/ //g' -e 's/,//g')" = "52152219100" ]; then

		echo "Rofi Matcha Azul already in use"

	else

		echo "Rofi Matcha Azul Light: Error"

	fi

### DARK

	if [ "$(grep "background:" "$DARK_ROFI_THEME_PATH" | awk -F"[()]" '{print $2}' | sed -e 's/^.//' -e 's/.$//' -e 's/.$//' | head -n 1 | sed -e 's/ //g' -e 's/,//g')" = "273436100" ]; then

		sed -i 's/background:                  rgba ( 27, 34, 36, 100 % )/background:                  rgba ( 27, 29, 36, 100 % )/' "$DARK_ROFI_THEME_PATH"
		sed -i 's/selected-normal-background:  rgba ( 47, 155, 133, 100 % )/selected-normal-background:  rgba ( 47, 137, 197, 100 % )/' "$DARK_ROFI_THEME_PATH"

	elif [ "$(grep "background:" "$DARK_ROFI_THEME_PATH" | awk -F"[()]" '{print $2}' | sed -e 's/^.//' -e 's/.$//' -e 's/.$//' | head -n 1 | sed -e 's/ //g' -e 's/,//g')" = "343434100" ]; then

		sed -i 's/background:                  rgba ( 34, 34, 34, 100 % )/background:                  rgba ( 27, 29, 36, 100 % )/' "$DARK_ROFI_THEME_PATH"
		sed -i 's/selected-normal-background:  rgba ( 228, 65, 56, 100 % )/selected-normal-background:  rgba ( 47, 137, 197, 100 % )/' "$DARK_ROFI_THEME_PATH"

	elif [ "$(grep "background:" "$DARK_ROFI_THEME_PATH" | awk -F"[()]" '{print $2}' | sed -e 's/^.//' -e 's/.$//' -e 's/.$//' | head -n 1 | sed -e 's/ //g' -e 's/,//g')" = "272936100" ]; then

		echo "Rofi Matcha Azul already in use"

	else

		echo "Rofi Matcha Azul Dark: Error"

	fi

}

case $1 in

	--matcha-sea) OPENBOX_ROFI_MATCHA_SEA_SCHEME;;
	--matcha-aliz) OPENBOX_ROFI_MATCHA_ALIZ_SCHEME;;
	--matcha-azul) OPENBOX_ROFI_MATCHA_AZUL_SCHEME;;
	*)

	echo -e "
	RecBox Colors Global\n
	Helper script to change Openbox and Rofi
	themes colors on the fly.\n
	Usage:\n
	scripts-box.sh [ OPTION ]\n
	Options:\n
	--matcha-sea
	--matcha-aliz
	--matcha-azul
" >&2

exit 1;;

esac

exit 0
