#!/bin/sh

# VERSION = 1.2

MAIN_WINDOW_TITLE="RecBox Colors"
MAIN_WINDOW_TEXT="Here you can change colors for all menus and tint2 panel. GUI is pretty simple,\nso for more advance configuration you need to use Tint2 Edit."
ENTRY_BOX_TITLE="RecBox Colors"
JG_ENTRY_BOX_TEXT="Input new color and transparency here.\nDo not leave entry box empty."
JG_ENTRY_SEP_BOX_TEXT="Input new color here.\nDo not leave entry box empty."
TINT_ENTRY_BOX_TEXT="Input new color here.\nDo not leave entry box empty."
OK_BUTTON="Apply"
CANCEL_BUTTON="Close"
EXTRA_BUTTON="Back"
PICK_COLUMN="Pick"
OPTION_COLUMN="Option"
DESCRIPTION_COLUMN="Description"

### TITLES AND TEXT IN SUBMENUS

MMENU_TITLE_LIGHT="Main Menu - Light"
MMENU_TEXT_LIGHT="Change color and transparency (except separator markup) of chosen item.\nAfter you accept new color, menu will pop up. Don't be afraid, it's light menu\nvariant (right_click_light-rc) and changes made here don't have effect on active\nmenu config if you are in dark mode."
MMENU_TITLE_DARK="Main Menu - Dark"
MMENU_TEXT_DARK="Change color and transparency (except separator markup) of chosen item.\nAfter you accept new color, menu will pop up. Don't be afraid, it's light menu\nvariant (light-jgmenu) and changes made here don't have effect on active\nmenu config if you are in light mode."

RCMENU_TITLE_LIGHT="Right Click - Light"
RCMENU_TEXT_LIGHT="Change color and transparency (except separator markup) of chosen item.\nAfter you accept new color, menu will pop up."
RCMENU_TITLE_DARK="Right Click - Dark"
RCMENU_TEXT_DARK="Change color and transparency (except separator markup) of chosen item.\nAfter you accept new color, menu will pop up."

PANEL_MENUS_TITLE="Menus in Panel"
PANEL_MENUS_TEXT="Change color and transparency (except separator markup) of chosen item."

SMENU_TITLE_LIGHT="Side Menu - Light"
SMENU_TEXT_LIGHT="Change color and transparency (except separator markup) of chosen item.\nSide menu works a bit different from Main Menu so you need to switch off\nDark Mode to see changes made in Light Menu."
SMENU_TITLE_DARK="Side Menu - Dark"
SMENU_TEXT_DARK="Change color and transparency (except separator markup) of chosen item.\nSide menu works a bit different from Main Menu so you need to switch to\nDark Mode to see changes made in Dark Menu."

TINT_PANEL_TITLE_LIGHT="Tint2 Panel - Light"
TINT_PANEL_TEXT_LIGHT="Change color pallet of tint2 panel items. To change panel items color separately\nuse Tint2 Edit panel manager.\nTo see changes, switch off Dark Mode."
TINT_PANEL_TITLE_DARK="Tint2 Panel - Dark"
TINT_PANEL_TEXT_DARK="Change color pallet of tint2 panel items. To change panel items color separately\nuse Tint2 Edit panel manager.\nTo see changes, turn on Dark Mode."

RESTORE_MENU_TITLE="Restore Settings"
RESTORE_MENU_TEXT="For the tint2 panel restore option work same as for changing colors\nwhich means that script is changing all #xxxxxx colors.\nIt's not programmed to change specific line.So if you've manual modifications\nthe best way will be to clone RecBox repo and copy/paste files."

### MAIN MENU ITEMS

GLOBAL_MATCHA_SEA=" Matcha Sea"
GLOBAL_MATCHA_ALIZ=" Matcha Aliz"
GLOBAL_MATCHA_AZUL=" Matcha Azul"
MMENU_LIGHT=" Main Menu Light"
MMENU_DARK=" Main Menu Dark"
RCMENU_LIGHT=" Right Click Menu Light"
RCMENU_DARK=" Right Click Menu Dark"
PANEL_MENUS=" Panel Menus"
SMENU_LIGHT=" Side Menu Light"
SMENU_DARK=" Side Menu Dark"
TINT_LIGHT_PANEL=" Tint2 Panel Light"
TINT_DARK_PANEL=" Tint2 Panel Dark"
DEFAULT_SETTINGS=" Restore Default Settings"

GLOBAL_MATCHA_SEA_DESCRIPTION="Global Green theme."
GLOBAL_MATCHA_ALIZ_DESCRIPTION="Global Red theme."
GLOBAL_MATCHA_AZUL_DESCRIPTION="Global Blue theme."

MMENU_LIGHT_DESCRIPTION="Change colors for light main menu."
MMENU_DARK_DESCRIPTION="Change colors for dark main menu."

RCMENU_LIGHT_DESCRIPTION="Change color for light menu variant."
RCMENU_DARK_DESCRIPTION="Change color for dark menu variant."

PANEL_MENUS_DESCRIPTION="Change colors for all menus at once."

SMENU_LIGHT_DESCRIPTION="Change colors for light side menu."
SMENU_DARK_DESCRIPTION="Change colors for dark side menu."

TINT_LIGHT_PANEL_DESCRIPTION="Change colors for light panels."
TINT_DARK_PANEL_DESCRIPTION="Change colors for dark panels."

ACCENTS_COLOR_MENU_DESCRIPTION="Main Menu buttons and border."

BUTTONS_COLOR_MENU_DESCRIPTION="Panel buttons color including clock and taskbar icons."
TASKBAR_COLOR_MENU_DESCRIPTION="Taskbar background color."

DEFAULT_SETTINGS_DESCRIPTION="Menus and tint2 panel."

FONT_COLOR_GENERAL_DESCRIPTION="Panel buttons and clock."
TASKBAR_BUTTONS_FONT_COLOR_DESCRIPTION="Show desktop and hide panel buttons."

### JGMENUS COLORS

MENU_BACKGROUND=" Menu Background"
MENU_BORDER=" Menu Border"

ITEM_BACKGROUND=" Item Background"
ITEM_FOREGROUND=" Item Foreground"

SELECTED_ITEM_BACKGROUND=" Selected Item Background"
SELECTED_ITEM_FOREGROUND=" Selected Item Foreground"
SELECTED_ITEM_BORDER=" Selected Item Border"

SEPARATOR_FOREGROUND=" Separator Foreground"
SCROLL_INDICATOR=" Scroll Indicator"
SEPARATOR_MARKUP=" Separator Markup"

PREPEND_RECT=" Search widget border"
PREPEND_SEARCH=" Search widget font"

### TINT2 COLOR SUBMENU

ACCENTS_COLOR_MENU=" Panel Accents color"
BUTTONS_COLOR_MENU=" Panel Buttons color"
TASKBAR_COLOR_MENU=" Panel Taskbar color"
FONT_COLOR_GENERAL=" Buttons font color"
TASKBAR_BUTTONS_FONT_COLOR=" Taksbar buttons font color"

### MENUS GENERAL

ICON_PATH="/usr/share/icons/Flat-Remix-Green/categories/scalable/preferences-color.svg"
ZEN_MENU="zenity --list --radiolist --width=470 --height=325 --hide-header"

### GTK CONFIG FILES PATHS
GTK2_CONFIG_PATH="$HOME/.gtkrc-2.0"
GTK3_CONFIG_PATH="$HOME/.config/gtk-3.0/settings.ini"

### QT THEMES PATHS
QT5CT_CONFIG_PATH="$HOME/.config/qt5ct/qt5ct.conf"
QT_LIGHT_THEME_PATH="$HOME/.config/Kvantum/Matcha-Light#/Matcha-Light#.kvconfig"
QT_DARK_THEME_PATH="$HOME/.config/Kvantum/Matcha-Dark#/Matcha-Dark#.kvconfig"

### DARK MODE SETTINGS PATH
DARK_MODE_SETTINGS_PATH="$HOME/.config/darkmode-settings/config"

### DUNST CONFIG PATH
DUNST_CONFIG_PATH="$HOME/.config/dunst/dunstrc"

### TINT2 PANEL PATHS

TINT2_PANEL_ACTIVE_PATH="$HOME/.config/tint2/tint2rc"
TINT2_PANEL_DAILY_LIGHT_PATH="$HOME/.config/tint2/recbox-daily-light.tint2rc"
TINT2_PANEL_DAILY_DARK_PATH="$HOME/.config/tint2/recbox-daily-dark.tint2rc"
TINT2_PANEL_STUDIO_LIGHT_PATH="$HOME/.config/tint2/recbox-studio-light.tint2rc"
TINT2_PANEL_STUDIO_DARK_PATH="$HOME/.config/tint2/recbox-studio-dark.tint2rc"
TINT2_PANEL_HIDDEN_PATH="$HOME/.config/tint2/recbox-hidden.tint2rc"

### JGMENU PATHS

ACTIVE_MAIN_MENU_PATH="$HOME/.config/jgmenu/jgmenurc"
ACTIVE_MAIN_MENU_PREPEND_PATH="$HOME/.config/jgmenu/prepend.csv"
LIGHT_MAIN_MENU_PATH="$HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu"
LIGHT_MAIN_MENU_PREPEND_PATH="$HOME/.config/openbox/jgmenu-theme-configs/light-prepend.csv"
DARK_MAIN_MENU_PATH="$HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu"
DARK_MAIN_MENU_PREPEND_PATH="$HOME/.config/openbox/jgmenu-theme-configs/dark-prepend.csv"
LIGHT_SIDE_MENU_PATH="$HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc"
DARK_SIDE_MENU_PATH="$HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc"
LIGHT_RCLICK_MENU_PATH="$HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc"
DARK_RCLICK_MENU_PATH="$HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc"
TINT_PANEL_MENUS_PATH="$HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc"
KEY_BINDINGS_MENU_PATH="$HOME/.config/openbox/key-bindings"

### TINT2 PANEL VARIABLES

LIGHT_ACCENTS_COLOR_FIND=$(awk 'NR==68' $HOME/.config/tint2/recbox-daily-light.tint2rc | sed 's/.*=//' | cut -d ' ' -f2)
LIGHT_BUTTONS_COLOR_FIND=$(awk 'NR==40' $HOME/.config/tint2/recbox-daily-light.tint2rc | sed 's/.*=//' | cut -d ' ' -f2)
LIGHT_TASKBAR_COLOR_FIND=$(awk 'NR==119' $HOME/.config/tint2/recbox-daily-light.tint2rc | sed 's/.*=//' | cut -d ' ' -f2)
LIGHT_FONT_COLOR_GENERAL_FIND=$(awk 'NR==230' $HOME/.config/tint2/recbox-daily-light.tint2rc | sed 's/.*=//' | cut -d ' ' -f2)
LIGHT_TASKBAR_BUTTONS_FONT_COLOR_FIND=$(awk 'NR==370' $HOME/.config/tint2/recbox-daily-light.tint2rc | sed 's/.*=//' | cut -d ' ' -f2)

DARK_ACCENTS_COLOR_FIND=$(awk 'NR==68' $HOME/.config/tint2/recbox-daily-dark.tint2rc | sed 's/.*=//' | cut -d ' ' -f2)
DARK_BUTTONS_COLOR_FIND=$(awk 'NR==40' $HOME/.config/tint2/recbox-daily-dark.tint2rc | sed 's/.*=//' | cut -d ' ' -f2)
DARK_TASKBAR_COLOR_FIND=$(awk 'NR==119' $HOME/.config/tint2/recbox-daily-dark.tint2rc | sed 's/.*=//' | cut -d ' ' -f2)
DARK_FONT_COLOR_GENERAL_FIND=$(awk 'NR==230' $HOME/.config/tint2/recbox-daily-dark.tint2rc | sed 's/.*=//' | cut -d ' ' -f2)
DARK_TASKBAR_BUTTONS_FONT_COLOR_FIND=$(awk 'NR==370' $HOME/.config/tint2/recbox-daily-dark.tint2rc | sed 's/.*=//' | cut -d ' ' -f2)

### MAIN MENU VARIABLES

MMENU_COLOR_MENU_BG_LIGHT=$(grep color_menu_bg $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu | sed 's/^color_menu_bg = //')
MMENU_COLOR_MENU_BORDER_LIGHT=$(grep color_menu_border $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu | sed 's/^color_menu_border = //')
MMENU_COLOR_NORM_BG_LIGHT=$(grep color_norm_bg $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu | sed 's/^color_norm_bg = //')
MMENU_COLOR_NORM_FG_LIGHT=$(grep color_norm_fg $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu | sed 's/^color_norm_fg = //')
MMENU_COLOR_SEL_BG_LIGHT=$(grep color_sel_bg $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu | sed 's/^color_sel_bg = //')
MMENU_COLOR_SEL_FG_LIGHT=$(grep color_sel_fg $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu | sed 's/^color_sel_fg = //')
MMENU_COLOR_SEL_BORDER_LIGHT=$(grep color_sel_border $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu | sed 's/^color_sel_border = //')
MMENU_COLOR_SEP_FG_LIGHT=$(grep color_sep_fg $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu | sed 's/^color_sep_fg = //')
MMENU_COLOR_SCROLL_IND_LIGHT=$(grep color_scroll_ind $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu | sed 's/^color_scroll_ind = //')
MMENU_COLOR_SEP_MARKUP_LIGHT=$(grep sep_markup $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu | sed 's/.*foreground=//' | cut -d '"' -f 2)
MMENU_COLOR_RECT_LIGHT=$(grep '@rect' $HOME/.config/openbox/jgmenu-theme-configs/light-prepend.csv | sed 's/\,/ /g' | awk '{print $9, $10}')
MMENU_COLOR_SEARCH_LIGHT=$(grep '@search' $HOME/.config/openbox/jgmenu-theme-configs/light-prepend.csv | sed 's/\,/ /g' | awk '{print $9, $10}')

MMENU_COLOR_MENU_BG_DARK=$(grep color_menu_bg $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu | sed 's/^color_menu_bg = //')
MMENU_COLOR_MENU_BORDER_DARK=$(grep color_menu_border $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu | sed 's/^color_menu_border = //')
MMENU_COLOR_NORM_BG_DARK=$(grep color_norm_bg $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu | sed 's/^color_norm_bg = //')
MMENU_COLOR_NORM_FG_DARK=$(grep color_norm_fg $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu | sed 's/^color_norm_fg = //')
MMENU_COLOR_SEL_BG_DARK=$(grep color_sel_bg $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu | sed 's/^color_sel_bg = //')
MMENU_COLOR_SEL_FG_DARK=$(grep color_sel_fg $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu | sed 's/^color_sel_fg = //')
MMENU_COLOR_SEL_BORDER_DARK=$(grep color_sel_border $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu | sed 's/^color_sel_border = //')
MMENU_COLOR_SEP_FG_DARK=$(grep color_sep_fg $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu | sed 's/^color_sep_fg = //')
MMENU_COLOR_SCROLL_IND_DARK=$(grep color_scroll_ind $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu | sed 's/^color_scroll_ind = //')
MMENU_COLOR_SEP_MARKUP_DARK=$(grep sep_markup $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu | sed 's/.*foreground=//' | cut -d '"' -f 2)
MMENU_COLOR_RECT_DARK=$(grep '@rect' $HOME/.config/openbox/jgmenu-theme-configs/dark-prepend.csv | sed 's/\,/ /g' | awk '{print $9, $10}')
MMENU_COLOR_SEARCH_DARK=$(grep '@search' $HOME/.config/openbox/jgmenu-theme-configs/dark-prepend.csv | sed 's/\,/ /g' | awk '{print $9, $10}')

### RIGHT-CLICK MENU VARIABLES

RCMENU_COLOR_MENU_BG_LIGHT=$(grep color_menu_bg $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc | sed 's/^color_menu_bg = //')
RCMENU_COLOR_MENU_BORDER_LIGHT=$(grep color_menu_border $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc | sed 's/^color_menu_border = //')
RCMENU_COLOR_NORM_BG_LIGHT=$(grep color_norm_bg $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc | sed 's/^color_norm_bg = //')
RCMENU_COLOR_NORM_FG_LIGHT=$(grep color_norm_fg $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc | sed 's/^color_norm_fg = //')
RCMENU_COLOR_SEL_BG_LIGHT=$(grep color_sel_bg $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc | sed 's/^color_sel_bg = //')
RCMENU_COLOR_SEL_FG_LIGHT=$(grep color_sel_fg $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc | sed 's/^color_sel_fg = //')
RCMENU_COLOR_SEL_BORDER_LIGHT=$(grep color_sel_border $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc | sed 's/^color_sel_border = //')
RCMENU_COLOR_SEP_FG_LIGHT=$(grep color_sep_fg $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc | sed 's/^color_sep_fg = //')
RCMENU_COLOR_SCROLL_IND_LIGHT=$(grep color_scroll_ind $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc | sed 's/^color_scroll_ind = //')
RCMENU_COLOR_SEP_MARKUP_LIGHT=$(grep sep_markup $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc | sed 's/.*foreground=//' | cut -d '"' -f 2)

RCMENU_COLOR_MENU_BG_DARK=$(grep color_menu_bg $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc | sed 's/^color_menu_bg = //')
RCMENU_COLOR_MENU_BORDER_DARK=$(grep color_menu_border $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc | sed 's/^color_menu_border = //')
RCMENU_COLOR_NORM_BG_DARK=$(grep color_norm_bg $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc | sed 's/^color_norm_bg = //')
RCMENU_COLOR_NORM_FG_DARK=$(grep color_norm_fg $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc | sed 's/^color_norm_fg = //')
RCMENU_COLOR_SEL_BG_DARK=$(grep color_sel_bg $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc | sed 's/^color_sel_bg = //')
RCMENU_COLOR_SEL_FG_DARK=$(grep color_sel_fg $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc | sed 's/^color_sel_fg = //')
RCMENU_COLOR_SEL_BORDER_DARK=$(grep color_sel_border $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc | sed 's/^color_sel_border = //')
RCMENU_COLOR_SEP_FG_DARK=$(grep color_sep_fg $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc | sed 's/^color_sep_fg = //')
RCMENU_COLOR_SCROLL_IND_DARK=$(grep color_scroll_ind $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc | sed 's/^color_scroll_ind = //')
RCMENU_COLOR_SEP_MARKUP_DARK=$(grep sep_markup $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc | sed 's/.*foreground=//' | cut -d '"' -f 2)

### PANEL MENUS VARIABLES

PMENUS_COLOR_SEP_MARKUP=$(grep sep_markup $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc | sed 's/.*foreground=//' | cut -d '"' -f 2)
PMENUS_COLOR_MENU_BG=$(grep color_menu_bg $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc | sed 's/^color_menu_bg = //')
PMENUS_COLOR_MENU_BORDER=$(grep color_menu_border $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc | sed 's/^color_menu_border = //')
PMENUS_COLOR_NORM_BG=$(grep color_norm_bg $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc | sed 's/^color_norm_bg = //')
PMENUS_COLOR_NORM_FG=$(grep color_norm_fg $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc | sed 's/^color_norm_fg = //')
PMENUS_COLOR_SEL_BG=$(grep color_sel_bg $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc | sed 's/^color_sel_bg = //')
PMENUS_COLOR_SEL_FG=$(grep color_sel_fg $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc | sed 's/^color_sel_fg = //')
PMENUS_COLOR_SEL_BORDER=$(grep color_sel_border $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc | sed 's/^color_sel_border = //')
PMENUS_COLOR_SEP_FG=$(grep color_sep_fg $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc | sed 's/^color_sep_fg = //')
PMENUS_COLOR_SCROLL_IND=$(grep color_scroll_ind $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc | sed 's/^color_scroll_ind = //')

### SIDE MENU VARIABLES

SMENU_COLOR_MENU_BG_LIGHT=$(grep color_menu_bg $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc | sed 's/^color_menu_bg = //')
SMENU_COLOR_MENU_BORDER_LIGHT=$(grep color_menu_border $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc | sed 's/^color_menu_border = //')
SMENU_COLOR_NORM_BG_LIGHT=$(grep color_norm_bg $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc | sed 's/^color_norm_bg = //')
SMENU_COLOR_NORM_FG_LIGHT=$(grep color_norm_fg $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc | sed 's/^color_norm_fg = //')
SMENU_COLOR_SEL_BG_LIGHT=$(grep color_sel_bg $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc | sed 's/^color_sel_bg = //')
SMENU_COLOR_SEL_FG_LIGHT=$(grep color_sel_fg $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc | sed 's/^color_sel_fg = //')
SMENU_COLOR_SEL_BORDER_LIGHT=$(grep color_sel_border $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc | sed 's/^color_sel_border = //')
SMENU_COLOR_SEP_FG_LIGHT=$(grep color_sep_fg $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc | sed 's/^color_sep_fg = //')
SMENU_COLOR_SCROLL_IND_LIGHT=$(grep color_scroll_ind $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc | sed 's/^color_scroll_ind = //')
SMENU_COLOR_SEP_MARKUP_LIGHT=$(grep sep_markup $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc | sed 's/.*foreground=//' | cut -d '"' -f 2)

SMENU_COLOR_MENU_BG_DARK=$(grep color_menu_bg $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc | sed 's/^color_menu_bg = //')
SMENU_COLOR_MENU_BORDER_DARK=$(grep color_menu_border $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc | sed 's/^color_menu_border = //')
SMENU_COLOR_NORM_BG_DARK=$(grep color_norm_bg $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc | sed 's/^color_norm_bg = //')
SMENU_COLOR_NORM_FG_DARK=$(grep color_norm_fg $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc | sed 's/^color_norm_fg = //')
SMENU_COLOR_SEL_BG_DARK=$(grep color_sel_bg $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc | sed 's/^color_sel_bg = //')
SMENU_COLOR_SEL_FG_DARK=$(grep color_sel_fg $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc | sed 's/^color_sel_fg = //')
SMENU_COLOR_SEL_BORDER_DARK=$(grep color_sel_border $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc | sed 's/^color_sel_border = //')
SMENU_COLOR_SEP_FG_DARK=$(grep color_sep_fg $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc | sed 's/^color_sep_fg = //')
SMENU_COLOR_SCROLL_IND_DARK=$(grep color_scroll_ind $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc | sed 's/^color_scroll_ind = //')
SMENU_COLOR_SEP_MARKUP_DARK=$(grep sep_markup $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc | sed 's/.*foreground=//' | cut -d '"' -f 2)

MENU_RECT=$(grep '@rect' $HOME/.config/openbox/jgmenu-theme-configs/light-prepend.csv | awk '{print $2, $3}' | sed 's/.*#//')
MENU_SEARCH=$(grep '@search' $HOME/.config/openbox/jgmenu-theme-configs/light-prepend.csv | awk '{print $2, $3}' | sed 's/.*#//')

### DARM MODE VARIABLE

MODE_TEST=$(grep SIDEMENU_RC $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)
DARK_MODE_STATUS=$(grep DARK_MODE_STATUS $HOME/.config/darkmode-settings/config | cut -d '"' -f 2)

#########################    GLOBAL SETTINGS    ######################################################################################################

### GLOBAL THEME - TINT2 PANEL

### MATCHA SEA - LIGHT AND DARK
LIGHT_ACCENTS_DEF_FIND="#2f9b85"
LIGHT_BUTTONS_DEF_FIND="#f7f7f7"
LIGHT_TASKBAR_DEF_FIND="#c2c0c0"
LIGHT_FONT_DEF_GENERAL_FIND="#585858"
LIGHT_TASKBAR_BUTTONS_FONT_DEF_FIND="#828282"

DARK_ACCENTS_DEF_FIND="#2f9b85"
DARK_BUTTONS_DEF_FIND="#222b2e"
DARK_TASKBAR_DEF_FIND="#1b2224"
DARK_FONT_DEF_GENERAL_FIND="#f2f2f2"
DARK_TASKBAR_BUTTONS_FONT_DEF_FIND="#b1b1b1"

### MATCHA ALIZ - LIGHT AND DARK
LIGHT_ACCENTS_DEF_FIND="#2f9b85"
LIGHT_BUTTONS_DEF_FIND="#f7f7f7"
LIGHT_TASKBAR_DEF_FIND="#c2c0c0"
LIGHT_FONT_DEF_GENERAL_FIND="#585858"
LIGHT_TASKBAR_BUTTONS_FONT_DEF_FIND="#828282"

DARK_ACCENTS_DEF_FIND="#2f9b85"
DARK_BUTTONS_DEF_FIND="#222b2e"
DARK_TASKBAR_DEF_FIND="#1b2224"
DARK_FONT_DEF_GENERAL_FIND="#f2f2f2"
DARK_TASKBAR_BUTTONS_FONT_DEF_FIND="#b1b1b1"

### MATCHA AZUL - LIGHT AND DARK
LIGHT_ACCENTS_DEF_FIND="#2f9b85"
LIGHT_BUTTONS_DEF_FIND="#f7f7f7"
LIGHT_TASKBAR_DEF_FIND="#c2c0c0"
LIGHT_FONT_DEF_GENERAL_FIND="#585858"
LIGHT_TASKBAR_BUTTONS_FONT_DEF_FIND="#828282"

DARK_ACCENTS_DEF_FIND="#2f9b85"
DARK_BUTTONS_DEF_FIND="#222b2e"
DARK_TASKBAR_DEF_FIND="#1b2224"
DARK_FONT_DEF_GENERAL_FIND="#f2f2f2"
DARK_TASKBAR_BUTTONS_FONT_DEF_FIND="#b1b1b1"

### GLOBAL THEMES - JGMENU

### MATCHA SEA - LIGHT AND DARK
### MAIN MENU
SEA_MMENU_DEF_SEP_MARKUP_LIGHT="#72717d"
SEA_MMENU_DEF_RECT_LIGHT="#9594a1"
SEA_MMENU_DEF_SEARCH_LIGHT="#9594a1"
SEA_MMENU_DEF_SEP_MARKUP_DARK="#8d8d9e"
SEA_MMENU_DEF_RECT_DARK="#504e65"
SEA_MMENU_DEF_SEARCH_DARK="#aaa9bc"

### RIGHT CLICK MENU
SEA_RCMENU_DEF_SEP_MARKUP_LIGHT="#8d8d9e"
SEA_RCMENU_DEF_SEP_MARKUP_DARK="#8d8d9e"

### PANEL MENUS
SEA_PMENUS_DEF_SEP_MARKUP="#a8d5cc"

### SIDE MENUS
SEA_SMENU_DEF_SEP_MARKUP_LIGHT="#868686"
SEA_SMENU_DEF_SEP_MARKUP_DARK="#8d8d9e"

### MATCHA ALIZ - LIGHT AND DARK
### MAIN MENU
ALIZ_MMENU_DEF_SEP_MARKUP_LIGHT="#72717d"
ALIZ_MMENU_DEF_RECT_LIGHT="#9594a1"
ALIZ_MMENU_DEF_SEARCH_LIGHT="#9594a1"
ALIZ_MMENU_DEF_SEP_MARKUP_DARK="#8d8d9e"
ALIZ_MMENU_DEF_RECT_DARK="#504e65"
ALIZ_MMENU_DEF_SEARCH_DARK="#aaa9bc"

### RIGHT CLICK MENU
ALIZ_RCMENU_DEF_SEP_MARKUP_LIGHT="#8d8d9e"
ALIZ_RCMENU_DEF_SEP_MARKUP_DARK="#8d8d9e"

### PANEL MENUS
ALIZ_PMENUS_DEF_SEP_MARKUP="#a8d5cc"

### SIDE MENUS
ALIZ_SMENU_DEF_SEP_MARKUP_LIGHT="#868686"
ALIZ_SMENU_DEF_SEP_MARKUP_DARK="#8d8d9e"

### MATCHA AZUL - LIGHT AND DARK
### MAIN MENU
AZUL_MMENU_DEF_SEP_MARKUP_LIGHT="#72717d"
AZUL_MMENU_DEF_RECT_LIGHT="#9594a1"
AZUL_MMENU_DEF_SEARCH_LIGHT="#9594a1"
AZUL_MMENU_DEF_SEP_MARKUP_DARK="#8d8d9e"
AZUL_MMENU_DEF_RECT_DARK="#504e65"
AZUL_MMENU_DEF_SEARCH_DARK="#aaa9bc"

### RIGHT CLICK MENU
AZUL_RCMENU_DEF_SEP_MARKUP_LIGHT="#8d8d9e"
AZUL_RCMENU_DEF_SEP_MARKUP_DARK="#8d8d9e"

### PANEL MENUS
AZUL_PMENUS_DEF_SEP_MARKUP="#a8d5cc"

### SIDE MENUS
AZUL_SMENU_DEF_SEP_MARKUP_LIGHT="#868686"
AZUL_SMENU_DEF_SEP_MARKUP_DARK="#8d8d9e"

### TINT2 PANEL #######################################################################################################################################

TINT2_SCHEME_START=$(grep -n '# Background 1' "$TINT2_PANEL_ACTIVE_PATH" | cut -d ':' -f 1 | head -n 1)
TINT2_SCHEME_END=$(expr $(grep -n '# Panel' "$TINT2_PANEL_ACTIVE_PATH" | cut -d ':' -f 1 | tail -n 1) - 3)

### TINT2 PANEL - MATCHA SEA

TINT2_PANEL_MATCHA_SEA_SCHEME() {

SEA_LIGHT_PANEL_SCHEME=$(mktemp)
trap "rm -f ${SEA_LIGHT_PANEL_SCHEME}" EXIT
cat << 'EOF' > ${SEA_LIGHT_PANEL_SCHEME}
# Background 1: 
rounded = 0
border_width = 1
border_sides = T
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #000000 80
border_color = #333333 80
gradient_id = 1
background_color_hover = #000000 80
border_color_hover = #555555 80
background_color_pressed = #000000 80
border_color_pressed = #555555 80

# Background 2: Button, Clock
rounded = 0
border_width = 0
border_sides = B
border_content_tint_weight = 100
background_content_tint_weight = 0
background_color = #f7f7f7 100
border_color = #f7f7f7 100
background_color_hover = #95cabf 100
border_color_hover = #95cabf 100
background_color_pressed = #60b7a5 100
border_color_pressed = #60b7a5 100
gradient_id_pressed = 0

# Background 3: 
rounded = 0
border_width = 2
border_sides = B
border_content_tint_weight = 100
background_content_tint_weight = 100
background_color = #ffffff 100
border_color = #d9d9d9 100
background_color_hover = #ffffff 73
border_color_hover = #d9d9d9 100
background_color_pressed = #989898 73
border_color_pressed = #d9d9d9 100

# Background 4: Urgent task
rounded = 0
border_width = 0
border_sides = TBLR
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #2f9b85 100
border_color = #2f9b85 100
background_color_hover = #2f9b85 100
border_color_hover = #2f9b85 100
background_color_pressed = #2f9b85 100
border_color_pressed = #2f9b85 100

# Background 5: Active task, Tooltip
rounded = 0
border_width = 1
border_sides = TBLR
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #2f9b85 100
border_color = #2f9b85 100
background_color_hover = #2f9b85 100
border_color_hover = #2f9b85 100
background_color_pressed = #2f9b85 100
border_color_pressed = #2f9b85 100

# Background 6: Inactive desktop name
rounded = 0
border_width = 2
border_sides = B
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #777777 0
border_color = #777777 0
background_color_hover = #bdbdbd 21
border_color_hover = #cccccc 100
background_color_pressed = #777777 21
border_color_pressed = #777777 100

# Background 7: Panel
rounded = 0
border_width = 2
border_sides = B
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #f7f7f7 100
border_color = #2f9b85 100
background_color_hover = #ffffff 21
border_color_hover = #f7f7f7 100
background_color_pressed = #a9a9a9 21
border_color_pressed = #f7f7f7 100

# Background 8: Active taskbar, Default task, Iconified task, Systray
rounded = 0
border_width = 0
border_sides = 
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #c2c0c0 100
border_color = #2f9b85 100
background_color_hover = #2f9b85 100
border_color_hover = #2f9b85 100
background_color_pressed = #2f9b85 21
border_color_pressed = #2f9b85 21
EOF

SEA_DARK_PANEL_SCHEME=$(mktemp)
trap "rm -f ${SEA_DARK_PANEL_SCHEME}" EXIT
cat << 'EOF' > ${SEA_DARK_PANEL_SCHEME}
# Background 1: 
rounded = 0
border_width = 1
border_sides = T
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #000000 80
border_color = #333333 80
gradient_id = 1
background_color_hover = #000000 80
border_color_hover = #555555 80
background_color_pressed = #000000 80
border_color_pressed = #555555 80

# Background 2: Button, Clock
rounded = 0
border_width = 0
border_sides = B
border_content_tint_weight = 100
background_content_tint_weight = 0
background_color = #222b2e 100
border_color = #222b2e 100
background_color_hover = #2f9b85 100
border_color_hover = #2f9b85 100
background_color_pressed = #2f9b85 47
border_color_pressed = #2f9b85 47
gradient_id_pressed = 0

# Background 3: 
rounded = 0
border_width = 2
border_sides = B
border_content_tint_weight = 100
background_content_tint_weight = 100
background_color = #ffffff 100
border_color = #d9d9d9 100
background_color_hover = #ffffff 73
border_color_hover = #d9d9d9 100
background_color_pressed = #989898 73
border_color_pressed = #d9d9d9 100

# Background 4: Urgent task
rounded = 0
border_width = 0
border_sides = TBLR
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #2f9b85 100
border_color = #2f9b85 100
background_color_hover = #2f9b85 100
border_color_hover = #2f9b85 100
background_color_pressed = #2f9b85 100
border_color_pressed = #2f9b85 100

# Background 5: Active task, Tooltip
rounded = 0
border_width = 1
border_sides = TBLR
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #2f9b85 100
border_color = #2f9b85 100
background_color_hover = #2f9b85 100
border_color_hover = #2f9b85 100
background_color_pressed = #2f9b85 100
border_color_pressed = #2f9b85 100

# Background 6: Inactive desktop name
rounded = 0
border_width = 2
border_sides = B
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #777777 0
border_color = #777777 0
background_color_hover = #bdbdbd 21
border_color_hover = #cccccc 100
background_color_pressed = #777777 21
border_color_pressed = #777777 100

# Background 7: Panel
rounded = 0
border_width = 2
border_sides = B
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #222b2e 100
border_color = #2f9b85 100
background_color_hover = #ffffff 21
border_color_hover = #d9d9d9 100
background_color_pressed = #a9a9a9 21
border_color_pressed = #d9d9d9 100

# Background 8: Active taskbar, Default task, Iconified task, Systray
rounded = 0
border_width = 0
border_sides = 
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #1b2224 100
border_color = #2f9b85 100
background_color_hover = #2f9b85 100
border_color_hover = #2f9b85 100
background_color_pressed = #2f9b85 21
border_color_pressed = #2f9b85 21
EOF

	### removing lines with color scheme from config files
	sed -i ""$TINT2_SCHEME_START","$TINT2_SCHEME_END"d" "$TINT2_PANEL_DAILY_LIGHT_PATH" "$TINT2_PANEL_DAILY_DARK_PATH" "$TINT2_PANEL_STUDIO_LIGHT_PATH" "$TINT2_PANEL_STUDIO_DARK_PATH" "$TINT2_PANEL_HIDDEN_PATH" "$TINT2_PANEL_ACTIVE_PATH"

	### merging all lines into one
	sed -i 's/$/\\n/g' ${SEA_LIGHT_PANEL_SCHEME} ${SEA_DARK_PANEL_SCHEME}
	sed -i -e :a -e N -e 's/\n//' -e ta ${SEA_LIGHT_PANEL_SCHEME} ${SEA_DARK_PANEL_SCHEME}

	### pastin' new color scheme into config files
	sed -i "/# Backgrounds/a $(cat ${SEA_LIGHT_PANEL_SCHEME})" "$TINT2_PANEL_DAILY_LIGHT_PATH" "$TINT2_PANEL_STUDIO_LIGHT_PATH" "$TINT2_PANEL_HIDDEN_PATH"
	sed -i "$(expr $(grep -n '# Panel' "$TINT2_PANEL_ACTIVE_PATH" | cut -d ':' -f 1 | tail -n 1) - 2)d" "$TINT2_PANEL_DAILY_LIGHT_PATH" "$TINT2_PANEL_STUDIO_LIGHT_PATH" "$TINT2_PANEL_HIDDEN_PATH"
	sed -i "/# Backgrounds/a $(cat ${SEA_DARK_PANEL_SCHEME})" "$TINT2_PANEL_DAILY_DARK_PATH" "$TINT2_PANEL_STUDIO_DARK_PATH"
	sed -i "$(expr $(grep -n '# Panel' "$TINT2_PANEL_ACTIVE_PATH" | cut -d ':' -f 1 | tail -n 1) - 2)d" "$TINT2_PANEL_DAILY_DARK_PATH" "$TINT2_PANEL_STUDIO_DARK_PATH"

		if [ "$DARK_MODE_STATUS" = "OFF" ]; then

			sed -i "/# Backgrounds/a $(cat ${SEA_LIGHT_PANEL_SCHEME})" "$TINT2_PANEL_ACTIVE_PATH"
			sed -i "$(expr $(grep -n '# Panel' "$TINT2_PANEL_ACTIVE_PATH" | cut -d ':' -f 1 | tail -n 1) - 2)d""$TINT2_PANEL_ACTIVE_PATH"

		elif [ "$DARK_MODE_STATUS" = "ON" ]; then

			sed -i "/# Backgrounds/a $(cat ${SEA_DARK_PANEL_SCHEME})" "$TINT2_PANEL_ACTIVE_PATH"
			sed -i "$(expr $(grep -n '# Panel' "$TINT2_PANEL_ACTIVE_PATH" | cut -d ':' -f 1 | tail -n 1) - 2)d" "$TINT2_PANEL_ACTIVE_PATH"

			else
	
				echo "error"

		fi

}

### TINT2 PANEL - MATCHA ALIZ

TINT2_PANEL_MATCHA_ALIZ_SCHEME() {

ALIZ_LIGHT_PANEL_SCHEME=$(mktemp)
trap "rm -f ${ALIZ_LIGHT_PANEL_SCHEME}" EXIT
cat << 'EOF' > ${ALIZ_LIGHT_PANEL_SCHEME}
# Background 1: 
rounded = 0
border_width = 1
border_sides = T
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #000000 80
border_color = #333333 80
gradient_id = 1
background_color_hover = #000000 80
border_color_hover = #555555 80
background_color_pressed = #000000 80
border_color_pressed = #555555 80

# Background 2: Button, Clock
rounded = 0
border_width = 0
border_sides = B
border_content_tint_weight = 100
background_content_tint_weight = 0
background_color = #f7f7f7 100
border_color = #f7f7f7 100
background_color_hover = #ca9995 100
border_color_hover = #ca9895 100
background_color_pressed = #b76560 100
border_color_pressed = #b76560 100
gradient_id_pressed = 0

# Background 3: 
rounded = 0
border_width = 2
border_sides = B
border_content_tint_weight = 100
background_content_tint_weight = 100
background_color = #ffffff 100
border_color = #d9d9d9 100
background_color_hover = #ffffff 73
border_color_hover = #d9d9d9 100
background_color_pressed = #989898 73
border_color_pressed = #d9d9d9 100

# Background 4: Urgent task
rounded = 0
border_width = 0
border_sides = TBLR
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #e44138 100
border_color = #e44138 100
background_color_hover = #e44138 100
border_color_hover = #e44138 100
background_color_pressed = #e44138 100
border_color_pressed = #e44138 100

# Background 5: Active task, Tooltip
rounded = 0
border_width = 1
border_sides = TBLR
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #e44138 100
border_color = #e44138 100
background_color_hover = #e44138 100
border_color_hover = #e44138 100
background_color_pressed = #e44138 100
border_color_pressed = #e44138 100

# Background 6: Inactive desktop name
rounded = 0
border_width = 2
border_sides = B
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #777777 0
border_color = #777777 0
background_color_hover = #bdbdbd 21
border_color_hover = #cccccc 100
background_color_pressed = #777777 21
border_color_pressed = #777777 100

# Background 7: Panel
rounded = 0
border_width = 2
border_sides = B
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #f7f7f7 100
border_color = #e44138 100
background_color_hover = #ffffff 21
border_color_hover = #f7f7f7 100
background_color_pressed = #a9a9a9 21
border_color_pressed = #f7f7f7 100

# Background 8: Active taskbar, Default task, Iconified task, Systray
rounded = 0
border_width = 0
border_sides = 
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #c2c0c0 100
border_color = #e44138 100
background_color_hover = #e44138 100
border_color_hover = #e44138 100
background_color_pressed = #e44138 21
border_color_pressed = #e44138 21
EOF

ALIZ_DARK_PANEL_SCHEME=$(mktemp)
trap "rm -f ${ALIZ_DARK_PANEL_SCHEME}" EXIT
cat << 'EOF' > ${ALIZ_DARK_PANEL_SCHEME}
# Background 1: 
rounded = 0
border_width = 1
border_sides = T
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #000000 80
border_color = #333333 80
gradient_id = 1
background_color_hover = #000000 80
border_color_hover = #555555 80
background_color_pressed = #000000 80
border_color_pressed = #555555 80

# Background 2: Button, Clock
rounded = 0
border_width = 0
border_sides = B
border_content_tint_weight = 100
background_content_tint_weight = 0
background_color = #2b2b2b 100
border_color = #2b2b2b 100
background_color_hover = #e44138 100
border_color_hover = #e44138 100
background_color_pressed = #e44138 47
border_color_pressed = #e44138 47
gradient_id_pressed = 0

# Background 3: 
rounded = 0
border_width = 2
border_sides = B
border_content_tint_weight = 100
background_content_tint_weight = 100
background_color = #ffffff 100
border_color = #d9d9d9 100
background_color_hover = #ffffff 73
border_color_hover = #d9d9d9 100
background_color_pressed = #989898 73
border_color_pressed = #d9d9d9 100

# Background 4: Urgent task
rounded = 0
border_width = 0
border_sides = TBLR
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #e44138 100
border_color = #e44138 100
background_color_hover = #e44138 100
border_color_hover = #e44138 100
background_color_pressed = #e44138 100
border_color_pressed = #e44138 100

# Background 5: Active task, Tooltip
rounded = 0
border_width = 1
border_sides = TBLR
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #e44138 100
border_color = #e44138 100
background_color_hover = #e44138 100
border_color_hover = #e44138 100
background_color_pressed = #e44138 100
border_color_pressed = #e44138 100

# Background 6: Inactive desktop name
rounded = 0
border_width = 2
border_sides = B
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #777777 0
border_color = #777777 0
background_color_hover = #bdbdbd 21
border_color_hover = #cccccc 100
background_color_pressed = #777777 21
border_color_pressed = #777777 100

# Background 7: Panel
rounded = 0
border_width = 2
border_sides = B
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #2b2b2b 100
border_color = #e44138 100
background_color_hover = #ffffff 21
border_color_hover = #d9d9d9 100
background_color_pressed = #a9a9a9 21
border_color_pressed = #d9d9d9 100

# Background 8: Active taskbar, Default task, Iconified task, Systray
rounded = 0
border_width = 0
border_sides = 
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #222222 100
border_color = #e44138 100
background_color_hover = #e44138 100
border_color_hover = #e44138 100
background_color_pressed = #e44138 21
border_color_pressed = #e44138 21
EOF

	### removing lines with color scheme from config files
	sed -i ""$TINT2_SCHEME_START","$TINT2_SCHEME_END"d" "$TINT2_PANEL_DAILY_LIGHT_PATH" "$TINT2_PANEL_DAILY_DARK_PATH" "$TINT2_PANEL_STUDIO_LIGHT_PATH" "$TINT2_PANEL_STUDIO_DARK_PATH" "$TINT2_PANEL_HIDDEN_PATH" "$TINT2_PANEL_ACTIVE_PATH"

	### merging all lines into one
	sed -i 's/$/\\n/g' ${ALIZ_LIGHT_PANEL_SCHEME} ${ALIZ_DARK_PANEL_SCHEME}
	sed -i -e :a -e N -e 's/\n//' -e ta ${ALIZ_LIGHT_PANEL_SCHEME} ${ALIZ_DARK_PANEL_SCHEME}

	### pastin' new color scheme into config files
	sed -i "/# Backgrounds/a $(cat ${ALIZ_LIGHT_PANEL_SCHEME})" "$TINT2_PANEL_DAILY_LIGHT_PATH" "$TINT2_PANEL_STUDIO_LIGHT_PATH" "$TINT2_PANEL_HIDDEN_PATH"
	sed -i "$(expr $(grep -n '# Panel' "$TINT2_PANEL_ACTIVE_PATH" | cut -d ':' -f 1 | tail -n 1) - 2)d" "$TINT2_PANEL_DAILY_LIGHT_PATH" "$TINT2_PANEL_STUDIO_LIGHT_PATH" "$TINT2_PANEL_HIDDEN_PATH"
	sed -i "/# Backgrounds/a $(cat ${ALIZ_DARK_PANEL_SCHEME})" "$TINT2_PANEL_DAILY_DARK_PATH" "$TINT2_PANEL_STUDIO_DARK_PATH"
	sed -i "$(expr $(grep -n '# Panel' "$TINT2_PANEL_ACTIVE_PATH" | cut -d ':' -f 1 | tail -n 1) - 2)d" "$TINT2_PANEL_DAILY_DARK_PATH" "$TINT2_PANEL_STUDIO_DARK_PATH"

		if [ "$DARK_MODE_STATUS" = "OFF" ]; then

			sed -i "/# Backgrounds/a $(cat ${ALIZ_LIGHT_PANEL_SCHEME})" "$TINT2_PANEL_ACTIVE_PATH"
			sed -i "$(expr $(grep -n '# Panel' "$TINT2_PANEL_ACTIVE_PATH" | cut -d ':' -f 1 | tail -n 1) - 2)d" "$TINT2_PANEL_ACTIVE_PATH"

		elif [ "$DARK_MODE_STATUS" = "ON" ]; then

			sed -i "/# Backgrounds/a $(cat ${ALIZ_DARK_PANEL_SCHEME})" "$TINT2_PANEL_ACTIVE_PATH"
			sed -i "$(expr $(grep -n '# Panel' "$TINT2_PANEL_ACTIVE_PATH" | cut -d ':' -f 1 | tail -n 1) - 2)d" "$TINT2_PANEL_ACTIVE_PATH"

			else
	
				echo "error"

		fi

}

### TINT2 PANEL - MATCHA AZUL

TINT2_PANEL_MATCHA_AZUL_SCHEME() {

AZUL_LIGHT_PANEL_SCHEME=$(mktemp)
trap "rm -f ${AZUL_LIGHT_PANEL_SCHEME}" EXIT
cat << 'EOF' > ${AZUL_LIGHT_PANEL_SCHEME}
# Background 1: 
rounded = 0
border_width = 1
border_sides = T
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #000000 80
border_color = #333333 80
gradient_id = 1
background_color_hover = #000000 80
border_color_hover = #555555 80
background_color_pressed = #000000 80
border_color_pressed = #555555 80

# Background 2: Button, Clock
rounded = 0
border_width = 0
border_sides = B
border_content_tint_weight = 100
background_content_tint_weight = 0
background_color = #f2f2f2 100
border_color = #f2f2f2 100
background_color_hover = #95b3ca 100
border_color_hover = #95b3ca 100
background_color_pressed = #6093b7 100
border_color_pressed = #6093b7 100
gradient_id_pressed = 0

# Background 3: 
rounded = 0
border_width = 2
border_sides = B
border_content_tint_weight = 100
background_content_tint_weight = 100
background_color = #ffffff 100
border_color = #d9d9d9 100
background_color_hover = #ffffff 73
border_color_hover = #d9d9d9 100
background_color_pressed = #989898 73
border_color_pressed = #d9d9d9 100

# Background 4: Urgent task
rounded = 0
border_width = 0
border_sides = TBLR
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #3498db 100
border_color = #3498db 100
background_color_hover = #3498db 100
border_color_hover = #3498db 100
background_color_pressed = #3498db 100
border_color_pressed = #3498db 100

# Background 5: Active task, Tooltip
rounded = 0
border_width = 1
border_sides = TBLR
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #3498db 100
border_color = #3498db 100
background_color_hover = #3498db 100
border_color_hover = #3498db 100
background_color_pressed = #3498db 100
border_color_pressed = #3498db 100

# Background 6: Inactive desktop name
rounded = 0
border_width = 2
border_sides = B
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #777777 0
border_color = #777777 0
background_color_hover = #bdbdbd 21
border_color_hover = #cccccc 100
background_color_pressed = #777777 21
border_color_pressed = #777777 100

# Background 7: Panel
rounded = 0
border_width = 2
border_sides = B
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #f2f2f2 100
border_color = #3498db 100
background_color_hover = #ffffff 21
border_color_hover = #f2f2f2 100
background_color_pressed = #a9a9a9 21
border_color_pressed = #f2f2f2 100

# Background 8: Active taskbar, Default task, Iconified task, Systray
rounded = 0
border_width = 0
border_sides = 
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #c2c0c0 100
border_color = #3498db 100
background_color_hover = #3498db 100
border_color_hover = #3498db 100
background_color_pressed = #3498db 21
border_color_pressed = #3498db 21
EOF

AZUL_DARK_PANEL_SCHEME=$(mktemp)
trap "rm -f ${AZUL_DARK_PANEL_SCHEME}" EXIT
cat << 'EOF' > ${AZUL_DARK_PANEL_SCHEME}
# Background 1: 
rounded = 0
border_width = 1
border_sides = T
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #000000 80
border_color = #333333 80
gradient_id = 1
background_color_hover = #000000 80
border_color_hover = #555555 80
background_color_pressed = #000000 80
border_color_pressed = #555555 80

# Background 2: Button, Clock
rounded = 0
border_width = 0
border_sides = B
border_content_tint_weight = 100
background_content_tint_weight = 0
background_color = #22252c 100
border_color = #22252c 100
background_color_hover = #3498db 100
border_color_hover = #3498db 100
background_color_pressed = #3498db 47
border_color_pressed = #3498db 47
gradient_id_pressed = 0

# Background 3: 
rounded = 0
border_width = 2
border_sides = B
border_content_tint_weight = 100
background_content_tint_weight = 100
background_color = #ffffff 100
border_color = #d9d9d9 100
background_color_hover = #ffffff 73
border_color_hover = #d9d9d9 100
background_color_pressed = #989898 73
border_color_pressed = #d9d9d9 100

# Background 4: Urgent task
rounded = 0
border_width = 0
border_sides = TBLR
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #3498db 100
border_color = #3498db 100
background_color_hover = #3498db 100
border_color_hover = #3498db 100
background_color_pressed = #3498db 100
border_color_pressed = #3498db 100

# Background 5: Active task, Tooltip
rounded = 0
border_width = 1
border_sides = TBLR
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #3498db 100
border_color = #3498db 100
background_color_hover = #3498db 100
border_color_hover = #3498db 100
background_color_pressed = #3498db 100
border_color_pressed = #3498db 100

# Background 6: Inactive desktop name
rounded = 0
border_width = 2
border_sides = B
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #777777 0
border_color = #777777 0
background_color_hover = #bdbdbd 21
border_color_hover = #cccccc 100
background_color_pressed = #777777 21
border_color_pressed = #777777 100

# Background 7: Panel
rounded = 0
border_width = 2
border_sides = B
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #22252c 100
border_color = #3498db 100
background_color_hover = #ffffff 21
border_color_hover = #d9d9d9 100
background_color_pressed = #a9a9a9 21
border_color_pressed = #d9d9d9 100

# Background 8: Active taskbar, Default task, Iconified task, Systray
rounded = 0
border_width = 0
border_sides = 
border_content_tint_weight = 0
background_content_tint_weight = 0
background_color = #1b1d24 100
border_color = #3498db 100
background_color_hover = #3498db 100
border_color_hover = #3498db 100
background_color_pressed = #3498db 21
border_color_pressed = #3498db 21
EOF

	### removing lines with color scheme from config files
	sed -i ""$TINT2_SCHEME_START","$TINT2_SCHEME_END"d" "$TINT2_PANEL_DAILY_LIGHT_PATH" "$TINT2_PANEL_DAILY_DARK_PATH" "$TINT2_PANEL_STUDIO_LIGHT_PATH" "$TINT2_PANEL_STUDIO_DARK_PATH" "$TINT2_PANEL_HIDDEN_PATH" "$TINT2_PANEL_ACTIVE_PATH"

	### merging all lines into one
	sed -i 's/$/\\n/g' ${AZUL_LIGHT_PANEL_SCHEME} ${AZUL_DARK_PANEL_SCHEME}
	sed -i -e :a -e N -e 's/\n//' -e ta ${AZUL_LIGHT_PANEL_SCHEME} ${AZUL_DARK_PANEL_SCHEME}

	### pastin' new color scheme into config files
	sed -i "/# Backgrounds/a $(cat ${AZUL_LIGHT_PANEL_SCHEME})" "$TINT2_PANEL_DAILY_LIGHT_PATH" "$TINT2_PANEL_STUDIO_LIGHT_PATH" "$TINT2_PANEL_HIDDEN_PATH"
	sed -i "$(expr $(grep -n '# Panel' "$TINT2_PANEL_ACTIVE_PATH" | cut -d ':' -f 1 | tail -n 1) - 2)d" "$TINT2_PANEL_DAILY_LIGHT_PATH" "$TINT2_PANEL_STUDIO_LIGHT_PATH" "$TINT2_PANEL_HIDDEN_PATH"
	sed -i "/# Backgrounds/a $(cat ${AZUL_DARK_PANEL_SCHEME})" "$TINT2_PANEL_DAILY_DARK_PATH" "$TINT2_PANEL_STUDIO_DARK_PATH"
	sed -i "$(expr $(grep -n '# Panel' "$TINT2_PANEL_ACTIVE_PATH" | cut -d ':' -f 1 | tail -n 1) - 2)d" "$TINT2_PANEL_DAILY_DARK_PATH" "$TINT2_PANEL_STUDIO_DARK_PATH"

		if [ "$DARK_MODE_STATUS" = "OFF" ]; then

			sed -i "/# Backgrounds/a $(cat ${AZUL_LIGHT_PANEL_SCHEME})" "$TINT2_PANEL_ACTIVE_PATH"
			sed -i "$(expr $(grep -n '# Panel' "$TINT2_PANEL_ACTIVE_PATH" | cut -d ':' -f 1 | tail -n 1) - 2)d" "$TINT2_PANEL_ACTIVE_PATH"

		elif [ "$DARK_MODE_STATUS" = "ON" ]; then

			sed -i "/# Backgrounds/a $(cat ${AZUL_DARK_PANEL_SCHEME})" "$TINT2_PANEL_ACTIVE_PATH"
			sed -i "$(expr $(grep -n '# Panel' "$TINT2_PANEL_ACTIVE_PATH" | cut -d ':' -f 1 | tail -n 1) - 2)d" "$TINT2_PANEL_ACTIVE_PATH"

			else
	
				echo "error"

		fi

}

### MAIN MENU #########################################################################################################################################

MMENU_SCHEME_START=$(grep -n 'color_menu_bg' "$ACTIVE_MAIN_MENU_PATH" | cut -d ':' -f 1 | head -n 1)
MMENU_SCHEME_END=$(grep -n 'color_scroll_ind' "$ACTIVE_MAIN_MENU_PATH" | cut -d ':' -f 1 | tail -n 1)

### MMENU - MATCHA SEA

MMENU_MATCHA_SEA_SCHEME() {

SEA_LIGHT_MMENU_SCHEME=$(mktemp)
trap "rm -f ${SEA_LIGHT_MMENU_SCHEME}" EXIT
cat << 'EOF' > ${SEA_LIGHT_MMENU_SCHEME}
color_menu_bg = #f7f7f7 100
color_menu_border = #504e65 100
color_norm_bg = #f7f7f7 100
color_norm_fg = #272625 100
color_sel_bg = #2f9b85 100
color_sel_fg = #f2f2f2 100
color_sel_border = #f7f7f7 100
color_sep_fg = #f7f7f7 100
color_scroll_ind = #868686 100
EOF

SEA_DARK_MMENU_SCHEME=$(mktemp)
trap "rm -f ${SEA_DARK_MMENU_SCHEME}" EXIT
cat << 'EOF' > ${SEA_DARK_MMENU_SCHEME}
color_menu_bg = #1b2224 100
color_menu_border = #504e65 100
color_norm_bg = #1b2224 100
color_norm_fg = #f8f8f8 100
color_sel_bg = #2f9b85 100
color_sel_fg = #f8f8f8 100
color_sel_border = #1b2224 100
color_sep_fg = #1b2224 100
color_scroll_ind = #383745 100
EOF

	### removing lines with color scheme from config files
	sed -i ""$MMENU_SCHEME_START","$MMENU_SCHEME_END"d" "$LIGHT_MAIN_MENU_PATH" "$DARK_MAIN_MENU_PATH" "$ACTIVE_MAIN_MENU_PATH"

	### merging all lines into one
	sed -i 's/$/\\n/g' ${SEA_LIGHT_MMENU_SCHEME} ${SEA_DARK_MMENU_SCHEME}
	sed -i -e :a -e N -e 's/\n//' -e ta ${SEA_LIGHT_MMENU_SCHEME} ${SEA_DARK_MMENU_SCHEME}

	### pastin' new color scheme into config files
	### light scheme
	sed -i "/arrow_width/a $(cat ${SEA_LIGHT_MMENU_SCHEME})" "$LIGHT_MAIN_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$LIGHT_MAIN_MENU_PATH"
	sed -i "s/foreground=\"$MMENU_COLOR_SEP_MARKUP_LIGHT\"/foreground=\"$SEA_MMENU_DEF_SEP_MARKUP_LIGHT\"/" "$LIGHT_MAIN_MENU_PATH"
	sed -i "s/$MMENU_COLOR_RECT_LIGHT,#$MENU_RECT/$SEA_MMENU_DEF_RECT_LIGHT,#$MENU_RECT/" "$LIGHT_MAIN_MENU_PREPEND_PATH"
	sed -i "s/$MMENU_COLOR_SEARCH_LIGHT,#$MENU_SEARCH/$SEA_MMENU_DEF_SEARCH_LIGHT,#$MENU_SEARCH/" "$LIGHT_MAIN_MENU_PREPEND_PATH"
	### dark scheme
	sed -i "/arrow_width/a $(cat ${SEA_DARK_MMENU_SCHEME})" "$DARK_MAIN_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$DARK_MAIN_MENU_PATH"
	sed -i "s/foreground=\"$MMENU_COLOR_SEP_MARKUP_DARK\"/foreground=\"$SEA_MMENU_DEF_SEP_MARKUP_DARK\"/" "$DARK_MAIN_MENU_PATH"
	sed -i "s/$MMENU_COLOR_RECT_DARK.#$MENU_RECT/$SEA_MMENU_DEF_RECT_DARK.#$MENU_RECT/" "$DARK_MAIN_MENU_PREPEND_PATH"
	sed -i "s/$MMENU_COLOR_SEARCH_DARK,#$MENU_SEARCH/$SEA_MMENU_DEF_SEARCH_DARK,#$MENU_SEARCH/" "$DARK_MAIN_MENU_PREPEND_PATH"

		if [ $DARK_MODE_STATUS = "OFF" ]; then

			sed -i "/arrow_width/a $(cat ${SEA_LIGHT_MMENU_SCHEME})" "$ACTIVE_MAIN_MENU_PATH"
			sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$ACTIVE_MAIN_MENU_PATH"
			sed -i "s/foreground=\"$MMENU_COLOR_SEP_MARKUP_LIGHT\"/foreground=\"$SEA_MMENU_DEF_SEP_MARKUP_LIGHT\"/" "$ACTIVE_MAIN_MENU_PATH"
			sed -i "s/$MMENU_COLOR_RECT_LIGHT,#$MENU_RECT/$SEA_MMENU_DEF_RECT_LIGHT,#$MENU_RECT/" "$ACTIVE_MAIN_MENU_PREPEND_PATH"
			sed -i "s/$MMENU_COLOR_SEARCH_LIGHT,#$MENU_SEARCH/$SEA_MMENU_DEF_SEARCH_LIGHT,#$MENU_SEARCH/" "$ACTIVE_MAIN_MENU_PREPEND_PATH"

		elif [ $DARK_MODE_STATUS = "ON" ]; then

			sed -i "/arrow_width/a $(cat ${SEA_DARK_MMENU_SCHEME})" "$ACTIVE_MAIN_MENU_PATH"
			sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$ACTIVE_MAIN_MENU_PATH"
			sed -i "s/foreground=\"$MMENU_COLOR_SEP_MARKUP_DARK\"/foreground=\"$SEA_MMENU_DEF_SEP_MARKUP_DARK\"/" "$ACTIVE_MAIN_MENU_PATH"
			sed -i "s/$MMENU_COLOR_RECT_DARK,#$MENU_RECT/$SEA_MMENU_DEF_RECT_DARK,#$MENU_RECT/" "$ACTIVE_MAIN_MENU_PREPEND_PATH"
			sed -i "s/$MMENU_COLOR_SEARCH_DARK,#$MENU_SEARCH/$SEA_MMENU_DEF_SEARCH_DARK,#$MENU_SEARCH/" "$ACTIVE_MAIN_MENU_PREPEND_PATH"

		else

			echo "error"

		fi

}

MMENU_MATCHA_ALIZ_SCHEME() {

ALIZ_LIGHT_MMENU_SCHEME=$(mktemp)
trap "rm -f ${ALIZ_LIGHT_MMENU_SCHEME}" EXIT
cat << 'EOF' > ${ALIZ_LIGHT_MMENU_SCHEME}
color_menu_bg = #f7f7f7 100
color_menu_border = #504e65 100
color_norm_bg = #f7f7f7 100
color_norm_fg = #f8f8f8 100
color_sel_bg = #e44138 100
color_sel_fg = #f2f2f2 100
color_sel_border = #f7f7f7 100
color_sep_fg = #f7f7f7 100
color_scroll_ind = #868686 100
EOF

ALIZ_DARK_MMENU_SCHEME=$(mktemp)
trap "rm -f ${ALIZ_DARK_MMENU_SCHEME}" EXIT
cat << 'EOF' > ${ALIZ_DARK_MMENU_SCHEME}
color_menu_bg = #222222 100
color_menu_border = #504e65 100
color_norm_bg = #222222 100
color_norm_fg = #f8f8f8 100
color_sel_bg = #e44138 100
color_sel_fg = #f2f2f2 100
color_sel_border = #222222 100
color_sep_fg = #222222 100
color_scroll_ind = #868686 100
EOF

	### removing lines with color scheme from config files
	sed -i ""$MMENU_SCHEME_START","$MMENU_SCHEME_END"d" "$LIGHT_MAIN_MENU_PATH" "$DARK_MAIN_MENU_PATH" "$ACTIVE_MAIN_MENU_PATH"

	### merging all lines into one
	sed -i 's/$/\\n/g' ${ALIZ_LIGHT_MMENU_SCHEME} ${ALIZ_DARK_MMENU_SCHEME}
	sed -i -e :a -e N -e 's/\n//' -e ta ${ALIZ_LIGHT_MMENU_SCHEME} ${ALIZ_DARK_MMENU_SCHEME}

	### pastin' new color scheme into config files
	### light scheme
	sed -i "/arrow_width/a $(cat ${ALIZ_LIGHT_MMENU_SCHEME})" "$LIGHT_MAIN_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$LIGHT_MAIN_MENU_PATH"
	sed -i "s/foreground=\"$MMENU_COLOR_SEP_MARKUP_LIGHT\"/foreground=\"$ALIZ_MMENU_DEF_SEP_MARKUP_LIGHT\"/" "$LIGHT_MAIN_MENU_PATH"
	sed -i "s/$MMENU_COLOR_RECT_LIGHT,#$MENU_RECT/$ALIZ_MMENU_DEF_RECT_LIGHT,#$MENU_RECT/" "$LIGHT_MAIN_MENU_PREPEND_PATH"
	sed -i "s/$MMENU_COLOR_SEARCH_LIGHT,#$MENU_SEARCH/$ALIZ_MMENU_DEF_SEARCH_LIGHT,#$MENU_SEARCH/" "$LIGHT_MAIN_MENU_PREPEND_PATH"
	### dark scheme
	sed -i "/arrow_width/a $(cat ${ALIZ_DARK_MMENU_SCHEME})" "$DARK_MAIN_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$DARK_MAIN_MENU_PATH"
	sed -i "s/foreground=\"$MMENU_COLOR_SEP_MARKUP_DARK\"/foreground=\"$ALIZ_MMENU_DEF_SEP_MARKUP_DARK\"/" "$DARK_MAIN_MENU_PATH"
	sed -i "s/$MMENU_COLOR_RECT_DARK.#$MENU_RECT/$ALIZ_MMENU_DEF_RECT_DARK.#$MENU_RECT/" "$DARK_MAIN_MENU_PREPEND_PATH"
	sed -i "s/$MMENU_COLOR_SEARCH_DARK,#$MENU_SEARCH/$ALIZ_MMENU_DEF_SEARCH_DARK,#$MENU_SEARCH/" "$DARK_MAIN_MENU_PREPEND_PATH"

		if [ "$DARK_MODE_STATUS" = "OFF" ]; then

			sed -i "/arrow_width/a $(cat ${ALIZ_LIGHT_MMENU_SCHEME})" "$ACTIVE_MAIN_MENU_PATH"
			sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$ACTIVE_MAIN_MENU_PATH"
			sed -i "s/foreground=\"$MMENU_COLOR_SEP_MARKUP_LIGHT\"/foreground=\"$ALIZ_MMENU_DEF_SEP_MARKUP_LIGHT\"/" "$ACTIVE_MAIN_MENU_PATH"
			sed -i "s/$MMENU_COLOR_RECT_LIGHT,#$MENU_RECT/$ALIZ_MMENU_DEF_RECT_LIGHT,#$MENU_RECT/" "$ACTIVE_MAIN_MENU_PREPEND_PATH"
			sed -i "s/$MMENU_COLOR_SEARCH_LIGHT,#$MENU_SEARCH/$ALIZ_MMENU_DEF_SEARCH_LIGHT,#$MENU_SEARCH/" "$ACTIVE_MAIN_MENU_PREPEND_PATH"

		elif [ "$DARK_MODE_STATUS" = "ON" ]; then

			sed -i "/arrow_width/a $(cat ${ALIZ_DARK_MMENU_SCHEME})" "$ACTIVE_MAIN_MENU_PATH"
			sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$ACTIVE_MAIN_MENU_PATH"
			sed -i "s/foreground=\"$MMENU_COLOR_SEP_MARKUP_DARK\"/foreground=\"$ALIZ_MMENU_DEF_SEP_MARKUP_DARK\"/" "$ACTIVE_MAIN_MENU_PATH"
			sed -i "s/$MMENU_COLOR_RECT_DARK,#$MENU_RECT/$ALIZ_MMENU_DEF_RECT_DARK,#$MENU_RECT/" "$ACTIVE_MAIN_MENU_PREPEND_PATH"
			sed -i "s/$MMENU_COLOR_SEARCH_DARK,#$MENU_SEARCH/$ALIZ_MMENU_DEF_SEARCH_DARK,#$MENU_SEARCH/" "$ACTIVE_MAIN_MENU_PREPEND_PATH"

			else

				echo "error"

		fi

}

MMENU_MATCHA_AZUL_SCHEME() {

AZUL_LIGHT_MMENU_SCHEME=$(mktemp)
trap "rm -f ${AZUL_LIGHT_MMENU_SCHEME}" EXIT
cat << 'EOF' > ${AZUL_LIGHT_MMENU_SCHEME}
color_menu_bg = #f7f7f7 100
color_menu_border = #504e65 100
color_norm_bg = #f7f7f7 100
color_norm_fg = #272625 100
color_sel_bg = #3498db 100
color_sel_fg = #f2f2f2 100
color_sel_border = #f7f7f7 100
color_sep_fg = #f7f7f7 100
color_scroll_ind = #868686 100
EOF

AZUL_DARK_MMENU_SCHEME=$(mktemp)
trap "rm -f ${AZUL_DARK_MMENU_SCHEME}" EXIT
cat << 'EOF' > ${AZUL_DARK_MMENU_SCHEME}
color_menu_bg = #1b1d24 100
color_menu_border = #504e65 100
color_norm_bg = #1b1d24 100
color_norm_fg = #f8f8f8 100
color_sel_bg = #3498db 100
color_sel_fg = #f8f8f8 100
color_sel_border = #1b1d24 100
color_sep_fg = #1b1d24 100
color_scroll_ind = #383745 100
EOF

	### removing lines with color scheme from config files
	sed -i ""$MMENU_SCHEME_START","$MMENU_SCHEME_END"d" "$LIGHT_MAIN_MENU_PATH" "$DARK_MAIN_MENU_PATH" "$ACTIVE_MAIN_MENU_PATH"

	### merging all lines into one
	sed -i 's/$/\\n/g' ${AZUL_LIGHT_MMENU_SCHEME} ${AZUL_DARK_MMENU_SCHEME}
	sed -i -e :a -e N -e 's/\n//' -e ta ${AZUL_LIGHT_MMENU_SCHEME} ${AZUL_DARK_MMENU_SCHEME}

	### pastin' new color scheme into config files
	### light scheme
	sed -i "/arrow_width/a $(cat ${AZUL_LIGHT_MMENU_SCHEME})" "$LIGHT_MAIN_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$LIGHT_MAIN_MENU_PATH"
	sed -i "s/foreground=\"$MMENU_COLOR_SEP_MARKUP_LIGHT\"/foreground=\"$AZUL_MMENU_DEF_SEP_MARKUP_LIGHT\"/" "$LIGHT_MAIN_MENU_PATH"
	sed -i "s/$MMENU_COLOR_RECT_LIGHT,#$MENU_RECT/$AZUL_MMENU_DEF_RECT_LIGHT,#$MENU_RECT/" "$LIGHT_MAIN_MENU_PREPEND_PATH"
	sed -i "s/$MMENU_COLOR_SEARCH_LIGHT,#$MENU_SEARCH/$AZUL_MMENU_DEF_SEARCH_LIGHT,#$MENU_SEARCH/" "$LIGHT_MAIN_MENU_PREPEND_PATH"
	### dark scheme
	sed -i "/arrow_width/a $(cat ${AZUL_DARK_MMENU_SCHEME})" "$DARK_MAIN_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$DARK_MAIN_MENU_PATH"
	sed -i "s/foreground=\"$MMENU_COLOR_SEP_MARKUP_DARK\"/foreground=\"$AZUL_MMENU_DEF_SEP_MARKUP_DARK\"/" "$DARK_MAIN_MENU_PATH"
	sed -i "s/$MMENU_COLOR_RECT_DARK.#$MENU_RECT/$AZUL_MMENU_DEF_RECT_DARK.#$MENU_RECT/" "$DARK_MAIN_MENU_PREPEND_PATH"
	sed -i "s/$MMENU_COLOR_SEARCH_DARK,#$MENU_SEARCH/$AZUL_MMENU_DEF_SEARCH_DARK,#$MENU_SEARCH/" "$DARK_MAIN_MENU_PREPEND_PATH"

		if [ "$DARK_MODE_STATUS" = "OFF" ]; then

			sed -i "/arrow_width/a $(cat ${AZUL_LIGHT_MMENU_SCHEME})" "$ACTIVE_MAIN_MENU_PATH"
			sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$ACTIVE_MAIN_MENU_PATH"
			sed -i "s/foreground=\"$MMENU_COLOR_SEP_MARKUP_LIGHT\"/foreground=\"$AZUL_MMENU_DEF_SEP_MARKUP_LIGHT\"/" "$ACTIVE_MAIN_MENU_PATH"
			sed -i "s/$MMENU_COLOR_RECT_LIGHT,#$MENU_RECT/$AZUL_MMENU_DEF_RECT_LIGHT,#$MENU_RECT/" "$ACTIVE_MAIN_MENU_PREPEND_PATH"
			sed -i "s/$MMENU_COLOR_SEARCH_LIGHT,#$MENU_SEARCH/$AZUL_MMENU_DEF_SEARCH_LIGHT,#$MENU_SEARCH/" "$ACTIVE_MAIN_MENU_PREPEND_PATH"

		elif [ "$DARK_MODE_STATUS" = "ON" ]; then

			sed -i "/arrow_width/a $(cat ${AZUL_DARK_MMENU_SCHEME})" "$ACTIVE_MAIN_MENU_PATH"
			sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$ACTIVE_MAIN_MENU_PATH"
			sed -i "s/foreground=\"$MMENU_COLOR_SEP_MARKUP_DARK\"/foreground=\"$AZUL_MMENU_DEF_SEP_MARKUP_DARK\"/" "$ACTIVE_MAIN_MENU_PATH"
			sed -i "s/$MMENU_COLOR_RECT_DARK,#$MENU_RECT/$AZUL_MMENU_DEF_RECT_DARK,#$MENU_RECT/" "$ACTIVE_MAIN_MENU_PREPEND_PATH"
			sed -i "s/$MMENU_COLOR_SEARCH_DARK,#$MENU_SEARCH/$AZUL_MMENU_DEF_SEARCH_DARK,#$MENU_SEARCH/" "$ACTIVE_MAIN_MENU_PREPEND_PATH"

			else

				echo "error"

		fi

}

### RIGHT-CLICK MENU ##################################################################################################################################

RCLICK_MENU_SCHEME_START=$(grep -n 'color_menu_bg' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | head -n 1)
RCLICK_MENU_SCHEME_END=$(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1)

### RCLICK_MENU_SCHEME - MATCHA SEA

RCLICK_MENU_MATCHA_SEA_SCHEME() {

SEA_LIGHT_RCLICK_MENU_SCHEME=$(mktemp)
trap "rm -f ${SEA_LIGHT_RCLICK_MENU_SCHEME}" EXIT
cat << 'EOF' > ${SEA_LIGHT_RCLICK_MENU_SCHEME}
color_menu_bg = #f7f7f7 100
color_menu_border = #504e65 100
color_norm_bg = #f7f7f7 100
color_norm_fg = #272625 100
color_sel_bg = #2f9b85 100
color_sel_fg = #f2f2f2 100
color_sel_border = #f7f7f7 100
color_sep_fg = #f7f7f7 100
color_scroll_ind = #868686 100
EOF

SEA_DARK_RCLICK_MENU_SCHEME=$(mktemp)
trap "rm -f ${SEA_DARK_RCLICK_MENU_SCHEME}" EXIT
cat << 'EOF' > ${SEA_DARK_RCLICK_MENU_SCHEME}
color_menu_bg = #1b2224 100
color_menu_border = #504e65 100
color_norm_bg = #1b2224 100
color_norm_fg = #f8f8f8 100
color_sel_bg = #2f9b85 100
color_sel_fg = #f8f8f8 100
color_sel_border = #504e65 100
color_sep_fg = #1b2224 100
color_scroll_ind = #383745 100
EOF

	### removing lines with color scheme from config files
	sed -i ""$RCLICK_MENU_SCHEME_START","$RCLICK_MENU_SCHEME_END"d" "$LIGHT_RCLICK_MENU_PATH" "$DARK_RCLICK_MENU_PATH"

	### merging all lines into one
	sed -i 's/$/\\n/g' ${SEA_LIGHT_RCLICK_MENU_SCHEME} ${SEA_DARK_RCLICK_MENU_SCHEME}
	sed -i -e :a -e N -e 's/\n//' -e ta ${SEA_LIGHT_RCLICK_MENU_SCHEME} ${SEA_DARK_RCLICK_MENU_SCHEME}

	### pastin' new color scheme into config files
	### light scheme
	sed -i "/arrow_width/a $(cat ${SEA_LIGHT_RCLICK_MENU_SCHEME})" "$LIGHT_RCLICK_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$LIGHT_RCLICK_MENU_PATH"
	sed -i "s/foreground=\"$RCMENU_COLOR_SEP_MARKUP_LIGHT\"/foreground=\"$SEA_RCMENU_DEF_SEP_MARKUP_LIGHT\"/" "$LIGHT_RCLICK_MENU_PATH"
	### dark scheme
	sed -i "/arrow_width/a $(cat ${SEA_DARK_RCLICK_MENU_SCHEME})" "$DARK_RCLICK_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$DARK_RCLICK_MENU_PATH"
	sed -i "s/foreground=\"$RCMENU_COLOR_SEP_MARKUP_DARK\"/foreground=\"$SEA_RCMENU_DEF_SEP_MARKUP_DARK\"/" "$DARK_RCLICK_MENU_PATH"

}

### RCLICK_MENU_SCHEME - MATCHA ALIZ

RCLICK_MENU_MATCHA_ALIZ_SCHEME() {

ALIZ_LIGHT_RCLICK_MENU_SCHEME=$(mktemp)
trap "rm -f ${ALIZ_LIGHT_RCLICK_MENU_SCHEME}" EXIT
cat << 'EOF' > ${ALIZ_LIGHT_RCLICK_MENU_SCHEME}
color_menu_bg = #f7f7f7 100
color_menu_border = #504e65 100
color_norm_bg = #f0f0f0 100
color_norm_fg = #272625 100
color_sel_bg = #e44138 100
color_sel_fg = #f2f2f2 100
color_sel_border = #f7f7f7 100
color_sep_fg = #f7f7f7 100
color_scroll_ind = #504e65 100
EOF

ALIZ_DARK_RCLICK_MENU_SCHEME=$(mktemp)
trap "rm -f ${ALIZ_DARK_RCLICK_MENU_SCHEME}" EXIT
cat << 'EOF' > ${ALIZ_DARK_RCLICK_MENU_SCHEME}
color_menu_bg = #222222 100
color_menu_border = #504e65 100
color_norm_bg = #222222 100
color_norm_fg = #f8f8f8 100
color_sel_bg = #e44138 100
color_sel_fg = #f8f8f8 100
color_sel_border = #222222 100
color_sep_fg = #222222 100
color_scroll_ind = #383745 100
EOF

	### removing lines with color scheme from config files
	sed -i ""$RCLICK_MENU_SCHEME_START","$RCLICK_MENU_SCHEME_END"d" "$LIGHT_RCLICK_MENU_PATH" "$DARK_RCLICK_MENU_PATH"

	### merging all lines into one
	sed -i 's/$/\\n/g' ${ALIZ_LIGHT_RCLICK_MENU_SCHEME} ${ALIZ_DARK_RCLICK_MENU_SCHEME}
	sed -i -e :a -e N -e 's/\n//' -e ta ${ALIZ_LIGHT_RCLICK_MENU_SCHEME} ${ALIZ_DARK_RCLICK_MENU_SCHEME}

	### pastin' new color scheme into config files
	### light scheme
	sed -i "/arrow_width/a $(cat ${ALIZ_LIGHT_RCLICK_MENU_SCHEME})" "$LIGHT_RCLICK_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$LIGHT_RCLICK_MENU_PATH"
	sed -i "s/foreground=\"$RCMENU_COLOR_SEP_MARKUP_LIGHT\"/foreground=\"$ALIZ_RCMENU_DEF_SEP_MARKUP_LIGHT\"/" "$LIGHT_RCLICK_MENU_PATH"
	### dark scheme
	sed -i "/arrow_width/a $(cat ${ALIZ_DARK_RCLICK_MENU_SCHEME})" "$DARK_RCLICK_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$DARK_RCLICK_MENU_PATH"
	sed -i "s/foreground=\"$RCMENU_COLOR_SEP_MARKUP_DARK\"/foreground=\"$ALIZ_RCMENU_DEF_SEP_MARKUP_DARK\"/" "$DARK_RCLICK_MENU_PATH"

}

### RCLICK_MENU_SCHEME - MATCHA AZUL

RCLICK_MENU_MATCHA_AZUL_SCHEME() {

AZUL_LIGHT_RCLICK_MENU_SCHEME=$(mktemp)
trap "rm -f ${AZUL_LIGHT_RCLICK_MENU_SCHEME}" EXIT
cat << 'EOF' > ${AZUL_LIGHT_RCLICK_MENU_SCHEME}
color_menu_bg = #f7f7f7 100
color_menu_border = #504e65 100
color_norm_bg = #f7f7f7 100
color_norm_fg = #272625 100
color_sel_bg = #3498db 100
color_sel_fg = #f2f2f2 100
color_sel_border = #f7f7f7 100
color_sep_fg = #f7f7f7 100
color_scroll_ind = #868686 100
EOF

AZUL_DARK_RCLICK_MENU_SCHEME=$(mktemp)
trap "rm -f ${AZUL_DARK_RCLICK_MENU_SCHEME}" EXIT
cat << 'EOF' > ${AZUL_DARK_RCLICK_MENU_SCHEME}
color_menu_bg = #1b1d24 100
color_menu_border = #504e65 100
color_norm_bg = #1b1d24 100
color_norm_fg = #f8f8f8 100
color_sel_bg = #3498db 100
color_sel_fg = #f8f8f8 100
color_sel_border = #1b1d24 100
color_sep_fg = #1b1d24 100
color_scroll_ind = #383745 100
EOF

	### removing lines with color scheme from config files
	sed -i ""$RCLICK_MENU_SCHEME_START","$RCLICK_MENU_SCHEME_END"d" "$LIGHT_RCLICK_MENU_PATH" "$DARK_RCLICK_MENU_PATH"

	### merging all lines into one
	sed -i 's/$/\\n/g' ${AZUL_LIGHT_RCLICK_MENU_SCHEME} ${AZUL_DARK_RCLICK_MENU_SCHEME}
	sed -i -e :a -e N -e 's/\n//' -e ta ${AZUL_LIGHT_RCLICK_MENU_SCHEME} ${AZUL_DARK_RCLICK_MENU_SCHEME}

	### pastin' new color scheme into config files
	### light scheme
	sed -i "/arrow_width/a $(cat ${AZUL_LIGHT_RCLICK_MENU_SCHEME})" "$LIGHT_RCLICK_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$LIGHT_RCLICK_MENU_PATH"
	sed -i "s/foreground=\"$RCMENU_COLOR_SEP_MARKUP_LIGHT\"/foreground=\"$AZUL_RCMENU_DEF_SEP_MARKUP_LIGHT\"/" "$LIGHT_RCLICK_MENU_PATH"
	### dark scheme
	sed -i "/arrow_width/a $(cat ${AZUL_DARK_RCLICK_MENU_SCHEME})" "$DARK_RCLICK_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$DARK_RCLICK_MENU_PATH"
	sed -i "s/foreground=\"$RCMENU_COLOR_SEP_MARKUP_DARK\"/foreground=\"$AZUL_RCMENU_DEF_SEP_MARKUP_DARK\"/" "$DARK_RCLICK_MENU_PATH"

}

### PANEL MENUS #######################################################################################################################################

PANEL_MENUS_SCHEME_START=$(grep -n 'color_menu_bg' "$TINT_PANEL_MENUS_PATH" | cut -d ':' -f 1 | head -n 1)
PANEL_MENUS_SCHEME_END=$(grep -n 'color_scroll_ind' "$TINT_PANEL_MENUS_PATH" | cut -d ':' -f 1 | tail -n 1)

### PANEL_MENUS_SCHEME - MATCHA SEA

PANEL_MENUS_MATCHA_SEA_SCHEME() {

SEA_LIGHT_PANEL_MENUS_SCHEME=$(mktemp)
trap "rm -f ${SEA_LIGHT_PANEL_MENUS_SCHEME}" EXIT
cat << 'EOF' > ${SEA_LIGHT_PANEL_MENUS_SCHEME}
color_menu_bg = #2f9b85 100
color_menu_border = #2f9b85 100
color_norm_bg = #2f9b85 100
color_norm_fg = #b0d4cd 100
color_sel_bg = #a8d5cc 100
color_sel_fg = #3d8274 100
color_sel_border = #504e65 100
color_sep_fg = #2f9b85 100
color_scroll_ind = #504e65 100
EOF

	### removing lines with color scheme from config files
	sed -i ""$PANEL_MENUS_SCHEME_START","$PANEL_MENUS_SCHEME_END"d" "$TINT_PANEL_MENUS_PATH" "$KEY_BINDINGS_MENU_PATH"

	### merging all lines into one
	sed -i 's/$/\\n/g' ${SEA_LIGHT_PANEL_MENUS_SCHEME}
	sed -i -e :a -e N -e 's/\n//' -e ta ${SEA_LIGHT_PANEL_MENUS_SCHEME}

	### pastin' new color scheme into config files
	sed -i "/arrow_width/a $(cat ${SEA_LIGHT_PANEL_MENUS_SCHEME})" "$TINT_PANEL_MENUS_PATH" "$KEY_BINDINGS_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$TINT_PANEL_MENUS_PATH" "$KEY_BINDINGS_MENU_PATH"
	sed -i "s/foreground=\"$PMENUS_COLOR_SEP_MARKUP\"/foreground=\"$SEA_PMENUS_DEF_SEP_MARKUP\"/" "$TINT_PANEL_MENUS_PATH" "$KEY_BINDINGS_MENU_PATH"

}

### PANEL_MENUS_SCHEME - MATCHA ALIZ

PANEL_MENUS_MATCHA_ALIZ_SCHEME() {

ALIZ_LIGHT_PANEL_MENUS_SCHEME=$(mktemp)
trap "rm -f ${ALIZ_LIGHT_PANEL_MENUS_SCHEME}" EXIT
cat << 'EOF' > ${ALIZ_LIGHT_PANEL_MENUS_SCHEME}
color_menu_bg = #e44138 100
color_menu_border = #e44138 100
color_norm_bg = #e44138 100
color_norm_fg = #d4b4b0 100
color_sel_bg = #eb9f95 100
color_sel_fg = #8c3e34 100
color_sel_border = #a76860 100
color_sep_fg = #e44138 100
color_scroll_ind = #a76860 100
EOF

	### removing lines with color scheme from config files
	sed -i ""$PANEL_MENUS_SCHEME_START","$PANEL_MENUS_SCHEME_END"d" "$TINT_PANEL_MENUS_PATH" "$KEY_BINDINGS_MENU_PATH"

	### merging all lines into one
	sed -i 's/$/\\n/g' ${ALIZ_LIGHT_PANEL_MENUS_SCHEME}
	sed -i -e :a -e N -e 's/\n//' -e ta ${ALIZ_LIGHT_PANEL_MENUS_SCHEME}

	### pastin' new color scheme into config files
	sed -i "/arrow_width/a $(cat ${ALIZ_LIGHT_PANEL_MENUS_SCHEME})" "$TINT_PANEL_MENUS_PATH" "$KEY_BINDINGS_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$TINT_PANEL_MENUS_PATH" "$KEY_BINDINGS_MENU_PATH"
	sed -i "s/foreground=\"$PMENUS_COLOR_SEP_MARKUP\"/foreground=\"$ALIZ_PMENUS_DEF_SEP_MARKUP\"/" "$TINT_PANEL_MENUS_PATH" "$KEY_BINDINGS_MENU_PATH"

}

### PANEL_MENUS_SCHEME - MATCHA AZUL

PANEL_MENUS_MATCHA_AZUL_SCHEME () {

AZUL_LIGHT_PANEL_MENUS_SCHEME=$(mktemp)
trap "rm -f ${AZUL_LIGHT_PANEL_MENUS_SCHEME}" EXIT
cat << 'EOF' > ${AZUL_LIGHT_PANEL_MENUS_SCHEME}
color_menu_bg = #3498db 100
color_menu_border = #3498db 100
color_norm_bg = #3498db 100
color_norm_fg = #b0c0d4 100
color_sel_bg = #95caeb 100
color_sel_fg = #34708C 100
color_sel_border = #6090a7 100
color_sep_fg = #3498db 100
color_scroll_ind = #6090a7 100
EOF

	### removing lines with color scheme from config files
	sed -i ""$PANEL_MENUS_SCHEME_START","$PANEL_MENUS_SCHEME_END"d" "$TINT_PANEL_MENUS_PATH" "$KEY_BINDINGS_MENU_PATH"

	### merging all lines into one
	sed -i 's/$/\\n/g' ${AZUL_LIGHT_PANEL_MENUS_SCHEME}
	sed -i -e :a -e N -e 's/\n//' -e ta ${AZUL_LIGHT_PANEL_MENUS_SCHEME}

	### pastin' new color scheme into config files
	sed -i "/arrow_width/a $(cat ${AZUL_LIGHT_PANEL_MENUS_SCHEME})" "$TINT_PANEL_MENUS_PATH" "$KEY_BINDINGS_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$TINT_PANEL_MENUS_PATH" "$KEY_BINDINGS_MENU_PATH"
	sed -i "s/foreground=\"$PMENUS_COLOR_SEP_MARKUP\"/foreground=\"$AZUL_PMENUS_DEF_SEP_MARKUP\"/" "$TINT_PANEL_MENUS_PATH" "$KEY_BINDINGS_MENU_PATH"

}

### SIDE MENU #########################################################################################################################################

SIDE_MENU_SCHEME_START=$(grep -n 'color_menu_bg' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | head -n 1)
SIDE_MENU_SCHEME_END=$(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1)

### SIDE_MENU_SCHEME - MATCHA SEA

SIDE_MENU_MATCHA_SEA_SCHEME() {

SEA_LIGHT_SIDE_MENU_SCHEME=$(mktemp)
trap "rm -f ${SEA_LIGHT_SIDE_MENU_SCHEME}" EXIT
cat << 'EOF' > ${SEA_LIGHT_SIDE_MENU_SCHEME}
color_menu_bg = #f7f7f7 100
color_menu_border = #504e65 100
color_norm_bg = #f0f0f0 100
color_norm_fg = #272625 100
color_sel_bg = #2f9b85 100
color_sel_fg = #f2f2f2 100
color_sel_border = #f7f7f7 100
color_sep_fg = #f7f7f7 100
color_scroll_ind = #504e65 100
EOF

SEA_DARK_SIDE_MENU_SCHEME=$(mktemp)
trap "rm -f ${SEA_DARK_SIDE_MENU_SCHEME}" EXIT
cat << 'EOF' > ${SEA_DARK_SIDE_MENU_SCHEME}
color_menu_bg = #1b2224 100
color_menu_border = #504e65 100
color_norm_bg = #222b2e 100
color_norm_fg = #f8f8f8 100
color_sel_bg = #2f9b85 100
color_sel_fg = #f8f8f8 100
color_sel_border = #1b2224 100
color_sep_fg = #1b2224 100
color_scroll_ind = #383745 100
EOF

	### removing lines with color scheme from config files
	sed -i ""$SIDE_MENU_SCHEME_START","$SIDE_MENU_SCHEME_END"d" "$LIGHT_SIDE_MENU_PATH" "$DARK_SIDE_MENU_PATH"

	### merging all lines into one
	sed -i 's/$/\\n/g' ${SEA_LIGHT_SIDE_MENU_SCHEME} ${SEA_DARK_SIDE_MENU_SCHEME}
	sed -i -e :a -e N -e 's/\n//' -e ta ${SEA_LIGHT_SIDE_MENU_SCHEME} ${SEA_DARK_SIDE_MENU_SCHEME}

	### pastin' new color scheme into config files
	### light scheme
	sed -i "/arrow_width/a $(cat ${SEA_LIGHT_SIDE_MENU_SCHEME})" "$LIGHT_SIDE_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$LIGHT_SIDE_MENU_PATH"
	sed -i "s/foreground=\"$SMENU_COLOR_SEP_MARKUP_LIGHT\"/foreground=\"$SEA_SMENU_DEF_SEP_MARKUP_LIGHT\"/" "$LIGHT_SIDE_MENU_PATH"
	### dark scheme
	sed -i "/arrow_width/a $(cat ${SEA_DARK_SIDE_MENU_SCHEME})" "$DARK_SIDE_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$DARK_SIDE_MENU_PATH"
	sed -i "s/foreground=\"$SMENU_COLOR_SEP_MARKUP_DARK\"/foreground=\"$SEA_SMENU_DEF_SEP_MARKUP_DARK\"/" "$DARK_SIDE_MENU_PATH"

}

### SIDE_MENU_SCHEME - MATCHA ALIZ

SIDE_MENU_MATCHA_ALIZ_SCHEME() {

ALIZ_LIGHT_SIDE_MENU_SCHEME=$(mktemp)
trap "rm -f ${ALIZ_LIGHT_SIDE_MENU_SCHEME}" EXIT
cat << 'EOF' > ${ALIZ_LIGHT_SIDE_MENU_SCHEME}
color_menu_bg = #f7f7f7 100
color_menu_border = #504e65 100
color_norm_bg = #f0f0f0 100
color_norm_fg = #272625 100
color_sel_bg = #e44138 100
color_sel_fg = #f2f2f2 100
color_sel_border = #f7f7f7 100
color_sep_fg = #f7f7f7 100
color_scroll_ind = #504e65 100
EOF

ALIZ_DARK_SIDE_MENU_SCHEME=$(mktemp)
trap "rm -f ${ALIZ_DARK_SIDE_MENU_SCHEME}" EXIT
cat << 'EOF' > ${ALIZ_DARK_SIDE_MENU_SCHEME}
color_menu_bg = #222222 100
color_menu_border = #504e65 100
color_norm_bg = #2e2224 100
color_norm_fg = #f8f8f8 100
color_sel_bg = #e44138 100
color_sel_fg = #f8f8f8 100
color_sel_border = #222222 100
color_sep_fg = #222222 100
color_scroll_ind = #383745 100
EOF

	### removing lines with color scheme from config files
	sed -i ""$SIDE_MENU_SCHEME_START","$SIDE_MENU_SCHEME_END"d" "$LIGHT_SIDE_MENU_PATH" "$DARK_SIDE_MENU_PATH"

	### merging all lines into one
	sed -i 's/$/\\n/g' ${ALIZ_LIGHT_SIDE_MENU_SCHEME} ${ALIZ_DARK_SIDE_MENU_SCHEME}
	sed -i -e :a -e N -e 's/\n//' -e ta ${ALIZ_LIGHT_SIDE_MENU_SCHEME} ${ALIZ_DARK_SIDE_MENU_SCHEME}

	### pastin' new color scheme into config files
	### light scheme
	sed -i "/arrow_width/a $(cat ${ALIZ_LIGHT_SIDE_MENU_SCHEME})" "$LIGHT_SIDE_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$LIGHT_SIDE_MENU_PATH"
	sed -i "s/foreground=\"$SMENU_COLOR_SEP_MARKUP_LIGHT\"/foreground=\"$ALIZ_SMENU_DEF_SEP_MARKUP_LIGHT\"/" "$LIGHT_SIDE_MENU_PATH"
	### dark scheme
	sed -i "/arrow_width/a $(cat ${ALIZ_DARK_SIDE_MENU_SCHEME})" "$DARK_SIDE_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$DARK_SIDE_MENU_PATH"
	sed -i "s/foreground=\"$SMENU_COLOR_SEP_MARKUP_DARK\"/foreground=\"$ALIZ_SMENU_DEF_SEP_MARKUP_DARK\"/" "$DARK_SIDE_MENU_PATH"

}

### SIDE_MENU_SCHEME - MATCHA AZUL

SIDE_MENU_MATCHA_AZUL_SCHEME() {

AZUL_LIGHT_SIDE_MENU_SCHEME=$(mktemp)
trap "rm -f ${AZUL_LIGHT_SIDE_MENU_SCHEME}" EXIT
cat << 'EOF' > ${AZUL_LIGHT_SIDE_MENU_SCHEME}
color_menu_bg = #f7f7f7 100
color_menu_border = #504e65 100
color_norm_bg = #f0f0f0 100
color_norm_fg = #272625 100
color_sel_bg = #3498db 100
color_sel_fg = #f2f2f2 100
color_sel_border = #f7f7f7 100
color_sep_fg = #f7f7f7 100
color_scroll_ind = #504e65 100
EOF

AZUL_DARK_SIDE_MENU_SCHEME=$(mktemp)
trap "rm -f ${AZUL_DARK_SIDE_MENU_SCHEME}" EXIT
cat << 'EOF' > ${AZUL_DARK_SIDE_MENU_SCHEME}
color_menu_bg = #1b1d24 100
color_menu_border = #504e65 100
color_norm_bg = #22252c 100
color_norm_fg = #f8f8f8 100
color_sel_bg = #3498db 100
color_sel_fg = #f8f8f8 100
color_sel_border = #1b1d24 100
color_sep_fg = #1b2224 100
color_scroll_ind = #383745 100
EOF

	### removing lines with color scheme from config files
	sed -i ""$SIDE_MENU_SCHEME_START","$SIDE_MENU_SCHEME_END"d" "$LIGHT_SIDE_MENU_PATH" "$DARK_SIDE_MENU_PATH"

	### merging all lines into one
	sed -i 's/$/\\n/g' ${AZUL_LIGHT_SIDE_MENU_SCHEME} ${AZUL_DARK_SIDE_MENU_SCHEME}
	sed -i -e :a -e N -e 's/\n//' -e ta ${AZUL_LIGHT_SIDE_MENU_SCHEME} ${AZUL_DARK_SIDE_MENU_SCHEME}

	### pastin' new color scheme into config files
	### light scheme
	sed -i "/arrow_width/a $(cat ${AZUL_LIGHT_SIDE_MENU_SCHEME})" "$LIGHT_SIDE_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$LIGHT_SIDE_MENU_PATH"
	sed -i "s/foreground=\"$SMENU_COLOR_SEP_MARKUP_LIGHT\"/foreground=\"$AZUL_SMENU_DEF_SEP_MARKUP_LIGHT\"/" "$LIGHT_SIDE_MENU_PATH"
	### dark scheme
	sed -i "/arrow_width/a $(cat ${AZUL_DARK_SIDE_MENU_SCHEME})" "$DARK_SIDE_MENU_PATH"
	sed -i "$(expr $(grep -n 'color_scroll_ind' "$LIGHT_SIDE_MENU_PATH" | cut -d ':' -f 1 | tail -n 1) + 1)d" "$DARK_SIDE_MENU_PATH"
	sed -i "s/foreground=\"$SMENU_COLOR_SEP_MARKUP_DARK\"/foreground=\"$AZUL_SMENU_DEF_SEP_MARKUP_DARK\"/" "$DARK_SIDE_MENU_PATH"

}

### GTK THEMES ########################################################################################################################################

GTK_MATCHA_SEA_THEME_CONFIGS() {

	if [ "$DARK_MODE_STATUS" = "OFF" ]; then

		xfconf-query -c xsettings -p /Net/ThemeName -s Matcha-light-sea
		xfconf-query -c xsettings -p /Net/IconThemeName -s Flat-Remix-Green-Light
		sed -i "s/$(grep "gtk-theme-name" "$GTK2_CONFIG_PATH")/gtk-theme-name=\"Matcha-light-sea\"/" "$GTK2_CONFIG_PATH"
		sed -i "s/$(grep "gtk-icon-theme-name" "$GTK2_CONFIG_PATH")/gtk-icon-theme-name=\"Flat-Remix-Green-Light\"/" "$GTK2_CONFIG_PATH"
		sed -i "s/$(grep "gtk-theme-name" "$GTK3_CONFIG_PATH")/gtk-theme-name=Matcha-light-sea/" "$GTK3_CONFIG_PATH"
		sed -i "s/$(grep "gtk-icon-theme-name" "$GTK3_CONFIG_PATH")/gtk-icon-theme-name=Flat-Remix-Green-Light/" "$GTK3_CONFIG_PATH"

	elif [ "$DARK_MODE_STATUS" = "ON" ]; then

		xfconf-query -c xsettings -p /Net/ThemeName -s Matcha-dark-sea
		xfconf-query -c xsettings -p /Net/IconThemeName -s Flat-Remix-Green-Dark
		sed -i "s/$(grep "gtk-theme-name" "$GTK2_CONFIG_PATH")/Mgtk-theme-name=\"Matcha-dark-sea\"/" "$GTK2_CONFIG_PATH"
		sed -i "s/$(grep "gtk-icon-theme-name" "$GTK2_CONFIG_PATH")/gtk-icon-theme-name=\"Flat-Remix-Green-Dark\"/" "$GTK2_CONFIG_PATH"
		sed -i "s/$(grep "gtk-theme-name" "$GTK3_CONFIG_PATH")/gtk-theme-name=Matcha-dark-sea/" "$GTK3_CONFIG_PATH"
		sed -i "s/$(grep "gtk-icon-theme-name" "$GTK3_CONFIG_PATH")/gtk-icon-theme-name=Flat-Remix-Green-Dark/" "$GTK3_CONFIG_PATH"

	fi

	sed -i "s/$(grep "GTK_LIGHT" "$DARK_MODE_SETTINGS_PATH")/GTK_LIGHT=\"Matcha-light-sea\"/" "$DARK_MODE_SETTINGS_PATH"
	sed -i "s/$(grep "GTK_DARK" "$DARK_MODE_SETTINGS_PATH")/GTK_DARK=\"Matcha-dark-sea\"/" "$DARK_MODE_SETTINGS_PATH"
	sed -i "s/$(grep "ICONS_LIGHT" "$DARK_MODE_SETTINGS_PATH" | head -n 1)/ICONS_LIGHT=\"Flat-Remix-Green-Light\"/" "$DARK_MODE_SETTINGS_PATH"
	sed -i "s/$(grep "ICONS_DARK" "$DARK_MODE_SETTINGS_PATH" | head -n 1)/ICONS_DARK=\"Flat-Remix-Green-Dark\"/" "$DARK_MODE_SETTINGS_PATH"
	sed -i "s/$(grep "MAINMENU_ICONS_LIGHT_THEME" "$DARK_MODE_SETTINGS_PATH")/MAINMENU_ICONS_LIGHT_THEME=\"Flat-Remix-Green-Light\"/" "$DARK_MODE_SETTINGS_PATH"
	sed -i "s/$(grep "MAINMENU_ICONS_DARK_THEME" "$DARK_MODE_SETTINGS_PATH")/MAINMENU_ICONS_DARK_THEME=\"Flat-Remix-Green-Dark\"/" "$DARK_MODE_SETTINGS_PATH"

}

GTK_MATCHA_ALIZ_THEME_CONFIGS() {

	if [ "$DARK_MODE_STATUS" = "OFF" ]; then

		xfconf-query -c xsettings -p /Net/ThemeName -s Matcha-light-aliz
		xfconf-query -c xsettings -p /Net/IconThemeName -s Flat-Remix-Red-Light
		sed -i "s/$(grep "gtk-theme-name" "$GTK2_CONFIG_PATH")/gtk-theme-name=\"Matcha-light-aliz\"/" "$GTK2_CONFIG_PATH"
		sed -i "s/$(grep "gtk-icon-theme-name" "$GTK2_CONFIG_PATH")/gtk-icon-theme-name=\"Flat-Remix-Red-Light\"/" "$GTK2_CONFIG_PATH"
		sed -i "s/$(grep "gtk-theme-name" "$GTK3_CONFIG_PATH")/gtk-theme-name=Matcha-light-aliz/" "$GTK3_CONFIG_PATH"
		sed -i "s/$(grep "gtk-icon-theme-name" "$GTK3_CONFIG_PATH")/gtk-icon-theme-name=Flat-Remix-Red-Light/" "$GTK3_CONFIG_PATH"

	elif [ "$DARK_MODE_STATUS" = "ON" ]; then

		xfconf-query -c xsettings -p /Net/ThemeName -s Matcha-dark-aliz
		xfconf-query -c xsettings -p /Net/IconThemeName -s Flat-Remix-Red-Dark
		sed -i "s/$(grep "gtk-theme-name" "$GTK2_CONFIG_PATH")/gtk-theme-name=\"Matcha-dark-aliz\"/" "$GTK2_CONFIG_PATH"
		sed -i "s/$(grep "gtk-icon-theme-name" "$GTK2_CONFIG_PATH")/gtk-icon-theme-name=\"Flat-Remix-Red-Dark\"/" "$GTK2_CONFIG_PATH"
		sed -i "s/$(grep "gtk-theme-name" "$GTK3_CONFIG_PATH")/gtk-theme-name=Matcha-dark-aliz/" "$GTK3_CONFIG_PATH"
		sed -i "s/$(grep "gtk-icon-theme-name" "$GTK3_CONFIG_PATH")/gtk-icon-theme-name=Flat-Remix-Red-Dark/" "$GTK3_CONFIG_PATH"

	fi

	sed -i "s/$(grep "GTK_LIGHT" "$DARK_MODE_SETTINGS_PATH")/GTK_LIGHT=\"Matcha-light-aliz\"/" "$DARK_MODE_SETTINGS_PATH"
	sed -i "s/$(grep "GTK_DARK" "$DARK_MODE_SETTINGS_PATH")/GTK_DARK=\"Matcha-dark-aliz\"/" "$DARK_MODE_SETTINGS_PATH"
	sed -i "s/$(grep "ICONS_LIGHT" "$DARK_MODE_SETTINGS_PATH" | head -n 1)/ICONS_LIGHT=\"Flat-Remix-Red-Light\"/" "$DARK_MODE_SETTINGS_PATH"
	sed -i "s/$(grep "ICONS_DARK" "$DARK_MODE_SETTINGS_PATH" | head -n 1)/ICONS_DARK=\"Flat-Remix-Red-Dark\"/" "$DARK_MODE_SETTINGS_PATH"
	sed -i "s/$(grep "MAINMENU_ICONS_LIGHT_THEME" "$DARK_MODE_SETTINGS_PATH")/MAINMENU_ICONS_LIGHT_THEME=\"Flat-Remix-Red-Light\"/" "$DARK_MODE_SETTINGS_PATH"
	sed -i "s/$(grep "MAINMENU_ICONS_DARK_THEME" "$DARK_MODE_SETTINGS_PATH")/MAINMENU_ICONS_DARK_THEME=\"Flat-Remix-Red-Dark\"/" "$DARK_MODE_SETTINGS_PATH"

}

GTK_MATCHA_AZUL_THEME_CONFIGS() {

	if [ "$DARK_MODE_STATUS" = "OFF" ]; then

		xfconf-query -c xsettings -p /Net/ThemeName -s Matcha-light-azul
		xfconf-query -c xsettings -p /Net/IconThemeName -s Flat-Remix-Blue-Light
		sed -i "s/$(grep "gtk-theme-name" "$GTK2_CONFIG_PATH")/gtk-theme-name=\"Matcha-light-azul\"/" "$GTK2_CONFIG_PATH"
		sed -i "s/$(grep "gtk-icon-theme-name" "$GTK2_CONFIG_PATH")/gtk-icon-theme-name=\"Flat-Remix-Blue-Light\"/" "$GTK2_CONFIG_PATH"
		sed -i "s/$(grep "gtk-theme-name" "$GTK3_CONFIG_PATH")/gtk-theme-name=Matcha-light-azul/" "$GTK3_CONFIG_PATH"
		sed -i "s/$(grep "gtk-icon-theme-name" "$GTK3_CONFIG_PATH")/gtk-icon-theme-name=Flat-Remix-Blue-Light/" "$GTK3_CONFIG_PATH"

	elif [ "$DARK_MODE_STATUS" = "ON" ]; then

		xfconf-query -c xsettings -p /Net/ThemeName -s Matcha-dark-azul
		xfconf-query -c xsettings -p /Net/IconThemeName -s Flat-Remix-Blue-Dark
		sed -i "s/$(grep "gtk-theme-name" "$GTK2_CONFIG_PATH")/gtk-theme-name=\"Matcha-dark-azul\"/" "$GTK2_CONFIG_PATH"
		sed -i "s/$(grep "gtk-icon-theme-name" "$GTK2_CONFIG_PATH")/gtk-icon-theme-name=\"Flat-Remix-Blue-Dark\"/" "$GTK2_CONFIG_PATH"
		sed -i "s/$(grep "gtk-theme-name" "$GTK3_CONFIG_PATH")/gtk-theme-name=Matcha-dark-azul/" "$GTK3_CONFIG_PATH"
		sed -i "s/$(grep "gtk-icon-theme-name" "$GTK3_CONFIG_PATH")/gtk-icon-theme-name=Flat-Remix-Blue-Dark/" "$GTK3_CONFIG_PATH"

	fi

	sed -i "s/$(grep "GTK_LIGHT" "$DARK_MODE_SETTINGS_PATH")/GTK_LIGHT=\"Matcha-light-azul\"/" "$DARK_MODE_SETTINGS_PATH"
	sed -i "s/$(grep "GTK_DARK" "$DARK_MODE_SETTINGS_PATH")/GTK_DARK=\"Matcha-dark-azul\"/" "$DARK_MODE_SETTINGS_PATH"
	sed -i "s/$(grep "ICONS_LIGHT" "$DARK_MODE_SETTINGS_PATH" | head -n 1)/ICONS_LIGHT=\"Flat-Remix-Blue-Light\"/" "$DARK_MODE_SETTINGS_PATH"
	sed -i "s/$(grep "ICONS_DARK" "$DARK_MODE_SETTINGS_PATH" | head -n 1)/ICONS_DARK=\"Flat-Remix-Blue-Dark\"/" "$DARK_MODE_SETTINGS_PATH"
	sed -i "s/$(grep "MAINMENU_ICONS_LIGHT_THEME" "$DARK_MODE_SETTINGS_PATH")/MAINMENU_ICONS_LIGHT_THEME=\"Flat-Remix-Blue-Light\"/" "$DARK_MODE_SETTINGS_PATH"
	sed -i "s/$(grep "MAINMENU_ICONS_DARK_THEME" "$DARK_MODE_SETTINGS_PATH")/MAINMENU_ICONS_DARK_THEME=\"Flat-Remix-Blue-Dark\"/" "$DARK_MODE_SETTINGS_PATH"

}

### QT THEMES #########################################################################################################################################

### QT SCHEME - MATCHA SEA

QT_MATCHA_SEA_SCHEME() {

	if [ "$DARK_MODE_STATUS" = "OFF" ]; then

		sed -i "s/$(grep "icon_theme" "$QT5CT_CONFIG_PATH")/icon_theme=Flat-Remix-Green-Light/" "$QT5CT_CONFIG_PATH"

	elif [ "$DARK_MODE_STATUS" = "ON" ]; then

		sed -i "s/$(grep "icon_theme" "$QT5CT_CONFIG_PATH")/icon_theme=Flat-Remix-Green-Dark/" "$QT5CT_CONFIG_PATH"

	fi

### Light theme

	sed -i "s/$(grep "highlight.color" "$QT_LIGHT_THEME_PATH" | head -n 1)/highlight.color=#2eb398/" "$QT_LIGHT_THEME_PATH"
	sed -i "s/$(grep "inactive.highlight.color" "$QT_LIGHT_THEME_PATH" | head -n 1)/inactive.highlight.color=#2eb398/" "$QT_LIGHT_THEME_PATH"
	sed -i "s/$(grep "progress.indicator.text.color" "$QT_LIGHT_THEME_PATH" | head -n 1)/progress.indicator.text.color=#2eb398/" "$QT_LIGHT_THEME_PATH"

### Dark theme

	sed -i "s/$(grep "highlight.color" "$QT_DARK_THEME_PATH" | head -n 1)/highlight.color=#2f6157/" "$QT_DARK_THEME_PATH"
	sed -i "s/$(grep "inactive.highlight.color" "$QT_DARK_THEME_PATH" | head -n 1)/inactive.highlight.color=#2f6157/" "$QT_DARK_THEME_PATH"

}

### QT SCHEME - MATCHA ALIZ

QT_MATCHA_ALIZ_SCHEME() {

	if [ "$DARK_MODE_STATUS" = "OFF" ]; then

		sed -i "s/$(grep "icon_theme" "$QT5CT_CONFIG_PATH")/icon_theme=Flat-Remix-Red-Light/" "$QT5CT_CONFIG_PATH"

	elif [ "$DARK_MODE_STATUS" = "ON" ]; then

		sed -i "s/$(grep "icon_theme" "$QT5CT_CONFIG_PATH")/icon_theme=Flat-Remix-Red-Dark/" "$QT5CT_CONFIG_PATH"

	fi

### Light theme
	sed -i "s/$(grep "highlight.color" "$QT_LIGHT_THEME_PATH" | head -n 1)/highlight.color=#f0544c/" "$QT_LIGHT_THEME_PATH"
	sed -i "s/$(grep "inactive.highlight.color" "$QT_LIGHT_THEME_PATH" | head -n 1)/inactive.highlight.color=#f0544c/" "$QT_LIGHT_THEME_PATH"
	sed -i "s/$(grep "progress.indicator.text.color" "$QT_LIGHT_THEME_PATH" | head -n 1)/progress.indicator.text.color=#f0544c/" "$QT_LIGHT_THEME_PATH"

### Dark theme
	sed -i "s/$(grep "highlight.color" "$QT_DARK_THEME_PATH" | head -n 1)/highlight.color=#bf3933/" "$QT_DARK_THEME_PATH"
	sed -i "s/$(grep "inactive.highlight.color" "$QT_DARK_THEME_PATH" | head -n 1)/inactive.highlight.color=#bf3933/" "$QT_DARK_THEME_PATH"

}

### QT SCHEME - MATCHA AZUL

QT_MATCHA_AZUL_SCHEME() {

	if [ "$DARK_MODE_STATUS" = "OFF" ]; then

		sed -i "s/$(grep "icon_theme" "$QT5CT_CONFIG_PATH")/icon_theme=Flat-Remix-Blue-Light/" "$QT5CT_CONFIG_PATH"

	elif [ "$DARK_MODE_STATUS" = "ON" ]; then

		sed -i "s/$(grep "icon_theme" "$QT5CT_CONFIG_PATH")/icon_theme=Flat-Remix-Blue-Dark/" "$QT5CT_CONFIG_PATH"

	fi

### Light theme
	sed -i "s/$(grep "highlight.color" "$QT_LIGHT_THEME_PATH" | head -n 1)/highlight.color=#3497da/" "$QT_LIGHT_THEME_PATH"
	sed -i "s/$(grep "inactive.highlight.color" "$QT_LIGHT_THEME_PATH" | head -n 1)/inactive.highlight.color=#3497da/" "$QT_LIGHT_THEME_PATH"
	sed -i "s/$(grep "progress.indicator.text.color" "$QT_LIGHT_THEME_PATH" | head -n 1)/progress.indicator.text.color=#3497da/" "$QT_LIGHT_THEME_PATH"

### Dark theme
	sed -i "s/$(grep "highlight.color" "$QT_DARK_THEME_PATH" | head -n 1)/highlight.color=#3497da/" "$QT_DARK_THEME_PATH"
	sed -i "s/$(grep "inactive.highlight.color" "$QT_DARK_THEME_PATH" | head -n 1)/inactive.highlight.color=#3497da/" "$QT_DARK_THEME_PATH"

}

### DUNST #############################################################################################################################################

DUNST_MATCHA_SEA_SCHEME() {

	sed -i "s/$(grep "background =" "$DUNST_CONFIG_PATH" | head -n 1 | sed 's/^ *//')/background = \"#2f9b85\"/g" "$DUNST_CONFIG_PATH"
	sed -i "s/$(grep "frame_color =" "$DUNST_CONFIG_PATH" | head -n 1 | sed 's/^ *//')/frame_color = \"#2f9b85\"/g" "$DUNST_CONFIG_PATH"
	sed -i "s/$(grep "separator_color =" "$DUNST_CONFIG_PATH" | head -n 1 | sed 's/^ *//')/separator_color = \"#247565\"/g" "$DUNST_CONFIG_PATH"

}

DUNST_MATCHA_ALIZ_SCHEME() {

	sed -i "s/$(grep "background =" $HOME/.config/dunst/dunstrc | head -n 1 | sed 's/^ *//')/background = \"#e44138\"/g" $HOME/.config/dunst/dunstrc
	sed -i "s/$(grep "frame_color =" $HOME/.config/dunst/dunstrc | head -n 1 | sed 's/^ *//')/frame_color = \"#e44138\"/g" $HOME/.config/dunst/dunstrc
	sed -i "s/$(grep "separator_color =" $HOME/.config/dunst/dunstrc | head -n 1 | sed 's/^ *//')/separator_color = \"#752e24\"/g" $HOME/.config/dunst/dunstrc

}

DUNST_MATCHA_AZUL_SCHEME() {

	sed -i "s/$(grep "background =" $HOME/.config/dunst/dunstrc | head -n 1 | sed 's/^ *//')/background = \"#3498db\"/g" $HOME/.config/dunst/dunstrc
	sed -i "s/$(grep "frame_color =" $HOME/.config/dunst/dunstrc | head -n 1 | sed 's/^ *//')/frame_color = \"#3498db\"/g" $HOME/.config/dunst/dunstrc
	sed -i "s/$(grep "separator_color =" $HOME/.config/dunst/dunstrc | head -n 1 | sed 's/^ *//')/separator_color = \"#245775\"/g" $HOME/.config/dunst/dunstrc

}

#######################################################################################################################################################

### MAIN MENU BOX

	MAIN_BOX=$($ZEN_MENU --title="$MAIN_WINDOW_TITLE" --text="$MAIN_WINDOW_TEXT" --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
		--column="pick" --column="$OPTION_COLUMN" --column="$DESCRIPTION_COLUMN" --window-icon="$ICON_PATH" \
		TRUE "$GLOBAL_MATCHA_SEA" "$GLOBAL_MATCHA_SEA_DESCRIPTION" \
		FALSE "$GLOBAL_MATCHA_ALIZ" "$GLOBAL_MATCHA_ALIZ_DESCRIPTION" \
		FALSE "$GLOBAL_MATCHA_AZUL" "$GLOBAL_MATCHA_AZUL_DESCRIPTION" \
		FALSE "$MMENU_LIGHT" "$MMENU_LIGHT_DESCRIPTION" \
		FALSE "$MMENU_DARK" "$MMENU_LIGHT_DESCRIPTION" \
		FALSE "$RCMENU_LIGHT" "$RCMENU_LIGHT_DESCRIPTION" \
		FALSE "$RCMENU_DARK" "$RCMENU_DARK_DESCRIPTION" \
		FALSE "$PANEL_MENUS" "$PANEL_MENUS_DESCRIPTION" \
		FALSE "$SMENU_LIGHT" "$SMENU_LIGHT_DESCRIPTION" \
		FALSE "$SMENU_DARK" "$SMENU_DARK_DESCRIPTION" \
		FALSE "$TINT_LIGHT_PANEL" "$TINT_LIGHT_PANEL_DESCRIPTION" \
		FALSE "$TINT_DARK_PANEL" "$TINT_DARK_PANEL_DESCRIPTION" \
		2>/dev/null)

	case $MAIN_BOX in

		$GLOBAL_MATCHA_SEA )

			GTK_MATCHA_SEA_THEME_CONFIGS
			QT_MATCHA_SEA_SCHEME
			MMENU_MATCHA_SEA_SCHEME
			RCLICK_MENU_MATCHA_SEA_SCHEME
			PANEL_MENUS_MATCHA_SEA_SCHEME
			SIDE_MENU_MATCHA_SEA_SCHEME
			TINT2_PANEL_MATCHA_SEA_SCHEME
			pkexec rb-colors-global.sh --matcha-sea
			DUNST_MATCHA_SEA_SCHEME
			killall dunst

			openbox --reconfigure
			sleep 1
			openbox --restart
			sleep 3
			manjaro-tint2restart 2>/dev/null
			sleep 1
			notify-send -t 2600 "Matcha Sea Global theme configured."

		;;

		$GLOBAL_MATCHA_ALIZ )

			GTK_MATCHA_ALIZ_THEME_CONFIGS
			QT_MATCHA_ALIZ_SCHEME
			MMENU_MATCHA_ALIZ_SCHEME
			RCLICK_MENU_MATCHA_ALIZ_SCHEME
			PANEL_MENUS_MATCHA_ALIZ_SCHEME
			SIDE_MENU_MATCHA_ALIZ_SCHEME
			TINT2_PANEL_MATCHA_ALIZ_SCHEME
			pkexec rb-colors-global.sh --matcha-aliz
			DUNST_MATCHA_ALIZ_SCHEME
			killall dunst

			openbox --reconfigure
			sleep 1
			openbox --restart
			sleep 3
			manjaro-tint2restart 2>/dev/null
			sleep 1
			notify-send -t 2600 "Matcha Aliz Global theme configured."

		;;

		$GLOBAL_MATCHA_AZUL )

			GTK_MATCHA_AZUL_THEME_CONFIGS
			QT_MATCHA_AZUL_SCHEME
			MMENU_MATCHA_AZUL_SCHEME
			RCLICK_MENU_MATCHA_AZUL_SCHEME
			PANEL_MENUS_MATCHA_AZUL_SCHEME
			SIDE_MENU_MATCHA_AZUL_SCHEME
			TINT2_PANEL_MATCHA_AZUL_SCHEME
			pkexec rb-colors-global.sh --matcha-azul
			DUNST_MATCHA_AZUL_SCHEME
			killall dunst

			openbox --reconfigure
			sleep 1
			openbox --restart
			sleep 3
			manjaro-tint2restart 2>/dev/null
			sleep 1
			notify-send -t 2600 "Matcha Azul Global theme configured."

		;;

		$MMENU_LIGHT )

				ITEM_BOX=$($ZEN_MENU --title="$MMENU_TITLE_LIGHT" --text="$MMENU_TEXT_LIGHT" --ok-label="$OK_BUTTON" \
				--cancel-label="$CANCEL_BUTTON" --column="pick" --column="option" --extra-button="$EXTRA_BUTTON" --window-icon="$ICON_PATH" \
				TRUE "$MENU_BACKGROUND" \
				FALSE "$MENU_BORDER" \
				FALSE "$ITEM_BACKGROUND" \
				FALSE "$ITEM_FOREGROUND" \
				FALSE "$SELECTED_ITEM_BACKGROUND" \
				FALSE "$SELECTED_ITEM_FOREGROUND" \
				FALSE "$SELECTED_ITEM_BORDER" \
				FALSE "$SEPARATOR_FOREGROUND" \
				FALSE "$SCROLL_INDICATOR" \
				FALSE "$SEPARATOR_MARKUP" \
				FALSE "$PREPEND_RECT" \
				FALSE "$PREPEND_SEARCH" \
				2>/dev/null)

				case $ITEM_BOX in

					$MENU_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_MENU_BG_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_menu_bg = $MMENU_COLOR_MENU_BG_LIGHT/color_menu_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu

									if [ "$MODE_TEST" = "sidemenu-light-rc" ]; then

										sed -i "s/color_menu_bg = $MMENU_COLOR_MENU_BG_LIGHT/color_menu_bg = $COLOR/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$MENU_BORDER )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_MENU_BORDER_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_menu_border = $MMENU_COLOR_MENU_BORDER_LIGHT/color_menu_border = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu

									if [ "$MODE_TEST" = "sidemenu-light-rc" ]; then

										sed -i "s/color_menu_border = $MMENU_COLOR_MENU_BORDER_LIGHT/color_menu_border = $COLOR/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$ITEM_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_NORM_BG_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_norm_bg = $MMENU_COLOR_NORM_BG_LIGHT/color_norm_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu

									if [ "$MODE_TEST" = "sidemenu-light-rc" ]; then

										sed -i "s/color_norm_bg = $MMENU_COLOR_NORM_BG_LIGHT/color_norm_bg = $COLOR/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$ITEM_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_NORM_FG_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_norm_fg = $MMENU_COLOR_NORM_FG_LIGHT/color_norm_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu

									if [ "$MODE_TEST" = "sidemenu-light-rc" ]; then

										sed -i "s/color_norm_fg = $MMENU_COLOR_NORM_FG_LIGHT/color_norm_fg = $COLOR/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_SEL_BG_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_bg = $MMENU_COLOR_SEL_BG_LIGHT/color_sel_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu

									if [ "$MODE_TEST" = "sidemenu-light-rc" ]; then

										sed -i "s/color_sel_bg = $MMENU_COLOR_SEL_BG_LIGHT/color_sel_bg = $COLOR/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_SEL_FG_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_fg = $MMENU_COLOR_SEL_FG_LIGHT/color_sel_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu

									if [ "$MODE_TEST" = "sidemenu-light-rc" ]; then

										sed -i "s/color_sel_fg = $MMENU_COLOR_SEL_FG_LIGHT/color_sel_fg = $COLOR/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_BORDER )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_SEL_BORDER_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_border = $MMENU_COLOR_SEL_BORDER_LIGHT/color_sel_border = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu

									if [ "$MODE_TEST" = "sidemenu-light-rc" ]; then

										sed -i "s/color_sel_border = $MMENU_COLOR_SEL_BORDER_LIGHT/color_sel_border = $COLOR/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SEPARATOR_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_SEP_FG_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sep_fg = $MMENU_COLOR_SEP_FG_LIGHT/color_sep_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu

									if [ "$MODE_TEST" = "sidemenu-light-rc" ]; then

										sed -i "s/color_sep_fg = $MMENU_COLOR_SEP_FG_LIGHT/color_sep_fg = $COLOR/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SCROLL_INDICATOR )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_SCROLL_IND_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_scroll_ind = $MMENU_COLOR_SCROLL_IND_LIGHT/color_scroll_ind = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu

									if [ "$MODE_TEST" = "sidemenu-light-rc" ]; then

										sed -i "s/color_scroll_ind = $MMENU_COLOR_SCROLL_IND_LIGHT/color_scroll_ind = $COLOR/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SEPARATOR_MARKUP )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_SEP_MARKUP_LIGHT" --text="$JG_ENTRY_SEP_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/foreground=\"$MMENU_COLOR_SEP_MARKUP_LIGHT\"/foreground=\"$COLOR\"/" $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu

									if [ "$MODE_TEST" = "sidemenu-light-rc" ]; then

										sed -i "s/foreground=\"$MMENU_COLOR_SEP_MARKUP_LIGHT\"/foreground=\"$COLOR\"/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$PREPEND_RECT )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_RECT_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/$MMENU_COLOR_RECT_LIGHT,#$MENU_RECT/$COLOR,#$MENU_RECT/" $HOME/.config/openbox/jgmenu-theme-configs/light-prepend.csv

									if [ "$MODE_TEST" = "sidemenu-light-rc" ]; then

										sed -i "s/$MMENU_COLOR_RECT_LIGHT,#$MENU_RECT/$COLOR,#$MENU_RECT/" $HOME/.config/jgmenu/prepend.csv

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu" --csv-file="$HOME/.config/openbox/jgmenu-theme-configs/light-prepend.csv" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$PREPEND_SEARCH )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_SEARCH_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/$MMENU_COLOR_SEARCH_LIGHT,#$MENU_SEARCH/$COLOR,#$MENU_SEARCH/" $HOME/.config/openbox/jgmenu-theme-configs/light-prepend.csv

									if [ "$MODE_TEST" = "sidemenu-light-rc" ]; then

									sed -i "s/$MMENU_COLOR_SEARCH_LIGHT,#$MENU_SEARCH/$COLOR,#$MENU_SEARCH/" $HOME/.config/jgmenu/prepend.csv

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu" --csv-file="$HOME/.config/openbox/jgmenu-theme-configs/light-prepend.csv" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$EXTRA_BUTTON )

						exec rb-colors.sh

					;;

					* )

				esac

		;;

		$MMENU_DARK )

				ITEM_BOX=$($ZEN_MENU --title="$MMENU_TITLE_DARK" --text="$MMENU_TEXT_DARK" --ok-label="$OK_BUTTON" \
				--cancel-label="$CANCEL_BUTTON" --column="pick" --column="option" --extra-button="$EXTRA_BUTTON" --window-icon="$ICON_PATH" \
				TRUE "$MENU_BACKGROUND" \
				FALSE "$MENU_BORDER" \
				FALSE "$ITEM_BACKGROUND" \
				FALSE "$ITEM_FOREGROUND" \
				FALSE "$SELECTED_ITEM_BACKGROUND" \
				FALSE "$SELECTED_ITEM_FOREGROUND" \
				FALSE "$SELECTED_ITEM_BORDER" \
				FALSE "$SEPARATOR_FOREGROUND" \
				FALSE "$SCROLL_INDICATOR" \
				FALSE "$SEPARATOR_MARKUP" \
				FALSE "$PREPEND_RECT" \
				FALSE "$PREPEND_SEARCH" \
				2>/dev/null)

				case $ITEM_BOX in

					$MENU_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_MENU_BG_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_menu_bg = $MMENU_COLOR_MENU_BG_DARK/color_menu_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu

									if [ "$MODE_TEST" = "sidemenu-dark-rc" ]; then

										sed -i "s/color_menu_bg = $MMENU_COLOR_MENU_BG_DARK/color_menu_bg = $COLOR/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$MENU_BORDER )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_MENU_BORDER_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_menu_border = $MMENU_COLOR_MENU_BORDER_DARK/color_menu_border = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu

									if [ "$MODE_TEST" = "sidemenu-dark-rc" ]; then

										sed -i "s/color_menu_border = $MMENU_COLOR_MENU_BORDER_DARK/color_menu_border = $COLOR/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$ITEM_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_NORM_BG_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_norm_bg = $MMENU_COLOR_NORM_BG_DARK/color_norm_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu

									if [ "$MODE_TEST" = "sidemenu-dark-rc" ]; then

										sed -i "s/color_norm_bg = $MMENU_COLOR_NORM_BG_DARK/color_norm_bg = $COLOR/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$ITEM_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_NORM_FG_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_norm_fg = $MMENU_COLOR_NORM_FG_DARK/color_norm_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu

									if [ "$MODE_TEST" = "sidemenu-dark-rc" ]; then

										sed -i "s/color_norm_fg = $MMENU_COLOR_NORM_FG_DARK/color_norm_fg = $COLOR/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_SEL_BG_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_bg = $MMENU_COLOR_SEL_BG_DARK/color_sel_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu

									if [ "$MODE_TEST" = "sidemenu-dark-rc" ]; then

										sed -i "s/color_sel_bg = $MMENU_COLOR_SEL_BG_DARK/color_sel_bg = $COLOR/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_SEL_FG_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_fg = $MMENU_COLOR_SEL_FG_DARK/color_sel_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu

									if [ "$MODE_TEST" = "sidemenu-dark-rc" ]; then

										sed -i "s/color_sel_fg = $MMENU_COLOR_SEL_FG_DARK/color_sel_fg = $COLOR/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_BORDER )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_SEL_BORDER_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_border = $MMENU_COLOR_SEL_BORDER_DARK/color_sel_border = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu

									if [ "$MODE_TEST" = "sidemenu-dark-rc" ]; then

										sed -i "s/color_sel_border = $MMENU_COLOR_SEL_BORDER_DARK/color_sel_border = $COLOR/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SEPARATOR_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_SEP_FG_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sep_fg = $MMENU_COLOR_SEP_FG_DARK/color_sep_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu

									if [ "$MODE_TEST" = "sidemenu-dark-rc" ]; then

										sed -i "s/color_sep_fg = $MMENU_COLOR_SEP_FG_DARK/color_sep_fg = $COLOR/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SCROLL_INDICATOR )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_SCROLL_IND_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_scroll_ind = $MMENU_COLOR_SCROLL_IND_DARK/color_scroll_ind = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu

									if [ "$MODE_TEST" = "sidemenu-dark-rc" ]; then

										sed -i "s/color_scroll_ind = $MMENU_COLOR_SCROLL_IND_DARK/color_scroll_ind = $COLOR/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SEPARATOR_MARKUP )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_SEP_MARKUP_DARK" --text="$JG_ENTRY_SEP_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/foreground=\"$MMENU_COLOR_SEP_MARKUP_DARK\"/foreground=\"$COLOR\"/" $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu

									if [ "$MODE_TEST" = "sidemenu-dark-rc" ]; then

										sed -i "s/foreground=\"$MMENU_COLOR_SEP_MARKUP_DARK\"/foreground=\"$COLOR\"/" $HOME/.config/jgmenu/jgmenurc

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$PREPEND_RECT )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_RECT_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/$MMENU_COLOR_RECT_DARK.#$MENU_RECT/$COLOR.#$MENU_RECT/" $HOME/.config/openbox/jgmenu-theme-configs/dark-prepend.csv

									if [ "$MODE_TEST" = "sidemenu-dark-rc" ]; then

									sed -i "s/$MMENU_COLOR_RECT_DARK,#$MENU_RECT/$COLOR,#$MENU_RECT/" $HOME/.config/jgmenu/prepend.csv

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu" --csv-file="$HOME/.config/openbox/jgmenu-theme-configs/dark-prepend.csv" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$PREPEND_SEARCH )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_SEARCH_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/$MMENU_COLOR_SEARCH_DARK,#$MENU_SEARCH/$COLOR,#$MENU_SEARCH/" $HOME/.config/openbox/jgmenu-theme-configs/dark-prepend.csv

									if [ "$MODE_TEST" = "sidemenu-dark-rc" ]; then

									sed -i "s/$MMENU_COLOR_SEARCH_DARK,#$MENU_SEARCH/$COLOR,#$MENU_SEARCH/" $HOME/.config/jgmenu/prepend.csv

									fi

									jgmenu --config-file="$HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu" --csv-file="$HOME/.config/openbox/jgmenu-theme-configs/dark-prepend.csv" 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$EXTRA_BUTTON )

						exec rb-colors.sh

					;;

					* )

				esac

		;;

		$RCMENU_LIGHT )

				ITEM_BOX=$($ZEN_MENU --title="$RCMENU_TITLE_LIGHT" --text="$RCMENU_TEXT_LIGHT" --ok-label="$OK_BUTTON" \
				--cancel-label="$CANCEL_BUTTON" --column="pick" --column="option" --extra-button="$EXTRA_BUTTON" --window-icon="$ICON_PATH" \
				TRUE "$MENU_BACKGROUND" \
				FALSE "$MENU_BORDER" \
				FALSE "$ITEM_BACKGROUND" \
				FALSE "$ITEM_FOREGROUND" \
				FALSE "$SELECTED_ITEM_BACKGROUND" \
				FALSE "$SELECTED_ITEM_FOREGROUND" \
				FALSE "$SELECTED_ITEM_BORDER" \
				FALSE "$SEPARATOR_FOREGROUND" \
				FALSE "$SCROLL_INDICATOR" \
				FALSE "$SEPARATOR_MARKUP" \
				2>/dev/null)

				case $ITEM_BOX in

					$MENU_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_MENU_BG_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )


									sed -i "s/color_menu_bg = $RCMENU_COLOR_MENU_BG_LIGHT/color_menu_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$MENU_BORDER )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_MENU_BORDER_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_menu_border = $RCMENU_COLOR_MENU_BORDER_LIGHT/color_menu_border = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$ITEM_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_NORM_BG_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_norm_bg = $RCMENU_COLOR_NORM_BG_LIGHT/color_norm_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$ITEM_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_NORM_FG_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_norm_fg = $RCMENU_COLOR_NORM_FG_LIGHT/color_norm_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_SEL_BG_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_bg = $RCMENU_COLOR_SEL_BG_LIGHT/color_sel_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_SEL_FG_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_fg = $RCMENU_COLOR_SEL_FG_LIGHT/color_sel_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_BORDER )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_SEL_BORDER_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_border = $RCMENU_COLOR_SEL_BORDER_LIGHT/color_sel_border = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SEPARATOR_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_SEP_FG_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sep_fg = $RCMENU_COLOR_SEP_FG_LIGHT/color_sep_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SCROLL_INDICATOR )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_SCROLL_IND_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_scroll_ind = $RCMENU_COLOR_SCROLL_IND_LIGHT/color_scroll_ind = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SEPARATOR_MARKUP )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_SEP_MARKUP_LIGHT" --text="$JG_ENTRY_SEP_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/foreground=\"$RCMENU_COLOR_SEP_MARKUP_LIGHT\"/foreground=\"$COLOR\"/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_light-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$EXTRA_BUTTON )

						exec rb-colors.sh

					;;

					* )

				esac

		;;

		$RCMENU_DARK )

				ITEM_BOX=$($ZEN_MENU --title="$RCMENU_TITLE_DARK" --text="$RCMENU_TEXT_DARK" --ok-label="$OK_BUTTON" \
				--cancel-label="$CANCEL_BUTTON" --column="pick" --column="option" --extra-button="$EXTRA_BUTTON" --window-icon="$ICON_PATH" \
				TRUE "$MENU_BACKGROUND" \
				FALSE "$MENU_BORDER" \
				FALSE "$ITEM_BACKGROUND" \
				FALSE "$ITEM_FOREGROUND" \
				FALSE "$SELECTED_ITEM_BACKGROUND" \
				FALSE "$SELECTED_ITEM_FOREGROUND" \
				FALSE "$SELECTED_ITEM_BORDER" \
				FALSE "$SEPARATOR_FOREGROUND" \
				FALSE "$SCROLL_INDICATOR" \
				FALSE "$SEPARATOR_MARKUP" \
				2>/dev/null)

				case $ITEM_BOX in

					$MENU_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_MENU_BG_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_menu_bg = $RCMENU_COLOR_MENU_BG_DARK/color_menu_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$MENU_BORDER )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_MENU_BORDER_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_menu_border = $RCMENU_COLOR_MENU_BORDER_DARK/color_menu_border = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$ITEM_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_NORM_BG_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_norm_bg = $RCMENU_COLOR_NORM_BG_DARK/color_norm_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$ITEM_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_NORM_FG_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_norm_fg = $RCMENU_COLOR_NORM_FG_DARK/color_norm_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_SEL_BG_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_bg = $RCMENU_COLOR_SEL_BG_DARK/color_sel_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_SEL_FG_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_fg = $RCMENU_COLOR_SEL_FG_DARK/color_sel_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_BORDER )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_SEL_BORDER_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_border = $RCMENU_COLOR_SEL_BORDER_DARK/color_sel_border = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SEPARATOR_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_SEP_FG_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sep_fg = $RCMENU_COLOR_SEP_FG_DARK/color_sep_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SCROLL_INDICATOR )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_SCROLL_IND_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_scroll_ind = $RCMENU_COLOR_SCROLL_IND_DARK/color_scroll_ind = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SEPARATOR_MARKUP )

						COLOR=$(zenity --entry --entry-text="$RCMENU_COLOR_SEP_MARKUP_DARK" --text="$JG_ENTRY_SEP_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/foreground=\"$RCMENU_COLOR_SEP_MARKUP_DARK\"/foreground=\"$COLOR\"/" $HOME/.config/openbox/jgmenu-theme-configs/right_click_dark-rc
									scripts-box.sh start_rclick_menu
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$EXTRA_BUTTON )

						exec rb-colors.sh

					;;

					* )

				esac

		;;

		$PANEL_MENUS )

				ITEM_BOX=$($ZEN_MENU --title="$PANEL_MENUS_TITLE" --text="$PANEL_MENUS_TEXT" --ok-label="$OK_BUTTON" \
				--cancel-label="$CANCEL_BUTTON" --column="pick" --column="option" --extra-button="$EXTRA_BUTTON" --window-icon="$ICON_PATH" \
				TRUE "$MENU_BACKGROUND" \
				FALSE "$MENU_BORDER" \
				FALSE "$ITEM_BACKGROUND" \
				FALSE "$ITEM_FOREGROUND" \
				FALSE "$SELECTED_ITEM_BACKGROUND" \
				FALSE "$SELECTED_ITEM_FOREGROUND" \
				FALSE "$SELECTED_ITEM_BORDER" \
				FALSE "$SEPARATOR_FOREGROUND" \
				FALSE "$SCROLL_INDICATOR" \
				FALSE "$SEPARATOR_MARKUP" \
				2>/dev/null)

				case $ITEM_BOX in

					$MENU_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$PMENUS_COLOR_MENU_BG" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_menu_bg = $PMENUS_COLOR_MENU_BG/color_menu_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc
									sed -i "s/color_menu_bg = $PMENUS_COLOR_MENU_BG/color_menu_bg = $COLOR/" $HOME/.config/openbox/key-bindings

									tint2-buttons.sh places 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$MENU_BORDER )

						COLOR=$(zenity --entry --entry-text="$PMENUS_COLOR_MENU_BORDER" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_menu_border = $PMENUS_COLOR_MENU_BORDER/color_menu_border = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc
									sed -i "s/color_menu_border = $PMENUS_COLOR_MENU_BORDER/color_menu_border = $COLOR/" $HOME/.config/openbox/key-bindings

									tint2-buttons.sh places 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$ITEM_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$PMENUS_COLOR_NORM_BG" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )
									
									sed -i "s/color_norm_bg = $PMENUS_COLOR_NORM_BG/color_norm_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc
									sed -i "s/color_norm_bg = $PMENUS_COLOR_NORM_BG/color_norm_bg = $COLOR/" $HOME/.config/openbox/key-bindings

									tint2-buttons.sh places 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$ITEM_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$PMENUS_COLOR_NORM_FG" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_norm_fg = $PMENUS_COLOR_NORM_FG/color_norm_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc
									sed -i "s/color_norm_fg = $PMENUS_COLOR_NORM_FG/color_norm_fg = $COLOR/" $HOME/.config/openbox/key-bindings

									tint2-buttons.sh places 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$PMENUS_COLOR_SEL_BG" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_bg = $PMENUS_COLOR_SEL_BG/color_sel_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc
									sed -i "s/color_sel_bg = $PMENUS_COLOR_SEL_BG/color_sel_bg = $COLOR/" $HOME/.config/openbox/key-bindings

									tint2-buttons.sh places 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$PMENUS_COLOR_SEL_FG" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_fg = $PMENUS_COLOR_SEL_FG/color_sel_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc
									sed -i "s/color_sel_fg = $PMENUS_COLOR_SEL_FG/color_sel_fg = $COLOR/" $HOME/.config/openbox/key-bindings

									tint2-buttons.sh places 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_BORDER )

						COLOR=$(zenity --entry --entry-text="$PMENUS_COLOR_SEL_BORDER" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_border = $PMENUS_COLOR_SEL_BORDER/color_sel_border = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc
									sed -i "s/color_sel_border = $PMENUS_COLOR_SEL_BORDER/color_sel_border = $COLOR/" $HOME/.config/openbox/key-bindings

									tint2-buttons.sh places 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SEPARATOR_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$PMENUS_COLOR_SEP_FG" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sep_fg = $PMENUS_COLOR_SEP_FG/color_sep_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc
									sed -i "s/color_sep_fg = $PMENUS_COLOR_SEP_FG/color_sep_fg = $COLOR/" $HOME/.config/openbox/key-bindings

									tint2-buttons.sh places 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SCROLL_INDICATOR )

						COLOR=$(zenity --entry --entry-text="$PMENUS_COLOR_SCROLL_IND" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_scroll_ind = $PMENUS_COLOR_SCROLL_IND/color_scroll_ind = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc
									sed -i "s/color_scroll_ind = $PMENUS_COLOR_SCROLL_IND/color_scroll_ind = $COLOR/" $HOME/.config/openbox/key-bindings

									tint2-buttons.sh places 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SEPARATOR_MARKUP )

						COLOR=$(zenity --entry --entry-text="$PMENUS_COLOR_SEP_MARKUP" --text="$JG_ENTRY_SEP_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/foreground=\"$PMENUS_COLOR_SEP_MARKUP\"/foreground=\"$COLOR\"/" $HOME/.config/openbox/jgmenu-theme-configs/panel-menus-rc
									sed -i "s/foreground=\"$PMENUS_COLOR_SEP_MARKUP\"/foreground=\"$COLOR\"/" $HOME/.config/openbox/key-bindings

									tint2-buttons.sh places 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$EXTRA_BUTTON )

						exec rb-colors.sh

					;;

					* )

				esac

		;;

		$SMENU_LIGHT )

				ITEM_BOX=$($ZEN_MENU --title="$SMENU_TITLE_LIGHT" --text="$SMENU_TEXT_LIGHT" --ok-label="$OK_BUTTON" \
				--cancel-label="$CANCEL_BUTTON" --column="pick" --column="option" --extra-button="$EXTRA_BUTTON" --window-icon="$ICON_PATH" \
				TRUE "$MENU_BACKGROUND" \
				FALSE "$MENU_BORDER" \
				FALSE "$ITEM_BACKGROUND" \
				FALSE "$ITEM_FOREGROUND" \
				FALSE "$SELECTED_ITEM_BACKGROUND" \
				FALSE "$SELECTED_ITEM_FOREGROUND" \
				FALSE "$SELECTED_ITEM_BORDER" \
				FALSE "$SEPARATOR_FOREGROUND" \
				FALSE "$SCROLL_INDICATOR" \
				FALSE "$SEPARATOR_MARKUP" \
				2>/dev/null)

				case $ITEM_BOX in

					$MENU_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$SMENU_COLOR_MENU_BG_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")

							case $? in

								0 )

									sed -i "s/color_menu_bg = $SMENU_COLOR_MENU_BG_LIGHT/color_menu_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$MENU_BORDER )

						COLOR=$(zenity --entry --entry-text="$SMENU_COLOR_MENU_BORDER_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_menu_border = $SMENU_COLOR_MENU_BORDER_LIGHT/color_menu_border = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$ITEM_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$SMENU_COLOR_NORM_BG_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_norm_bg = $SMENU_COLOR_NORM_BG_LIGHT/color_norm_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$ITEM_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$SMENU_COLOR_NORM_FG_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_norm_fg = $SMENU_COLOR_NORM_FG_LIGHT/color_norm_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$SMENU_COLOR_SEL_BG_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_bg = $SMENU_COLOR_SEL_BG_LIGHT/color_sel_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$SMENU_COLOR_SEL_FG_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_fg = $SMENU_COLOR_SEL_FG_LIGHT/color_sel_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_BORDER )

						COLOR=$(zenity --entry --entry-text="$SMENU_COLOR_SEL_BORDER_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_border = $SMENU_COLOR_SEL_BORDER_LIGHT/color_sel_border = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SEPARATOR_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$SMENU_COLOR_SEP_FG_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sep_fg = $SMENU_COLOR_SEP_FG_LIGHT/color_sep_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SCROLL_INDICATOR )

						COLOR=$(zenity --entry --entry-text="$SMENU_COLOR_SCROLL_IND_LIGHT" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_scroll_ind = $SMENU_COLOR_SCROLL_IND_LIGHT/color_scroll_ind = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SEPARATOR_MARKUP )

						COLOR=$(zenity --entry --entry-text="$MMENU_COLOR_SEP_MARKUP_LIGHT" --text="$JG_ENTRY_SEP_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/foreground=\"$MMENU_COLOR_SEP_MARKUP_LIGHT\"/foreground=\"$COLOR\"/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$EXTRA_BUTTON )

						exec rb-colors.sh

					;;

					* )

				esac

		;;

		$SMENU_DARK )

				ITEM_BOX=$($ZEN_MENU --title="$SMENU_TITLE_DARK" --text="$SMENU_TEXT_DARK" --ok-label="$OK_BUTTON" \
				--cancel-label="$CANCEL_BUTTON" --column="pick" --column="option" --extra-button="$EXTRA_BUTTON" --window-icon="$ICON_PATH" \
				TRUE "$MENU_BACKGROUND" \
				FALSE "$MENU_BORDER" \
				FALSE "$ITEM_BACKGROUND" \
				FALSE "$ITEM_FOREGROUND" \
				FALSE "$SELECTED_ITEM_BACKGROUND" \
				FALSE "$SELECTED_ITEM_FOREGROUND" \
				FALSE "$SELECTED_ITEM_BORDER" \
				FALSE "$SEPARATOR_FOREGROUND" \
				FALSE "$SCROLL_INDICATOR" \
				FALSE "$SEPARATOR_MARKUP" \
				2>/dev/null)

				case $ITEM_BOX in

					$MENU_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$SMENU_COLOR_MENU_BG_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_menu_bg = $SMENU_COLOR_MENU_BG_DARK/color_menu_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$MENU_BORDER )

						COLOR=$(zenity --entry --entry-text="$SMENU_COLOR_MENU_BORDER_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_menu_border = $SMENU_COLOR_MENU_BORDER_DARK/color_menu_border = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$ITEM_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$SMENU_COLOR_NORM_BG_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_norm_bg = $SMENU_COLOR_NORM_BG_DARK/color_norm_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$ITEM_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$SMENU_COLOR_NORM_FG_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_norm_fg = $SMENU_COLOR_NORM_FG_DARK/color_norm_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_BACKGROUND )

						COLOR=$(zenity --entry --entry-text="$SMENU_COLOR_SEL_BG_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_bg = $SMENU_COLOR_SEL_BG_DARK/color_sel_bg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$SMENU_COLOR_SEL_FG_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_fg = $SMENU_COLOR_SEL_FG_DARK/color_sel_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SELECTED_ITEM_BORDER )

						COLOR=$(zenity --entry --entry-text="$SMENU_COLOR_SEL_BORDER_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sel_border = $SMENU_COLOR_SEL_BORDER_DARK/color_sel_border = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SEPARATOR_FOREGROUND )

						COLOR=$(zenity --entry --entry-text="$SMENU_COLOR_SEP_FG_DARK" --text="$ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/color_sep_fg = $SMENU_COLOR_SEP_FG_DARK/color_sep_fg = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SCROLL_INDICATOR )

						COLOR=$(zenity --entry --entry-text="$SMENU_COLOR_SCROLL_IND_DARK" --text="$JG_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )


									sed -i "s/color_scroll_ind = $SMENU_COLOR_SCROLL_IND_DARK/color_scroll_ind = $COLOR/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$SEPARATOR_MARKUP )

						COLOR=$(zenity --entry --entry-text="$SMENU_COLOR_SEP_MARKUP_DARK" --text="$JG_ENTRY_SEP_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/foreground=\"$SMENU_COLOR_SEP_MARKUP_DARK\"/foreground=\"$COLOR\"/" $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc

									tint2-buttons.sh sidemenu 2>/dev/null
									exec rb-colors.sh

								;;

								1 )
			
									exec rb-colors.sh

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$EXTRA_BUTTON )

						exec rb-colors.sh

					;;

					* )

				esac

		;;

		$TINT_LIGHT_PANEL )

			ITEM_BOX=$($ZEN_MENU --title="$TINT_PANEL_TITLE_LIGHT" --text="$TINT_PANEL_TEXT_LIGHT" --ok-label="$OK_BUTTON" \
			--cancel-label="$CANCEL_BUTTON" --column="$PICK_COLUMN" --column="$OPTION_COLUMN" --column="$DESCRIPTION_COLUMN" \
			--extra-button="$EXTRA_BUTTON" --window-icon="$ICON_PATH" \
			TRUE "$ACCENTS_COLOR_MENU" "$ACCENTS_COLOR_MENU_DESCRIPTION" \
			FALSE "$BUTTONS_COLOR_MENU" "$BUTTONS_COLOR_MENU_DESCRIPTION" \
			FALSE "$TASKBAR_COLOR_MENU" "$TASKBAR_COLOR_MENU_DESCRIPTION" \
			FALSE "$FONT_COLOR_GENERAL" "$FONT_COLOR_GENERAL_DESCRIPTION" \
			FALSE "$TASKBAR_BUTTONS_FONT_COLOR" "$TASKBAR_BUTTONS_FONT_COLOR_DESCRIPTION" \
			2>/dev/null)

				case $ITEM_BOX in

					$ACCENTS_COLOR_MENU)

						ACCENTS_COLOR_BOX=$(zenity --entry --entry-text="$LIGHT_ACCENTS_COLOR_FIND" --title="$ENTRY_BOX_TITLE" --text="$TINT_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/$LIGHT_ACCENTS_COLOR_FIND/$ACCENTS_COLOR_BOX/" $HOME/.config/tint2/recbox-daily-light.tint2rc
									sed -i "s/$LIGHT_ACCENTS_COLOR_FIND/$ACCENTS_COLOR_BOX/" $HOME/.config/tint2/recbox-studio-light.tint2rc

									if [ "$MODE_TEST" = "sidemenu-light-rc" ]; then

										sed -i "s/$LIGHT_ACCENTS_COLOR_FIND/$ACCENTS_COLOR_BOX/" $HOME/.config/tint2/tint2rc
										manjaro-tint2restart 2>/dev/null

									fi

									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$BUTTONS_COLOR_MENU )

						BUTTONS_COLOR_BOX=$(zenity --entry --entry-text="$LIGHT_BUTTONS_COLOR_FIND" --title="$ENTRY_BOX_TITLE" --text="$TINT_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/$LIGHT_BUTTONS_COLOR_FIND/$BUTTONS_COLOR_BOX/" $HOME/.config/tint2/recbox-daily-light.tint2rc
									sed -i "s/$LIGHT_BUTTONS_COLOR_FIND/$BUTTONS_COLOR_BOX/" $HOME/.config/tint2/recbox-studio-light.tint2rc

									if [ "$MODE_TEST" = "sidemenu-light-rc" ]; then

										sed -i "s/$LIGHT_BUTTONS_COLOR_FIND/$BUTTONS_COLOR_BOX/" $HOME/.config/tint2/tint2rc
										manjaro-tint2restart 2>/dev/null

									fi

									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$TASKBAR_COLOR_MENU )

						TASKBAR_COLOR_BOX=$(zenity --entry --entry-text="$LIGHT_TASKBAR_COLOR_FIND" --title="$ENTRY_BOX_TITLE" --text="$TINT_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/$LIGHT_TASKBAR_COLOR_FIND/$TASKBAR_COLOR_BOX/" $HOME/.config/tint2/recbox-daily-light.tint2rc
									sed -i "s/$LIGHT_TASKBAR_COLOR_FIND/$TASKBAR_COLOR_BOX/" $HOME/.config/tint2/recbox-studio-light.tint2rc

									if [ "$MODE_TEST" = "sidemenu-light-rc" ]; then

										sed -i "s/$LIGHT_TASKBAR_COLOR_FIND/$TASKBAR_COLOR_BOX/" $HOME/.config/tint2/tint2rc
										manjaro-tint2restart 2>/dev/null

									fi

									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$FONT_COLOR_GENERAL )

						FONT_COLOR_GENERAL_BOX=$(zenity --entry --entry-text="$LIGHT_FONT_COLOR_GENERAL_FIND" --title="$ENTRY_BOX_TITLE" --text="$TINT_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/$LIGHT_FONT_COLOR_GENERAL_FIND/$FONT_COLOR_GENERAL_BOX/" $HOME/.config/tint2/recbox-daily-light.tint2rc
									sed -i "s/$LIGHT_FONT_COLOR_GENERAL_FIND/$FONT_COLOR_GENERAL_BOX/" $HOME/.config/tint2/recbox-studio-light.tint2rc

									if [ "$MODE_TEST" = "sidemenu-light-rc" ]; then

										sed -i "s/$LIGHT_FONT_COLOR_GENERAL_FIND/$FONT_COLOR_GENERAL_BOX/" $HOME/.config/tint2/tint2rc
										manjaro-tint2restart 2>/dev/null

									fi

									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$TASKBAR_BUTTONS_FONT_COLOR )

						TASKBAR_BUTTONS_FONT_COLOR_BOX=$(zenity --entry --entry-text="$LIGHT_TASKBAR_BUTTONS_FONT_COLOR_FIND" --title="$ENTRY_BOX_TITLE" --text="$TINT_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/$LIGHT_TASKBAR_BUTTONS_FONT_COLOR_FIND/$TASKBAR_BUTTONS_FONT_COLOR_BOX/" $HOME/.config/tint2/recbox-daily-light.tint2rc
									sed -i "s/$LIGHT_TASKBAR_BUTTONS_FONT_COLOR_FIND/$TASKBAR_BUTTONS_FONT_COLOR_BOX/" $HOME/.config/tint2/recbox-studio-light.tint2rc

									if [ "$MODE_TEST" = "sidemenu-light-rc" ]; then

										sed -i "s/$LIGHT_TASKBAR_BUTTONS_FONT_COLOR_FIND/$TASKBAR_BUTTONS_FONT_COLOR_BOX/" $HOME/.config/tint2/tint2rc
										manjaro-tint2restart 2>/dev/null

									fi

									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$EXTRA_BUTTON )

						exec rb-colors.sh

					;;

					* )

				esac

				;;

		$TINT_DARK_PANEL )

			ITEM_BOX=$($ZEN_MENU --title="$TINT_PANEL_TITLE_DARK" --text="$TINT_PANEL_TEXT_DARK" --ok-label="$OK_BUTTON" \
			--cancel-label="$CANCEL_BUTTON" --column="$PICK_COLUMN" --column="$OPTION_COLUMN" --column="$DESCRIPTION_COLUMN" \
			--extra-button="$EXTRA_BUTTON" --window-icon="$ICON_PATH" \
			TRUE "$ACCENTS_COLOR_MENU" "$ACCENTS_COLOR_MENU_DESCRIPTION" \
			FALSE "$BUTTONS_COLOR_MENU" "$BUTTONS_COLOR_MENU_DESCRIPTION" \
			FALSE "$TASKBAR_COLOR_MENU" "$TASKBAR_COLOR_MENU_DESCRIPTION" \
			FALSE "$FONT_COLOR_GENERAL" "$FONT_COLOR_GENERAL_DESCRIPTION" \
			FALSE "$TASKBAR_BUTTONS_FONT_COLOR" "$TASKBAR_BUTTONS_FONT_COLOR_DESCRIPTION" \
			2>/dev/null)

				case $ITEM_BOX in

					$ACCENTS_COLOR_MENU)

						ACCENTS_COLOR_BOX=$(zenity --entry --entry-text="$DARK_ACCENTS_COLOR_FIND" --title="$ENTRY_BOX_TITLE" --text="$TINT_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/$DARK_ACCENTS_COLOR_FIND/$ACCENTS_COLOR_BOX/" $HOME/.config/tint2/recbox-daily-dark.tint2rc
									sed -i "s/$DARK_ACCENTS_COLOR_FIND/$ACCENTS_COLOR_BOX/" $HOME/.config/tint2/recbox-studio-dark.tint2rc

									if [ "$MODE_TEST" = "sidemenu-dark-rc" ]; then

										sed -i "s/$DARK_ACCENTS_COLOR_FIND/$ACCENTS_COLOR_BOX/" $HOME/.config/tint2/tint2rc
										manjaro-tint2restart 2>/dev/null

									fi

									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$BUTTONS_COLOR_MENU )

						BUTTONS_COLOR_BOX=$(zenity --entry --entry-text="$DARK_BUTTONS_COLOR_FIND" --title="$ENTRY_BOX_TITLE" --text="$TINT_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/$DARK_BUTTONS_COLOR_FIND/$BUTTONS_COLOR_BOX/" $HOME/.config/tint2/recbox-daily-dark.tint2rc
									sed -i "s/$DARK_BUTTONS_COLOR_FIND/$BUTTONS_COLOR_BOX/" $HOME/.config/tint2/recbox-studio-dark.tint2rc

									if [ "$MODE_TEST" = "sidemenu-dark-rc" ]; then

										sed -i "s/$DARK_BUTTONS_COLOR_FIND/$BUTTONS_COLOR_BOX/" $HOME/.config/tint2/tint2rc
										manjaro-tint2restart 2>/dev/null

									fi

									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$TASKBAR_COLOR_MENU )

						TASKBAR_COLOR_BOX=$(zenity --entry --entry-text="$DARK_TASKBAR_COLOR_FIND" --title="$ENTRY_BOX_TITLE" --text="$TINT_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/$DARK_TASKBAR_COLOR_FIND/$TASKBAR_COLOR_BOX/" $HOME/.config/tint2/recbox-daily-dark.tint2rc
									sed -i "s/$DARK_TASKBAR_COLOR_FIND/$TASKBAR_COLOR_BOX/" $HOME/.config/tint2/recbox-studio-dark.tint2rc

									if [ "$MODE_TEST" = "sidemenu-dark-rc" ]; then

										sed -i "s/$DARK_TASKBAR_COLOR_FIND/$TASKBAR_COLOR_BOX/" $HOME/.config/tint2/tint2rc
										manjaro-tint2restart 2>/dev/null

									fi

									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$FONT_COLOR_GENERAL )

						FONT_COLOR_GENERAL_BOX=$(zenity --entry --entry-text="$DARK_FONT_COLOR_GENERAL_FIND" --title="$ENTRY_BOX_TITLE" --text="$TINT_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/$DARK_FONT_COLOR_GENERAL_FIND/$FONT_COLOR_GENERAL_BOX/" $HOME/.config/tint2/recbox-daily-dark.tint2rc
									sed -i "s/$DARK_FONT_COLOR_GENERAL_FIND/$FONT_COLOR_GENERAL_BOX/" $HOME/.config/tint2/recbox-studio-dark.tint2rc

									if [ "$MODE_TEST" = "sidemenu-dark-rc" ]; then

										sed -i "s/$DARK_FONT_COLOR_GENERAL_FIND/$FONT_COLOR_GENERAL_BOX/" $HOME/.config/tint2/tint2rc
										manjaro-tint2restart 2>/dev/null

									fi

									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$TASKBAR_BUTTONS_FONT_COLOR )

						TASKBAR_BUTTONS_FONT_COLOR_BOX=$(zenity --entry --entry-text="$DARK_TASKBAR_BUTTONS_FONT_COLOR_FIND" --title="$ENTRY_BOX_TITLE" --text="$TINT_ENTRY_BOX_TEXT" --window-icon="$ICON_PATH")
	
							case $? in

								0 )

									sed -i "s/$DARK_TASKBAR_BUTTONS_FONT_COLOR_FIND/$TASKBAR_BUTTONS_FONT_COLOR_BOX/" $HOME/.config/tint2/recbox-daily-dark.tint2rc
									sed -i "s/$DARK_TASKBAR_BUTTONS_FONT_COLOR_FIND/$TASKBAR_BUTTONS_FONT_COLOR_BOX/" $HOME/.config/tint2/recbox-studio-dark.tint2rc

									if [ "$MODE_TEST" = "sidemenu-dark-rc" ]; then

										sed -i "s/$DARK_TASKBAR_BUTTONS_FONT_COLOR_FIND/$TASKBAR_BUTTONS_FONT_COLOR_BOX/" $HOME/.config/tint2/tint2rc
										manjaro-tint2restart 2>/dev/null

									fi

									exec rb-colors.sh

								;;

								1 )
			
									echo "No option chosen."

								;;

								-1 )

									zenity --error --width=140

								;;

							esac

					;;

					$EXTRA_BUTTON )

						exec rb-colors.sh

					;;

					* )

				esac

		;;

		* )

	esac