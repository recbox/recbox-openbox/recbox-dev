#!/usr/bin/sh

# Credits:
# merging lines: Enrico Maria De Angelis
# https://askubuntu.com/questions/164056/how-do-i-combine-all-lines-in-a-text-file-into-a-single-line

NIGHT_LIGHT=$(grep NIGHT_LIGHT $HOME/.config/night-light-settings/config | cut -d "'" -f 2)
GAMMA=$(grep 'XRANDR_USER_GAMMA=' $HOME/.config/night-light-settings/config | cut -d "'" -f 2)
GAMMA_DEFAULT=$(grep 'XRANDR_DEFAULT_GAMMA=' $HOME/.config/night-light-settings/config | cut -d "'" -f 2)
BRGHTNS=$(grep 'XRANDR_USER_BRGHTNS=' $HOME/.config/night-light-settings/config | cut -d "'" -f 2)
BRGHTNS_DEFAULT=$(grep 'XRANDR_DEFAULT_BRGHTNS=' $HOME/.config/night-light-settings/config | cut -d "'" -f 2)
CURRENT=$(grep XRANDR_CURRENT $HOME/.config/night-light-settings/config | cut -d "'" -f 2)
XRANDR_RUN=$(xrandr --listactivemonitors | grep -v Monitors | awk '{print $4}' | sed 's/^/--output /' | sed "s/$/ --gamma "$GAMMA" --brightness "$BRGHTNS"/" | sed ':a; N; s/\n/ /; ta' | sed 's/^/xrandr /')
XRANDR_STOP=$(xrandr --listactivemonitors | grep -v Monitors | awk '{print $4}' | sed 's/^/--output /' | sed "s/$/ --gamma "$GAMMA_DEFAULT" --brightness "$BRGHTNS_DEFAULT"/" | sed ':a; N; s/\n/ /; ta' | sed 's/^/xrandr /')

	if [ "$NIGHT_LIGHT" = "Redshift" ]; then

		if ! echo "redshift" | ps -A | grep -q redshift; then

			notify-send -t 2300 "Night Light Enabled." -i /usr/share/icons/Flat-Remix-Green/panel/night-light-symbolic.svg; exec redshift

		else

			killall -q redshift; notify-send -t 2300 "Night Light Disabled." -i /usr/share/icons/Flat-Remix-Green/panel/night-light-disabled-symbolic.svg

		fi

	elif [ "$NIGHT_LIGHT" = "Xrandr" ]; then

		if [ "$CURRENT" != "OFF" ]; then

			notify-send -t 2300 "Night Light Disabled." -i /usr/share/icons/Flat-Remix-Green/panel/night-light-disabled-symbolic.svg; sed -i '12s/ON/OFF/' $HOME/.config/night-light-settings/config; exec $XRANDR_STOP

		else

			notify-send -t 2300 "Night Light Enabled." -i /usr/share/icons/Flat-Remix-Green/panel/night-light-symbolic.svg; sed -i '12s/OFF/ON/' $HOME/.config/night-light-settings/config; exec $XRANDR_RUN

		fi

	fi
