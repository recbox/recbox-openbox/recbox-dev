#!/usr/bin/sh

TEST=$(grep 'HandleLidSwitch=' /etc/systemd/logind.conf)

lid_close_suspend_test() {

	if [ "$TEST" = "#HandleLidSwitch=suspend" ]; then
		zenity --info --title="RecBox - Lid Control" --text="Laptop lid is already set to Suspend when closed." --no-wrap
	elif [ "$TEST" = "HandleLidSwitch=suspend" ]; then
		zenity --info --title="RecBox - Lid Control" --text="Laptop lid is already set to Suspend when closed." --no-wrap
	elif [ "$TEST" = "HandleLidSwitch=ignore" ]; then
		pkexec rb-lidctl.sh --suspend
		else
			notify-send "Error"
	fi

}

lid_close_suspend() {

	if [ "$TEST" = "HandleLidSwitch=ignore" ]; then

		sed -i 's/HandleLidSwitch=ignore/HandleLidSwitch=suspend/' /etc/systemd/logind.conf
		systemctl restart systemd-logind

	fi

}

lid_close_ignore_test() {

	if [ "$TEST" = "HandleLidSwitch=ignore" ]; then
		zenity --info --title="RecBox - Lid Control" --text="Laptop lid is already set to Ignore suspend when closed." --no-wrap
	elif [ "$TEST" = "#HandleLidSwitch=suspend" ]; then
		notify-send "pkexec rb-lidctl.sh --ignore"
	elif [ "$TEST" = "HandleLidSwitch=suspend" ]; then
		pkexec rb-lidctl.sh --ignore
		else
			notify-send "Error"
	fi

}

lid_close_ignore() {

	if [ "$TEST" = "#HandleLidSwitch=suspend" ]; then
		sed -i 's/#HandleLidSwitch=suspend/HandleLidSwitch=ignore/' /etc/systemd/logind.conf
		systemctl restart systemd-logind
	elif [ "$TEST" = "HandleLidSwitch=suspend" ]; then
		sed -i 's/HandleLidSwitch=suspend/HandleLidSwitch=ignore/' /etc/systemd/logind.conf
		systemctl restart systemd-logind
	fi


}

case "$1" in

	--ignore) lid_close_ignore;;
	--ignore-test) lid_close_ignore_test;;
	--suspend) lid_close_suspend;;
	--suspend-test) lid_close_suspend_test;;

	*)

	echo -e "
	RecBox Laptop Lid Control\n
	Usage:\n
	rb-lidctl.sh [ OPTION ]\n
	Options:\n
	--ignore-test     run test and send notification or execute --ignore option
	--ignore          ignores system suspend when lid is closed
	--suspend-test    run test and send notification or execute --suspend option
	--suspend         suspend system whem lid is closed
" >&2

	exit 1

	;;

esac

exit 0
