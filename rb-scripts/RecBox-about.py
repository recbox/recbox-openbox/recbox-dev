#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class AboutWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="About RecBox")
        self.set_position(Gtk.WindowPosition.CENTER)
        self.set_default_size(425, 0)
        self.set_resizable(False)
        self.set_border_width(10)
        
        main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        main_box.set_homogeneous(False)
        self.add(main_box)
        about_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        about_box.set_homogeneous(False)
        button_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        button_box.set_homogeneous(False)
        
        main_box.pack_start(about_box, True, True, 0)
        main_box.pack_start(button_box, True, True, 0)
        
        logo_image = Gtk.Image()
        logo_image.set_from_file("/usr/share/icons/RecBox/dark-menu-icon.png")
        about_box.pack_start(logo_image, True, True, 0)

        about_label = Gtk.Label()
        about_label.set_markup("<big><b>RecBox</b></big>\n\n"
                        "20.12.0\n\n"
                        "New version of RecBox is released every Sunday.\n"
                        "Check out <a href=\"https://gitlab.com/ProjektRoot/recbox-dev/blob/master/CHANGELOG.md\" "
                        "title=\"Go to GitLab RecBox Repository\">CHANGELOG.md</a> to learn what needs to be updated.\n\n"
                        "GitLab <a href=\"https://gitlab.com/ProjektRoot\" "
                        "title=\"Go to GitLab Profile\">Projekt:Root</a> | <a href=\"https://gitlab.com/ProjektRoot/recbox-dev\" "
                        "title=\"Go to RecBox Repository\">RecBox</a>\n"
                        "Manjaro Forum <a href=\"https://forum.manjaro.org/t/home-studio-with-recbox/7423\" "
                        "title=\"Go to Forum Topic\">Home Studio with RecBox</a>")
        about_label.set_line_wrap(True)
        about_label.set_justify(Gtk.Justification.CENTER)
        about_box.pack_start(about_label, True, True, 0)
        
        close_button = Gtk.Button.new_with_mnemonic("_Close")
        close_button.set_property("width-request", 85)
        close_button.set_halign(Gtk.Align.END)
        close_button.connect("clicked", self.on_close_clicked)
        button_box.pack_start(close_button, True, True, 0)

    def on_close_clicked(self, close_button):
        Gtk.main_quit()

window = AboutWindow()        
window.connect("destroy", Gtk.main_quit)
window.show_all()
Gtk.main()
