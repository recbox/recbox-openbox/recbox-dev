#!/usr/bin/env bash

# VERSION=2.4

export BLACK='\033[0;30m'
export RED='\033[0;31m'
export GREEN='\033[0;32m'
export YELLOW='\033[0;33m'
export BLUE='\033[0;34m'
export PURPLE='\033[0;35m'
export CYAN='\033[0;36m'
export WHITE='\033[0;37m'
export RESETCOLOR='\033[1;00m'

#-----------------------------------------------------------------------------------------------------------------------------------------#
#                                                               DEPENDENCY INSTALLATION                                                   #
#-----------------------------------------------------------------------------------------------------------------------------------------#

deps_install() {

    pamac install alsa-utils openbox obmenu-generator obconf obkey-gtk3 tint2 qt5ct qt5-styleplugins kvantum-qt5 gtk3 exo-gtk3 nitrogen hardinfo jack2 libffado jgmenu networkmanager-dmenu picom dunst scrot skippy-xd bleachbit htop libreoffice-fresh meld cmus noto-fonts ttf-icomoon-icons maia-cursor-theme catfish matcha-gtk-theme kvantum-theme-matcha kid3 manjaro-openbox-scripts manjaro-openbox-common manjaro-openbox-fonts manjaro-openbox-config manjaro-openbox-desktop-settings manjaro-slick-greeter-theme-brown manjaro-grub-theme-brown manjaro-openbox-wallpapers pamac-cli pamac-gtk polkit-gnome redshift rofi zenity xdotool
    pamac build betterlockscreen flat-remix
    sleep 2
    ~/recbox-dev/recbox-install.sh
}

#-----------------------------------------------------------------------------------------------------------------------------------------#
#                                                               RECBOX CONFIGURATION                                                      #
#-----------------------------------------------------------------------------------------------------------------------------------------#

recbox_conf() {

    # picom config file
    echo -e "${GREEN}picom${RESETCOLOR} configuration"
    yes | cp -r ~/recbox-dev/picom $HOME/.config/

    # Dunst config file
    echo -e "${GREEN}Dunst${RESETCOLOR} configuration"
    yes | cp ~/recbox-dev/dunst/dunstrc ~/.config/dunst/

    # Network manager dmenu (Rofi) config file
    echo -e "${GREEN}Networkmanager-dmenu${RESETCOLOR} configuration"
    yes | cp ~/recbox-dev/networkmanager-dmenu/config.ini ~/.config/networkmanager-dmenu/

    # Obmenu generator config files
    echo -e "${GREEN}Obmenu-generator${RESETCOLOR} configuration"
    yes | cp -r ~/recbox-dev/obmenu-generator ~/.config/

    # Notes backup
    echo -e "${GREEN}Backing up your notes${WHITE} (don't bother errors if first configuration)${RESETCOLOR}"
    yes | cp ~/recbox-dev/openbox/notes/mix ~/.config/openbox/notes/mix.bak
    yes | cp ~/recbox-dev/openbox/notes/master ~/.config/openbox/notes/master.bak

    # OpenBox config files
    echo -e "${GREEN}Openbox${RESETCOLOR} configuration"
    yes | cp -r ~/recbox-dev/openbox ~/.config/

    # Ranger theme configuration
    echo -e "${GREEN}Ranger theme${RESETCOLOR} configuration"
    yes | cp -r ~/recbox-dev/ranger/* ~/.config/

    # Redshift config file
    echo -e "${GREEN}Redshift${RESETCOLOR} configuration"
    yes | cp ~/recbox-dev/redshift-config/redshift.conf ~/.config/

    # Thunar config file
    echo -e "${GREEN}Thunar${RESETCOLOR} configuration"
    yes | cp ~/recbox-dev/Thunar/* ~/.config/Thunar/

    # Tint2 panel
    echo -e "${GREEN}Tint2${RESETCOLOR} configuration"
    yes | cp ~/recbox-dev/tint2/* ~/.config/tint2/

    # Skippy-xd config file
    echo -e "${GREEN}Skippy-xd${RESETCOLOR} configuration"
    yes | cp -r ~/recbox-dev/skippy-xd ~/.config/

    # Termite config file
    echo -e "${GREEN}Termite${RESETCOLOR} configuration"
    yes | cp ~/recbox-dev/termite/* ~/.config/termite/

    # Xresources
    echo -e "${GREEN}Xresources${RESETCOLOR} config file configuration"
    yes | cp ~/recbox-dev/Xresources/Xresources/.* ~

    # Htop config file
    echo -e "${GREEN}Htop${RESETCOLOR} configuration"
    yes | cp ~/recbox-dev/htop/htoprc ~/.config/htop/htoprc

    # polybar panel
    echo -e "${GREEN}Polybar${RESETCOLOR} configuration"
    yes | cp ~/recbox-dev/polybar/* ~/.config/polybar/

    # nitrogen config files
    echo -e "${GREEN}Nitrogen${RESETCOLOR} configuration"
    yes | cp -r ~/recbox-dev/nitrogen ~/.config/

    # backup manager configuration
    mkdir -p ~/.config/backup-settings
    yes | cp ~/recbox-dev/backup-settings/* ~/.config/backup-settings

    # darkmode-settings configuration
    mkdir -p ~/.config/darkmode-settings/
    yes | cp ~/recbox-dev/darkmode-settings/* ~/.config/darkmode-settings/

    # might-light-settings configuration
    mkdir -p ~/.config/night-light-settings/
    yes | cp ~/recbox-dev/night-light-settings/* ~/.config/night-light-settings/


    # workflow-settings configuration
    mkdir -p ~/.config/workflow-settings/
    yes | cp ~/recbox-dev/workflow-settings/* ~/.config/workflow-settings/

    # QT Kvantum themes
    echo -e "${GREEN}Kvantum themes${RESETCOLOR} configuration"
    mkdir -p ~/.config/Kvantum
    yes | cp -r ~/recbox-dev/Kvantum ~/.config/

    # mirror list helper script configuration
    echo -e "${GREEN}mirror list helper${RESETCOLOR} configuration"
    yes | cp ~/recbox-dev/mirrors-conf/manjaro-mirrors.conf ~/.config

    # GTK+ themes
    echo -e "${GREEN}Themes${RESETCOLOR} configuration"
    yes | cp -r ~/recbox-dev/gtk-3.0 ~/.config/
    yes | cp -r ~/recbox-dev/default ~/.icons/
    yes | cp -r ~/recbox-dev/qt5ct ~/.config/
    yes | cp ~/recbox-dev/gtkrc-2.0 ~/.gtkrc-2.0
    yes | cp -r ~/recbox-dev/xfce4 ~/.config/
    yes | sudo cp -r ~/recbox-dev/themes/light-recbox /usr/share/themes/
    yes | sudo cp -r ~/recbox-dev/themes/dark-recbox /usr/share/themes/

    # Jgmenu config files and icons
    echo -e "${GREEN}Jgmenu${RESETCOLOR} configuration"
    yes | sudo cp -r ~/recbox-dev/jgmenu/RecBox /usr/share/icons/
    yes | sudo cp ~/recbox-dev/jgmenu/* ~/.config/jgmenu/

    # Tint2 icons
    echo -e "${GREEN}Tint2 icons${RESETCOLOR} configuration"
    yes | sudo cp ~/recbox-dev/tint-icons/* /usr/share/icons/RecBox/

    # Scripts
    echo -e "${GREEN}Scripts${RESETCOLOR} configuration"
    yes | sudo cp ~/recbox-dev/rb-scripts/* /usr/bin/

    # Copy documentation to the /usr/share/doc/ folder
    echo -e "${GREEN}Copy the documentation to the /usr/share/doc/ folder${RESETCOLOR}"
    yes | sudo mkdir -p /usr/share/doc/recbox/
    yes | sudo cp ~/recbox-dev/offline-help/* /usr/share/doc/recbox/

    # OpenBox items for XFCE Settings Manager
    echo -e "${GREEN}Adding OpenBox items to XFCE Settings Manager${RESETCOLOR}"
    yes | sudo cp ~/recbox-dev/rb-xfce-items/* /usr/share/applications/
    yes | sudo cp ~/recbox-dev/rb-xfce-items-local/* /usr/share/applications/

    # Rofi themes
    echo -e "${GREEN}Rofi themes${RESETCOLOR} configuration"
    yes | sudo cp ~/recbox-dev/rofi-themes/* /usr/share/rofi/themes/
    yes | cp ~/recbox-dev/rofi/config ~/.config/rofi/config

    # Backgrounds
    echo -e "${GREEN}Adding RecBox wallpaper${RESETCOLOR}"
    yes | sudo cp ~/recbox-dev/backgrounds/* /usr/share/backgrounds/

    # Removing nm-applet from panel tray
    echo -e "${GREEN}Removing${RESETCOLOR} Network applet from panel tray"
    echo -e "${GREEN}Can be restored${RESETCOLOR} via Desktop > Network applet > add to panel tray"
    yes | sudo mv /etc/xdg/autostart/nm-applet.desktop /etc/xdg/autostart/nm-applet.back

    # betterlockscreen
    echo -e "${GREEN}\nBetterLockScreen${RESETCOLOR} configuration"
    yes | cp ~/recbox-dev/betterlockscreen/* ~/.config/
    betterlockscreen -u /usr/share/backgrounds/RecBox_minimalist_dark.png

    # betterlockscreen service
    echo -e "${GREEN}betterlockscreen service${RESETCOLOR} configuration"
    yes | sudo cp ~/recbox-dev/betterlock-service/betterlockscreen@.service /etc/systemd/system/

    # RTC service
    echo -e "${GREEN}RTC service${RESETCOLOR} configuration"
    yes | sudo cp ~/recbox-dev/rtc-service/rtc.sh /usr/bin/    
    yes | sudo cp ~/recbox-dev/rtc-service/rtc.service /etc/systemd/system/

    # HPET service
    echo -e "${GREEN}HPET service${RESETCOLOR} configuration"
    yes | sudo cp ~/recbox-dev/hpet-service/hpet.sh /usr/bin/    
    yes | sudo cp ~/recbox-dev/hpet-service/hpet.service /etc/systemd/system/

    sleep 2
    ~/recbox-dev/recbox-install.sh
}

#-----------------------------------------------------------------------------------------------------------------------------------------#
#                                                               MAIN MENU                                                                 #
#-----------------------------------------------------------------------------------------------------------------------------------------#

clear

title="RecBox Menu:"
prompt="What you want to do?"
options=("Install Dependency" "Configure RecBox")

echo "$title"
PS3="$prompt "
select opt in "${options[@]}" "Quit"; do 

    case "$REPLY" in

    1 ) echo -e "You picked${YELLOW} $opt${RESETCOLOR}"
        deps_install; break;;
    2 ) echo -e "You picked${YELLOW} $opt${RESETCOLOR}"
        recbox_conf; break;;

    $(( ${#options[@]}+1 )) ) echo "Goodbye!"; break;;
    *) echo "Invalid option. Try another one.";continue;;

    esac
done
