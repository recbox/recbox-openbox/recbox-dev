**What is RecBox:**

RecBox is an OpenBox config focused on Audio Recording and daily usage without creating extra user account or installing it on separate partition. If you've used "Studio" like operating systems before, you probably know that there are major differences between distributions for daily usage and "Studio" ones. It's possible to use them as daily driver but it's not necessarily comfortable when you want to use full potential of Linux as a system for audio engineering. So, RecBox is an attempt to merge both worlds.

**Where is the trick?**

Technically RecBox is "one cofig" but with help of shell scripts it replaces configuration files and enable/disable needed services between modes (Daily, Studio and Alternative). One of main goals is to keep RecBox lightweight and Studio mode even lighter to maximize performance on laptops and older hardware. From a work point of view (recording, mixing etc) you don't need all those visual fireworks and other daemonized stuff working in background when you spend 99% of your time in DAW. This is waste of resources imo (when hardware is not speed daemon). But if you like you can enable and add whatever you want or need to your session. There is no restriction here.

From the basics, RecBox is unified with Manjaro Openbox regular version maintained by **linux-aarhus**, so there are not many differences in work-flow in general. RecBox adds few more gadgets on top, so if you like you can pick what you like and add to existing Openbox installation but it will require scripts modification.

**Mouse and keyboard:**

RecBox uses both philosophies. Everything what you can do from keyboard, you're able to do with mouse. It's not because I want to target both groups. It is a practical aspect. Using mouse while recording is more practical especially when you are one man army. So RecBox is kind a mix of i3 and traditional desktop.

**Philosophical aspect:**

Goal for RecBox is not to be the best ever made "Studio" like OS. Main idea is to give opportunity for users with older hardware to create more advanced projects and help musicians to save money for better hardware for recording and/or be own music producers. Even if you prefer to leave album production for professionals there are many things you need to learn if you want to save stress for your band mates and yourself.

**Last but not least:**

- if you want to make some modifications, but you don't know where to start check out "How RecBox works" in documentation or Project Wiki

- in Documentation, you can find links to tutorials and few written by me if you need help

- also, if you don't want to drop your desktop environment and like to adopt some tools I'll be happy to help with adaptation

---------------------------------------------------------------

**Recommended Distribution**:

> [Manjaro Openbox](https://manjaro.org/get-manjaro/#openbox)

**Installation:**

$ `cd ~`

$ `git clone https://gitlab.com/ProjektRoot/recbox-dev.git`

$ `cd recbox-dev/`

$ `./recbox-install.sh`

#### Check out [Wiki](https://gitlab.com/ProjektRoot/recbox-dev/wikis/home) for dependency and more.

---------------------------------------------------------------

#### Troubleshooting:

**betterlockscreen**:

If lockscreen is white just type your password and refresh path to wallpaper:
```
betterlockscreen -u /usr/share/backgrounds/xfce/light-stripe-maia.jpg
```

If after betterlockscreen update showing errors delete i3lock cache from /.cache/ folder
```
rm -r ~/.cache/i3lock
```

-------------------------------------------------------------------

**Easy configuration**:

![easy_config.png](https://gitlab.com/ProjektRoot/recbox-screenshots/raw/master/easy_config.png)

**Easy extra plugins installation**:

![extra_plugins](https://gitlab.com/ProjektRoot/recbox-screenshots/raw/master/extra_plugins.png)

**One click mode changing**:

![changing_modes](https://gitlab.com/ProjektRoot/recbox-screenshots/raw/master/changing_modes.png)

**Tint2 Panel**:

![panel_layouts](https://gitlab.com/ProjektRoot/recbox-screenshots/raw/master/panel_layouts.png)

**OpenBox utilities in XFCE Settings Manager**:

![xfce_settings](https://gitlab.com/ProjektRoot/recbox-screenshots/raw/master/xfce_settings.png)

**Jgmenu**:

![jgmenus](https://gitlab.com/ProjektRoot/recbox-screenshots/raw/master/jgmenus.png)

**Obmenu**:

![obmenu](https://gitlab.com/ProjektRoot/recbox-screenshots/raw/master/obmenu.png)

**Rofi Themes**:

![rofimenu](https://gitlab.com/ProjektRoot/recbox-screenshots/raw/master/rofimenu.png)

#### **Special thanks to**:

* [deadguy](https://gitlab.com/deadguy)
* [linux-aarhus](https://github.com/fhdk)

#### **Inspiration**:

[AV Linux](http://www.bandshed.net/avlinux/) | [Manjaro](https://manjaro.org/) | [ArchLabs Linux](https://archlabslinux.com/) | [Luke Smith](https://www.youtube.com/channel/UC2eYFnH61tmytImy1mTYvhA)
