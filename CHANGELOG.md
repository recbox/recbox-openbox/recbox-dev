## RecBox 20.12.0

- add Restart Dunst item to Dunst Notifications (Settings Menu)
- fix Network Menu don't open when Wi-Fi is Disabled and no VPN configured
- add Global themes to RecBox Colors:
- - Matcha Sea
- - Matcha Aliz
- - Matcha Azul
- update *About RecBox* release number


**Changes in files (20.11.3 --> 20.11.4):**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-dev.git
meld recbox-dev/jgmenu/right_click_menu.csv $HOME/.config/jgmenu/right_click_menu.csv
sudo cp recbox-dev/rb-scripts/tint2-buttons.sh /usr/bin/tint2-buttons.sh
sudo cp recbox-dev/rb-scripts/polybar-buttons.sh /usr/bin/polybar-buttons.sh
sudo cp recbox-dev/rb-scripts/rb-colors.sh /usr/bin/rb-colors.sh
sudo cp recbox-dev/rb-scripts/RecBox-about.py /usr/bin/RecBox-about.py
```

**Files to add:**
```
sudo cp recbox-dev/rb-scripts/rb-colors-global.sh /usr/bin/
```


## RecBox 20.11.4

- Rofi screen-shot menu improved and moved to **rb-screenshot.sh**
- changed **rb-screensot.sh** commands from xxx_xxx to --xxx-xx
- add **Edit Menu** to Tools menu. Sub menu contains items to edit:
- - Carla menu
- - Info menu
- - Instrument FX
- - Jack Connections
- - Mixer
- - Notes
- - Shortcuts
- - Software
- - Tools
- - Window
- minor changes in **shortcuts.csv**
- Announcements items (stable, testing unstable) moved to sub menu **Announcements** in Software menu
- update *About RecBox* release number

### Before you start switch to normal mode (light theme) first.

**Changes in files (20.11.3 --> 20.11.4):**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-dev.git
meld recbox-dev/openbox/rc.xml $HOME/.config/openbox/rc.xml
meld recbox-dev/jgmenu/tools.csv $HOME/.config/jgmenu/tools.csv
meld recbox-dev/jgmenu/shortcuts.csv $HOME/.config/jgmenu/shortcuts.csv
meld recbox-dev/jgmenu/software.csv  $HOME/.config/jgmenu/software.csv
sudo cp recbox-dev/rb-scripts/scripts-box.sh /usr/bin/scripts-box.sh
sudo cp recbox-dev/rb-scripts/rb-screenshot.sh /usr/bin/rb-screenshot.sh
sudo cp recbox-dev/rb-scripts/tint2-buttons.sh /usr/bin/tint2-buttons.sh
sudo cp recbox-dev/rb-scripts/polybar-buttons.sh /usr/bin/polybar-buttons.sh
sudo cp recbox-dev/rb-scripts/RecBox-about.py /usr/bin/RecBox-about.py
openbox --reconfigure
```
## RecBox 20.11.3

- stay alive commands change in main menu
- change menu font improvement and command change
- panel switch script minor changes
- update *About RecBox* release number

**Changes in files (20.11.2 --> 20.11.3):**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-dev.git
meld recbox-dev/jgmenu/append.csv $HOME/.config/jgmenu/append.csv
sudo cp recbox-dev/rb-scripts/scripts-box.sh /usr/bin/scripts-box.sh
sudo cp recbox-dev/rb-scripts/RecBox-about.py /usr/bin/RecBox-about.py
```

## RecBox 20.11.2

- lid control script improvement, instead of typing password in terminal, policy authentication window will pop-up
- fix logout in side menu
- libre office decor is now set to 'yes' in rc.xml (change if you like)
- update *How RecBox works*, mostly bookmarks
- update *About RecBox* release number

### Before you start switch to normal mode (light theme) first.

**Changes in files (20.11.1 --> 20.11.2):**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-dev.git
meld recbox-dev/jgmenu/tools.csv $HOME/.config/jgmenu/tools.csv
meld recbox-dev/openbox/rc.xml $HOME/.config/openbox/rc.xml
sudo cp recbox-dev/rb-scripts/rb-session.sh /usr/bin/rb-session.sh
sudo cp recbox-dev/rb-scripts/rb-lidctl.sh /usr/bin/rb-lidctl.sh
sudo cp recbox-dev/offline-help/How\ RecBox\ works.pdf /usr/share/doc/recbox/How\ RecBox\ works.pdf
sudo cp recbox-dev/rb-scripts/RecBox-about.py /usr/bin/RecBox-about.py
openbox --reconfigure
```

## RecBox 20.11.1

- panel menus, side menu and main menu now will follow button position without manual configuration
- fix Backup and Update
- notifications display update
- minor changes in rb-colors.sh (RecBox Colors)
- other minor changes

### Before you start switch to normal mode (light theme) first.

**Changes in files (20.11.0 --> 20.11.1):**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-dev.git
cp recbox-dev/openbox/jgmenu-theme-configs/panel-menus-rc $HOME/.config/openbox/jgmenu-theme-configs/
cp recbox-dev/tint2/recbox-hidden.tint2rc $HOME/.config/tint2/tint2rc 
cp recbox-dev/tint2/recbox-daily-dark.tint2rc $HOME/.config/tint2/recbox-daily-dark.tint2rc
cp recbox-dev/tint2/recbox-daily-light.tint2rc $HOME/.config/tint2/recbox-daily-light.tint2rc
cp recbox-dev/tint2/recbox-studio-dark.tint2rc $HOME/.config/tint2/recbox-studio-dark.tint2rc
cp recbox-dev/tint2/recbox-studio-light.tint2rc $HOME/.config/tint2/recbox-studio-light.tint2rc
cp recbox-dev/jgmenu/jgmenurc $HOME/.config/jgmenu/jgmenurc
cp recbox-dev/openbox/jgmenu-theme-configs/dark-jgmenu $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu
cp recbox-dev/openbox/jgmenu-theme-configs/light-jgmenu $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu
cp recbox-dev/openbox/jgmenu-theme-configs/sidemenu-dark-rc $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc
cp recbox-dev/openbox/jgmenu-theme-configs/sidemenu-light-rc $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc 
cp recbox-dev/openbox/key-bindings $HOME/.config/openbox/key-bindings
cp recbox-dev/dunst/dunstrc $HOME/.config/dunst/dunstrc
sudo cp recbox-dev/rb-scripts/rb-colors.sh /usr/bin/
sudo cp recbox-dev/rb-scripts/tint2-buttons.sh /usr/bin/tint2-buttons.sh
sudo cp recbox-dev/rb-scripts/polybar-buttons.sh /usr/bin/polybar-buttons.sh
sudo cp recbox-dev/rb-scripts/scripts-box.sh /usr/bin/scripts-box.sh
sudo cp recbox-dev/rb-scripts/RecBox-about.py /usr/bin/RecBox-about.py
```

**Files to remove:**

```
rm $HOME/.config/openbox/jgmenu-theme-configs/carla-rc
rm $HOME/.config/openbox/jgmenu-theme-configs/info-rc
rm $HOME/.config/openbox/jgmenu-theme-configs/instrumentfx-rc
rm $HOME/.config/openbox/jgmenu-theme-configs/jack-rc
rm $HOME/.config/openbox/jgmenu-theme-configs/mixer-rc
rm $HOME/.config/openbox/jgmenu-theme-configs/notes-rc
rm $HOME/.config/openbox/jgmenu-theme-configs/places-rc
rm $HOME/.config/openbox/jgmenu-theme-configs/shortcuts-rc
rm $HOME/.config/openbox/jgmenu-theme-configs/software-rc
rm $HOME/.config/openbox/jgmenu-theme-configs/tools-rc
rm $HOME/.config/openbox/jgmenu-theme-configs/update-stable-rc
rm $HOME/.config/openbox/jgmenu-theme-configs/update-testing-rc
rm $HOME/.config/openbox/jgmenu-theme-configs/update-unstable-rc
rm $HOME/.config/openbox/jgmenu-theme-configs/window-rc
```

## RecBox 20.11.0

- add markups to 'Back' item in:
- - main menu
- - side menu
- - panel menus
- - right-click menu
- RecBox Colors improvement and add right-click menu support
- add new item to main menu - Shortcuts
- update *How RecBox works*
- update *About RecBox* release number

**Changes in files (20.10.3 --> 20.11.0):**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-dev.git
meld recbox-dev/jgmenu/right_click_menu.csv $HOME/.config/jgmenu/right_click_menu.csv
meld recbox-dev/jgmenu/append.csv $HOME/.config/jgmenu/append.csv
cp recbox-dev/openbox/jgmenu-theme-configs/dark-jgmenu $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu
cp recbox-dev/openbox/jgmenu-theme-configs/light-jgmenu $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu
cp recbox-dev/openbox/key-bindings $HOME/.config/openbox/key-bindings
sudo cp recbox-dev/rb-scripts/rb-colors.sh /usr/bin/
sudo cp recbox-dev/rb-scripts/tint2-buttons.sh /usr/bin/tint2-buttons.sh
sudo cp recbox-dev/rb-scripts/polybar-buttons.sh /usr/bin/polybar-buttons.sh
sudo cp recbox-dev/offline-help/How\ RecBox\ works.pdf /usr/share/doc/recbox/How\ RecBox\ works.pdf
sudo cp recbox-dev/rb-scripts/RecBox-about.py /usr/bin/RecBox-about.py
```

## RecBox 20.10.3

- update RecBox Colors
- - menu text
- - default colors
- some cleaning and minor changes in rc.xml
- system tray background and item spacing in panel
- update *How RecBox works*
- update *About RecBox* release number

### Before you start switch to normal mode (light theme) first.

**Changes in files (20.10.2 --> 20.10.3):**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-dev.git
meld recbox-dev/openbox/rc.xml $HOME/.config/openbox/rc.xml
cp recbox-dev/tint2/recbox-hidden.tint2rc $HOME/.config/tint2/tint2rc 
cp recbox-dev/tint2/recbox-daily-dark.tint2rc $HOME/.config/tint2/recbox-daily-dark.tint2rc
cp recbox-dev/tint2/recbox-daily-light.tint2rc $HOME/.config/tint2/recbox-daily-light.tint2rc
cp recbox-dev/tint2/recbox-studio-dark.tint2rc $HOME/.config/tint2/recbox-studio-dark.tint2rc
cp recbox-dev/tint2/recbox-studio-light.tint2rc $HOME/.config/tint2/recbox-studio-light.tint2rc
sudo cp recbox-dev/rb-scripts/rb-colors.sh /usr/bin/
sudo cp recbox-dev/offline-help/How\ RecBox\ works.pdf /usr/share/doc/recbox/How\ RecBox\ works.pdf
sudo cp recbox-dev/rb-scripts/RecBox-about.py /usr/bin/RecBox-about.py
openbox --reconfigure
```

## RecBox 20.10.2

- window show/hide delays reduced (rc.xml)
- add links to Tutorials.pdf (Produce Like A Pro)
- `rb-rofi-suspend.sh` and `rb-jgmenu-suspend.sh` replaced with `rb-session-suspend.sh`
- minor changes in Screenshot script
- Recbox Colors desktop file renamed
- update *About RecBox* release number

### Before you start switch to normal mode (light theme) first.

**Changes in files (20.10.1 --> 20.10.2):**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-dev.git
meld recbox-dev/openbox/rc.xml $HOME/.config/openbox/rc.xml
sudo cp recbox-dev/rb-scripts/rb-screenshot.sh /usr/bin/
sudo cp recbox-dev/offline-help/Tutorials.pdf /usr/share/doc/recbox/Tutorials.pdf
sudo mv /usr/share/applications/RecBox\ Colors.desktop /usr/share/applications/recbox-colors.desktop
sudo cp recbox-dev/rb-scripts/rb-session.sh /usr/bin/
sudo cp recbox-dev/rb-scripts/rb-session-suspend.sh /usr/bin/ 
sudo rm /usr/bin/rb-rofi-suspend.sh
sudo rm /usr/bin/rb-jgmenu-suspend.sh
sudo cp recbox-dev/rb-scripts/tint2-buttons.sh /usr/bin/tint2-buttons.sh
sudo cp recbox-dev/rb-scripts/polybar-buttons.sh /usr/bin/polybar-buttons.sh
sudo cp recbox-dev/rb-scripts/RecBox-about.py /usr/bin/RecBox-about.py
openbox --reconfigure
```

## RecBox 20.10.1

- session management improved ( not 100% finished )
- - now after shutdown, reboot and logout Night Light  script will recognize that Night Light is off
- minor changes in dunst config file (duns restart required)
- - line height
- - word wrap
- - icons path
- icons for brightness and volume notifications updated
- csv_name_format in light jgmenu updated
- update *About RecBox* release number

### Before you start switch to normal mode (light theme) first.

**Changes in files (20.10.0 --> 20.10.1):**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-dev.git
meld recbox-dev/dunst/dunstrc $HOME/.config/dunst/dunstrc
meld recbox-dev/openbox/rc.xml $HOME/.config/openbox/rc.xml && openbox --reconfigure
sudo cp recbox-dev/rb-scripts/rb-control.sh /usr/bin/rb-control.sh
sudo cp recbox-dev/rb-scripts/rb-control-pa.sh /usr/bin/rb-control-pa.sh
sudo cp recbox-dev/rb-scripts/tint2-buttons.sh /usr/bin/tint2-buttons.sh
sudo cp recbox-dev/rb-scripts/polybar-buttons.sh /usr/bin/polybar-buttons.sh
sudo cp recbox-dev/rb-scripts/scripts-box.sh /usr/bin/scripts-box.sh
sudo cp recbox-dev/rb-scripts/RecBox-about.py /usr/bin/RecBox-about.py
```

**Files to add:**
```
sudo mv recbox-dev/rb-scripts/rb-session.sh /usr/bin/
```

## RecBox 20.10.0

- generic icons in taskbar replaced with Flat Remix ones
- - Calendar
- - RecBox Center
- - RecBox Documentation
- scrot commands in Screenshots menu (Side Menu) replaced with rb-screenshot.sh

Now you can change default screen shot name thanks to Zenity widget.

- minor cleaning in **rc.xml** file (Openbox config file)
- minor changes in night light settings config file
- update *About RecBox* release number

### Before you start switch to normal mode (light theme) first.

**Changes in files (20.9.3 --> 20.10.0):**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-dev.git
meld recbox-dev/openbox/rc.xml $HOME/.config/openbox/rc.xml
meld recbox-dev/night-light-settings/config $HOME/.config/night-light-settings/config
cp recbox-dev/openbox/jgmenu-theme-configs/light-jgmenu $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu
sudo cp recbox-dev/rb-scripts/tint2-buttons.sh /usr/bin/tint2-buttons.sh
sudo cp recbox-dev/rb-scripts/polybar-buttons.sh /usr/bin/polybar-buttons.sh
sudo cp recbox-dev/rb-scripts/scripts-box.sh /usr/bin/scripts-box.sh
sudo cp recbox-dev/rb-scripts/rb-center.sh /usr/bin/rb-center.sh
sudo cp recbox-dev/rb-scripts/RecBox-about.py /usr/bin/RecBox-about.py
```

**Files to add:**
```
sudo mv recbox-dev/rb-scripts/rb-screenshot.sh /usr/bin/
```

## RecBox 20.9.3

- hardcod for switching options in WM Settings and Dark Mode removed
- minimizing, maximizing, move window left and right behavior changed a bit, now they also toggle title bar
- move window left and right geometry changed
- update Ardour first start and tips (electric guitar mixing)
- update How RecBox works (check out Config Files)
- update *About RecBox* release number

### Before you start switch to normal mode (light theme) first.

**Changes in files (20.9.2 --> 20.9.3):**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-dev.git
meld recbox-dev/openbox/rc.xml $HOME/.config/openbox/rc.xml
sudo cp recbox-dev/rb-scripts/rb-dark-mode-settings.sh /usr/bin/rb-dark-mode-settings.sh
sudo cp recbox-dev/rb-scripts/rb-dark-mode-settings-test.sh /usr/bin/rb-dark-mode-settings-test.sh
sudo cp recbox-dev/rb-scripts/scripts-box.sh /usr/bin/scripts-box.sh
sudo cp recbox-dev/offline-help/Ardour\ first\ start\ and\ tips.pdf /usr/share/doc/recbox/Ardour\ first\ start\ and\ tips.pdf
sudo cp recbox-dev/offline-help/How\ RecBox\ works.pdf /usr/share/doc/recbox/How\ RecBox\ works.pdf
sudo cp recbox-dev/rb-scripts/RecBox-about.py /usr/bin/RecBox-about.py
```

## RecBox 20.9.2

- method of executing main menu in panels is changed, now it will start from script which suit menu position to panel height
- items in WM settings for border are changed to toggle item
- fix misspelling in custom layout item
- most of hardcode for changing Openbox behavior is removed and replaced with better scripts. See **How RecBox works** > **Config Files** > **Openbox**
- update How RecBox works (work in progress)
- update *About RecBox* release number

**Changes in files (20.9.1 --> 20.9.2):**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-dev.git
meld recbox-dev/openbox/rc.xml $HOME/.config/openbox/rc.xml
cp recbox-dev/tint2/recbox-hidden.tint2rc $HOME/.config/tint2/tint2rc 
cp recbox-dev/tint2/recbox-daily-dark.tint2rc $HOME/.config/tint2/recbox-daily-dark.tint2rc
cp recbox-dev/tint2/recbox-daily-light.tint2rc $HOME/.config/tint2/recbox-daily-light.tint2rc
cp recbox-dev/tint2/recbox-studio-dark.tint2rc $HOME/.config/tint2/recbox-studio-dark.tint2rc
cp recbox-dev/tint2/recbox-studio-light.tint2rc $HOME/.config/tint2/recbox-studio-light.tint2rc
sudo cp recbox-dev/rb-scripts/tint2-buttons.sh /usr/bin/tint2-buttons.sh
sudo cp recbox-dev/rb-scripts/polybar-buttons.sh /usr/bin/polybar-buttons.sh
sudo cp recbox-dev/rb-scripts/scripts-box.sh /usr/bin/scripts-box.sh
sudo cp recbox-dev/offline-help/How\ RecBox\ works.pdf /usr/share/doc/recbox/How\ RecBox\ works.pdf
sudo cp recbox-dev/rb-scripts/RecBox-about.py /usr/bin/RecBox-about.py
```

## RecBox 20.9.1

- changed iten normal background in light side menu
- changed important line numbers in rc.xml and scripts
- Ctrl+Tab is changed to Ctrl+0
- add version number to rc.xml
- add closing all notifications when opening side menu, info menu, volume and brightness widget
- update How RecBox works (work in progress)
- update *About RecBox* release number

**Changes in files (20.9.0 --> 20.9.1):**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-dev.git
meld recbox-dev/openbox/rc.xml $HOME/.config/openbox/rc.xml
cp recbox-dev/openbox/key-bindings $HOME/.config/openbox/key-bindings
cp recbox-dev/openbox/jgmenu-theme-configs/sidemenu-light-rc $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc 
sudo cp recbox-dev/rb-scripts/rb-dark-mode-settings.sh /usr/bin/rb-dark-mode-settings.sh
sudo cp recbox-dev/rb-scripts/rb-dark-mode-settings-test.sh /usr/bin/rb-dark-mode-settings-test.sh
sudo cp recbox-dev/rb-scripts/scripts-box.sh /usr/bin/scripts-box.sh
sudo cp recbox-dev/rb-scripts/rb-control.sh /usr/bin/rb-control.sh
sudo cp recbox-dev/rb-scripts/rb-control-pa.sh /usr/bin/rb-control-pa.sh
sudo cp recbox-dev/rb-scripts/tint2-buttons.sh /usr/bin/tint2-buttons.sh
sudo cp recbox-dev/rb-scripts/polybar-buttons.sh /usr/bin/polybar-buttons.sh
sudo cp recbox-dev/offline-help/How\ RecBox\ works.pdf /usr/share/doc/recbox/How\ RecBox\ works.pdf
sudo cp recbox-dev/rb-scripts/RecBox-about.py /usr/bin/RecBox-about.py
```

## RecBox 20.9.0

- fix rofi icons items in Menu Settings (right click menu)
- add new links to Tutorials.pdf
- update Ardour and Helm in RecBox Center
- Ardour theme removed from RecBox Center (soon will be available in Ardour themes)
- update Tooltips and Thumbnails item in Menu Settings (right click menu)
- fix Helm icon in Main Menu
- update How RecBox works (work in progress)
- update *About RecBox* release number

**Changes in files (20.8.4 --> 20.9.0):**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-dev.git
meld recbox-dev/jgmenu/right_click_menu.csv $HOME/.config/jgmenu/right_click_menu.csv
meld recbox-dev/jgmenu/append.csv $HOME/.config/jgmenu/append.csv
sudo cp recbox-dev/rb-scripts/rb-center.sh /usr/bin/rb-center.sh
sudo cp recbox-dev/offline-help/Tutorials.pdf /usr/share/doc/recbox/Tutorials.pdf 
sudo cp recbox-dev/offline-help/How\ RecBox\ works.pdf /usr/share/doc/recbox/How\ RecBox\ works.pdf
sudo cp recbox-dev/rb-scripts/RecBox-about.py /usr/bin/RecBox-about.py
```

## RecBox 20.8.4

- add script to execute jgmenu right click menu
- add csv file with items for right click menu (jgmenu)
- add light and dark theme variant for right click jgmenu
- update darkmode to work with new right click jgmenu
- update compositor configuration (picom)
- update openbox themes
- update Bass guitar mixing
- update *About RecBox* release number

**Changes in files (20.8.3 --> 20.8.4):**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-dev.git
meld recbox-dev/openbox/rc.xml $HOME/.config/openbox/rc.xml
meld recbox-dev/darkmode-settings/config $HOME/.config/darkmode-settings/config
meld recbox-dev/picom/picom.conf $HOME/.config/picom/picom.conf
sudo cp recbox-dev/rb-scripts/scripts-box.sh /usr/bin/scripts-box.sh
sudo cp recbox-dev/rb-scripts/rb-dark-mode.sh /usr/bin/rb-dark-mode.sh
sudo cp recbox-dev/rb-scripts/rb-dark-mode-settings.sh /usr/bin/rb-dark-mode-settings.sh
sudo cp recbox-dev/rb-scripts/rb-dark-mode-settings-test.sh /usr/bin/rb-dark-mode-settings-test.sh
sudo cp recbox-dev/themes/dark-recbox/openbox-3/themerc /usr/share/themes/dark-recbox/openbox-3/themerc
sudo cp recbox-dev/themes/light-recbox/openbox-3/themerc /usr/share/themes/light-recbox/openbox-3/themerc
sudo cp recbox-dev/offline-help/Ardour\ first\ start\ and\ tips.pdf /usr/share/doc/recbox/Ardour\ first\ start\ and\ tips.pdf
sudo cp recbox-dev/rb-scripts/RecBox-about.py /usr/bin/RecBox-about.py
```

**Files to add:**
```
cp recbox-dev/jgmenu/right_click_menu.csv $HOME/.config/jgmenu/
cp recbox-dev/openbox/jgmenu-theme-configs/right_click_light-rc $HOME/.config/openbox/jgmenu-theme-configs/
cp recbox-dev/openbox/jgmenu-theme-configs/right_click_dark-rc $HOME/.config/openbox/jgmenu-theme-configs/
```

## RecBox 20.8.3

Sadly I need to hold transition from obmenu to jgmenu to next week. But no worries, will be ready on next Sunday.

- tint2 panel layer changed from bottom to normal
- volume, microphone and brightness widgets now don't show icon in taskbar
- fix title double click option in WM Settings (side menu)
- update side menu layout and add slider widget to Microphone item
- update Bass guitar mixing
- update How RecBox works (important lines in rc.xml)
- update *About RecBox* release number and add link to new thread

**Changes in files (20.8.2 --> 20.8.3):**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-dev.git
meld recbox-dev/openbox/rc.xml $HOME/.config/openbox/rc.xml
cp recbox-dev/tint2/recbox-hidden.tint2rc $HOME/.config/tint2/recbox-hidden.tint2rc
cp recbox-dev/tint2/recbox-daily-dark.tint2rc $HOME/.config/tint2/recbox-daily-dark.tint2rc
cp recbox-dev/tint2/recbox-daily-light.tint2rc $HOME/.config/tint2/recbox-daily-light.tint2rc
cp recbox-dev/tint2/recbox-studio-dark.tint2rc $HOME/.config/tint2/recbox-studio-dark.tint2rc
cp recbox-dev/tint2/recbox-studio-light.tint2rc $HOME/.config/tint2/recbox-studio-light.tint2rc
sudo cp recbox-dev/rb-scripts/scripts-box.sh /usr/bin/scripts-box.sh
sudo cp recbox-dev/rb-scripts/tint2-buttons.sh /usr/bin/tint2-buttons.sh
sudo cp recbox-dev/rb-scripts/polybar-buttons.sh /usr/bin/polybar-buttons.sh
sudo cp recbox-dev/offline-help/Ardour\ first\ start\ and\ tips.pdf /usr/share/doc/recbox/Ardour\ first\ start\ and\ tips.pdf
sudo cp recbox-dev/offline-help/How\ RecBox\ works.pdf /usr/share/doc/recbox/How\ RecBox\ works.pdf
sudo cp recbox-dev/rb-scripts/RecBox-about.py /usr/bin/RecBox-about.py
```

## RecBox 20.8.2

Update for Openbox themes will arrive next Sunday with new right click menu based on jgmenu. Obmenu will be no more in use.

- update tint2 panel themes to match Matcha GTK themes
- update jgmenu themes to match Matcha GTK themes
- update rofi themes to match Matcha GTK themes
- update volume widget (On/Off button)
- update AVL Drums guide
- update About RecBox release number

**Changes in files (20.8.2 --> 20.8.1):**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-dev.git
cp recbox-dev/tint2/tint2rc $HOME/.config/tint2/tint2rc
cp recbox-dev/tint2/recbox-alternarive-dark.tint2rc $HOME/.config/tint2/recbox-alternarive-dark.tint2rc
cp recbox-dev/tint2/recbox-hidden.tint2rc $HOME/.config/tint2/recbox-hidden.tint2rc
cp recbox-dev/tint2/recbox-daily-dark.tint2rc $HOME/.config/tint2/recbox-daily-dark.tint2rc
cp recbox-dev/tint2/recbox-daily-light.tint2rc $HOME/.config/tint2/recbox-daily-light.tint2rc
cp recbox-dev/tint2/recbox-studio-dark.tint2rc $HOME/.config/tint2/recbox-studio-dark.tint2rc
cp recbox-dev/tint2/recbox-studio-light.tint2rc $HOME/.config/tint2/recbox-studio-light.tint2rc
cp recbox-dev/jgmenu/jgmenurc $HOME/.config/jgmenu/jgmenurc
cp recbox-dev/openbox/jgmenu-theme-configs/dark-jgmenu $HOME/.config/openbox/jgmenu-theme-configs/dark-jgmenu
cp recbox-dev/openbox/jgmenu-theme-configs/light-jgmenu $HOME/.config/openbox/jgmenu-theme-configs/light-jgmenu
cp recbox-dev/openbox/jgmenu-theme-configs/sidemenu-dark-rc $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-dark-rc
cp recbox-dev/openbox/jgmenu-theme-configs/sidemenu-light-rc $HOME/.config/openbox/jgmenu-theme-configs/sidemenu-light-rc 
sudo cp recbox-dev/rofi-themes/RecBox-Dark.rasi /usr/share/rofi/themes/RecBox-Dark.rasi
sudo cp recbox-dev/rofi-themes/RecBox-Light.rasi /usr/share/rofi/themes/RecBox-Light.rasi
sudo cp recbox-dev/rb-scripts/rb-control.sh /usr/bin/rb-control.sh
sudo cp recbox-dev/rb-scripts/rb-control-pa.sh /usr/bin/rb-control-pa.sh
sudo cp recbox-dev/offline-help/Ardour\ first\ start\ and\ tips.pdf /usr/share/doc/recbox/Ardour\ first\ start\ and\ tips.pdf
sudo cp recbox-dev/rb-scripts/RecBox-about.py /usr/bin/RecBox-about.py
```

## RecBox 20.8.1

- update guitars mixing guide (Ardour first start and tips)
- fix empty search in rofi places
- update About RecBox release number

**Changes in files (20.8.0 --> 20.8.1):**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-dev.git
sudo cp recbox-dev/rb-scripts/scripts-box.sh /usr/bin/scripts-box.sh
sudo cp recbox-dev/offline-help/Ardour\ first\ start\ and\ tips.pdf /usr/share/doc/recbox/Ardour\ first\ start\ and\ tips.pdf
sudo cp recbox-dev/rb-scripts/RecBox-about.py /usr/bin/RecBox-about.py
```

## RecBox 20.8.0

- update guitars mixing guide (Ardour first start and tips)
- update bass guitar mixing guide (Ardour first start and tips)
- update About RecBox release number

**Changes in files (20.7.3 --> 20.8.0):**

```
cd ~
git clone https://gitlab.com/ProjektRoot/recbox-dev.git
sudo cp recbox-dev/offline-help/Ardour\ first\ start\ and\ tips.pdf /usr/share/doc/recbox/Ardour\ first\ start\ and\ tips.pdf
sudo cp recbox-dev/rb-scripts/RecBox-about.py /usr/bin/RecBox-about.py
```
